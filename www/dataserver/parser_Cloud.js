exports.parse = function (message, callback) {
    var data = message.data;
    if(data.length == 22) {
        var equipData = null;
        var rssi = message.rssi;
        var channel = message.channel
        switch(data[0]) {
            case '1':
            equipData = new Equip_1(message.address, message.time);
            equipData.parse(data);
            break;
            case '2':
            equipData = new Equip_2(message.address, message.time);
            equipData.parse(data);
            break;
            case '3':
            equipData = new Equip_3(message.address, message.time);
            equipData.parse(data);
            break;
            case '4':
            equipData = new Equip_4(message.address, message.time);
            equipData.parse(data);
            break;
            case '5':
            equipData = new Equip_5(message.address, message.time);
            equipData.parse(data);
            break;
            case 'a':
            equipData = new Equip_A(message.address, message.time);
            equipData.parse(data);
            break;
            case 'f':
            equipData = new Equip_SP(message.macAddr, message.time);
            equipData.parse(data);
            break;
        }
        if (equipData == null || equipData.isValid() == false) {
            callback(false, null);
        } else {
            parse = equipData.data()
            parse['rssi'] = rssi;
            parse['channel'] = channel
            parse['data'] = data
            callback(true, {data: {address: message.address, data: message.data, time: message.time, gwid: message.gwid, rssi:message.rssi, channel:message.channel}, parse: parse});
        }   
    } else {
        callback(false, null);
    }
}

class Equip_1 {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 1;
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.power = '';      // 電源
        this.status = '';     // 運轉模式
        this.wind = '';       // 風速
        this.wind_dir = '';  // 風向
        this.set_temp = '';   // 溫度設定
        this.temp = '';       // 室內溫度
        this.cost = '';       // 耗能
        this.errorCode = '';      // 保養/故障碼
        this.remain = '';     // 保留
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                power: this.power,
                status: this.status,
                wind: this.wind,
                wind_dir: this.wind_dir,
                set_temp: this.set_temp,
                temp: this.temp,
                cost: this.cost,
                errorCode: this.errorCode,
                remain: this.remain,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: //電源
                        this.power = ch;
                    break;
                    case 5: // 運轉模式
                        this.status = ch;
                    break;
                    case 6: // 風速
                        this.wind = ch;
                    break;
                    case 7: // 風向
                        this.wind_dir = ch;
                    break;
                    case 8: // 溫度設定
                    case 9:
                        this.set_temp += ch;
                    break;
                    case 10: // 室內溫度
                    case 11:
                        this.temp += ch;
                    break;
                    case 12: // 耗能
                    case 13:
                        this.cost += ch;
                    break;
                    case 14: // 保養 故障碼
                    case 15:
                        this.errorCode += ch;
                    break;
                    case 16: // 保留
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        this.remain += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equip 1 Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}


class Equip_2 {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 2;
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.power = '';
        this.status = '';
        this.wind = '';
        this.cost = '';
        this.remain7 = '';
        this.remain89 = '';
        this.remain1011 = '';
        this.errorCode = '';
        this.remain = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                power: this.power,
                status: this.status,
                wind: this.wind,
                cost: this.cost,
                remain7: this.remain7,
                remain89: this.remain89,
                remain1011: this.remain1011,
                errorCode: this.errorCode,
                remain: this.remain,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: // 電源
                        this.power = ch;
                    break;
                    case 5: // 運轉模式
                        this.status = ch;
                    break;
                    case 6: 
                        this.wind = ch;
                    break;
                    case 7: 
                    case 8:
                        this.cost += ch;
                    break;
                    case 9:
                    case 10: 
                    case 11:
                    case 12:
                    case 13:
                    case 14: // 保留
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        this.remain += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equip 2 Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}

class Equip_3 {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 3;
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.HCHO = '';
        this.CO2 = '';
        this.CO = '';
        this.PM10 = '';
        this.TVOC = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                HCHO: this.HCHO,
                CO2: this.CO2,
                CO: this.CO,
                PM10: this.PM10,
                TVOC: this.TVOC,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: 
                    case 5: 
                    case 6: 
                    case 7: 
                        this.HCHO += ch;
                    break;
                    case 8: 
                    case 9:
                    case 10: 
                    case 11:
                        this.CO2 += ch
                    break;
                    case 12: 
                    case 13:
                    case 14: 
                    case 15:
                        this.CO += ch;
                    break;
                    case 16: 
                    case 17:
                    case 18:
                    case 19:
                        this.PM10 += ch;
                    break;
                    case 20:
                    case 21:
                        this.TVOC += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equipo 3 Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}

class Equip_4 {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 4;
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.opCode = '';     // 讀寫代碼//
        this.temp = '';
        this.maxTemp = '';
        this.minTemp = '';
        this.Other = '';
        this.diffTemp = '';
        this.irCO2 = '';
        this.irTVOC = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                temp: this.temp,
                maxTemp: this.maxTemp,
                minTemp: this.minTemp,
                diffTemp: this.diffTemp,
                irCO2: this.irCO2,
                irTVOC: this.irTVOC,
                Other: this.Other,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: 
                    case 5: 
                        this.temp += ch;
                    break;
                    case 6: 
                    case 7: 
                        this.maxTemp += ch;
                    break;
                    case 8: 
                    case 9:
                        this.minTemp += ch;
                    break;
                    case 10:
                        this.diffTemp += ch;
                    break; 
                    case 11:
                    case 12: 
                    case 13:
                    case 14:
                        this.irCO2 += ch;
                    break; 
                    case 15:
                    case 16: 
                    case 17:
                    case 18:
                        this.irTVOC += ch;
                    break; 
                    default:
                        console.log('\x1b[31m Equip 4 Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}

class Equip_5 {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 5;
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.power = '';
        this.status = '';
        this.wind = '';
        this.cost = '';
        this.temp = '';
        this.remain9 = '';
        this.remain1011 = '';
        this.errorCode = '';
        this.remain = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                power: this.power,
                status: this.status,
                wind: this.wind,
                cost: this.cost,
                temp: this.temp,
                remain9: this.remain9,
                remain1011: this.remain1011,
                errorCode: this.errorCode,
                remain: this.remain,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: // 電源
                        this.power = ch;
                    break;
                    case 5: // 運轉模式
                        this.status = ch;
                    break;
                    case 6: // 風速
                        this.wind = ch;
                    break;
                    case 7: 
                    case 8: 
                        this.temp += ch;
                    break;
                    case 9:
                    case 10:
                        this.cost += ch; 
                    break;
                    case 11:
                        this.remain9 = ch;
                    break;
                    case 12: 
                    case 13:
                        this.remain1011 += ch;
                    break;
                    case 14: 
                    case 15:
                        this.errorCode += ch;
                    break;
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        this.remain += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equip 5 Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}

class Equip_A {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 'a';
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.power = '';
        this.status = '';
        this.error = '';
        this.temp = '';
        this.humidity = '';
        this.produce = '';
        this.remain = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                power: this.power,
                status: this.status,
                error: this.error,
                temp: this.temp,
                humidity: this.humidity,
                produce: this.produce,
                remain: this.remain,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4: // 電源
                        this.power = ch;
                    break;
                    case 5: // 運轉模式
                        this.status = ch;
                    break
                    case 6: // 異常
                        this.error = ch;
                    break;
                    case 7: 
                    case 8: 
                        this.temp += ch;
                    break;
                    case 9:
                    case 10:
                        this.humidity += ch; 
                    break;
                    case 11:
                    case 12: 
                    case 13:
                    case 14: 
                        this.produce += ch;
                    break;
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        this.remain += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equip A Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}


class Equip_SP {
    constructor(macAddr, time) {
        // always initialize all instance properties
        this.deviceId = 'f';
        this.address = macAddr;    // 網卡
        this.equipId = '';    // 設備代碼
        this.packet = '';     //封包
        this.remain = '';
        this.timestamp = time;    
        this.valid = false;
    }
    isValid() {
        return this.valid;
    }
    data() {
        return {address: this.address,
                equipId: this.equipId, 
                packet: this.packet,
                remain: this.remain,
                timestamp: this.timestamp}
    }
    parse(data) {
        if(data[1] == this.deviceId) {
            for(var i = 0; i < data.length; i++) {
                var ch = data[i];
                switch(i) {
                    case 0: // 設備代碼
                    case 1:
                        this.equipId += ch;
                    break;
                    case 2: // 封包
                    case 3:
                        this.packet += ch;
                    break;
                    case 4:
                    case 5:
                    case 6:
                    case 7: 
                    case 8: 
                    case 9:
                    case 10:
                    case 11:
                    case 12: 
                    case 13:
                    case 14: 
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                        this.remain += ch;
                    break;
                    default:
                        console.log('\x1b[31m Equip A Parsing Error \x1b[37m');
                }
            }
            this.valid = true;
        } else {
            this.valid = false;
        }
    }
}