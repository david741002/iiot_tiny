var config = {}

config.COMPANY = '34525113' 

//config.LOCAL_MQTT_SERVER = '140.124.182.66'
//config.CLOUD_MQTT_SERVER = 'localhost'
//config.CLOUD_MQTT_SERVER_2 = 'localhost' //server_2

 config.LOCAL_MQTT_SERVER = 'localhost'
 config.CLOUD_MQTT_SERVER = '10.105.1.219'
// config.CLOUD_MQTT_SERVER_2 = '10.105.2.222' //server_2

config.CLOUD_MQTT_PORT = '1883'
config.CLOUD_MQTT_UP_TOPIC = 'iiot'
config.CLOUD_MQTT_PORT_TOPIC = 'CloudSync'
config.CLOUD_MQTT_DOWN_TOPIC = config.COMPANY
config.LOCAL_MQTT_PORT = '1883'
config.LOCAL_MQTT_TOPIC = 'LocalSync'
config.CLOUD_SERVER = 'http://10.105.1.219:8080'
config.CLOUD_API = {'devices':'/RequireDeviceAddress', 'depends':'/RequireControl'}
config.LOCAL_DATABASE = 'IAQ_Db'
config.LOCAL_DEVICE_DATABASE = 'IAQ_DEVICE_Db'
config.CLOUD_DATABASE = 'IAQ_Db'
config.CLOUD_DEVICE_DATABASE = 'IAQ_DEVICE_Db'

module.exports = config
