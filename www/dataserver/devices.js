var request = require("request"); 
var Device = require('./device')
var event = require('./js/event')
var server = '';
var method = null;
var company = '';
var mongo = null;
var register_devices = [];
var register_address = {};
var device_control = [];

exports.setRemote = function (__server, __method, __company) {
    server = __server;
    method = __method
    company = __company;
}

exports.setMongoDB = function(__mongo) {
    mongo = __mongo;    
}

exports.requestRegisterInformation = function() {
    console.info('SYSTEM : Request Start')
    console.info('SYSTEM : Connect To:' + server + method['devices'] +'?companyID=' + company);

    request(server + method['devices'] +'?companyID=' + company, {timeout: 5000}, function(error, response, body) {
        register_devices.length = 0;
        if(!error && response.statusCode == 200) {
            var json = JSON.parse(body)
            if(IsJsonString(body)) {
                mongo.remove('Ldevice_address', {}, function(result, message) {
                    parseJson(json.address)
                        
                    for(index in register_devices) {
                        mongo.insert('Ldevice_address', register_devices[index].data(), function(result, message) {}); 
                    }

                    mongo.remove('Ldevice_depend', {}, function(result, message) {
                        parseDependJson(json.depends)
                        if (json.depends.length > 0) {
                            mongo.insert('Ldevice_depend', json.depends, function(result, message) {});
                        } 
                        parseEnd();
                    })
                })

                mongo.remove('Lequipment_map', {}, function(result, message) {
                    mongo.insert('Lequipment_map', json.map, function(result, message) {});
                })
            }
        } else if(error || response.statusCode == 404) {
            console.info('SYSTEM : Connect Fail');
            console.info('SYSTEM : Get Data From Local Database');
            mongo.queryFindAll('Ldevice_address', {}, function(result, devices) {
                if(result) {
                    parseJson(devices)

                    mongo.queryFindAll('Ldevice_depend', {}, function(result, depends) {
                        if(result) {
                            parseDependJson(depends)
                        }
                        parseEnd();
                    })
                }
            })
        }
    });
}

exports.devices = function() {
    return register_devices;
}

exports.device = function(address) {
    if(register_address.hasOwnProperty(address))
        return register_address[address]
    return undefined
}

exports.isRegisterGWID = function(gwid) {
//console.log("55555555555555555555555555555555555555555555555555");
//console.log(device_control.indexOf(gwid));
//console.log(gwid);
    if(device_control.indexOf(gwid) > -1)
        return false;  
    device_control.push(gwid);
    mongo.remove('Ldevice_control', {}, function(result, message) {
        var insertion = [];
        for(var index in device_control) {
            insertion.push({gwid: device_control[index]})
        }
        mongo.insert('Ldevice_control', insertion, function(result, message) {})
    })
    
    return true;
}

exports.gwids = function() {
    return device_control;
}

exports.depends = function(address) {
    if(register_address.hasOwnProperty(address))
        return register_address[address].depends()
    return []
}

exports.equip = function(address) {
    if(register_address.hasOwnProperty(address))
        return register_address[address].equip()
    return -1
}

exports.isRegister = function(address) {
    return register_address.hasOwnProperty(address)
}

exports.initGWID = function() {
    mongo.queryFindAll('Ldevice_control', {}, function(result, message) {
        if(result) {
            for(var index in message) {
                device_control.push(message[index].gwid);
            }
        }
    })
}

function parseEnd() {
    register_address = {};
    for(index in register_devices) {
        register_address[register_devices[index].address()] = register_devices[index]
    }
    console.info('Get Local Register Devices');
    console.info(register_address);

    event.emit('SystemReady');
}

function parseJson(jsonArray) {
    for(index in jsonArray) {
        device = new Device(jsonArray[index]);
        register_devices.push(device);
    }
}

function parseDependJson(jsonArray) {
    for(index in register_devices) {
        device = register_devices[index]
        device.clearDepend()
        address = device.address()
        for(idx in jsonArray) {
            if (address == jsonArray[idx].address) {
                device.addDepend(jsonArray[idx].depend)
            }
        }
    }
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
