var config = require('./config')
var mqtt = require('mqtt');
var Device = require('./device')
var mongo = require('./db/mongoDB')
var event = require('./js/event')
var debug = require('./js/debug')

var interval = 10 //s
var Random = Math.floor(Math.random() * 1024) + 1  
var options = {
    port: config.LOCAL_MQTT_PORT,
    host: config.LOCAL_MQTT_SERVER,
    clientId: 'guest_'+Random,
    username: 'guest_'+Random,
    password: 'guest_'+Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

// Global Variables
var deviceObjs = {}
var triggerDevices = []
var actionsDevices = {}
var actionsLog = {}
var deviceControl = []
var client = null
var timer = null
var SystemStatus = false
mongo.init(27017, config.LOCAL_DATABASE, (result) => { //DB init
    if (result) {
        console.log('\x1b[32m mongodb open success \x1b[37m');

        registerSystemReadyEvent();
        requestDeviceInformation()

        // System first request devices information and submit SystemReady event
        event.emit('SystemPrepare');
        timer = setInterval(Trigger, interval * 1000)

        // Mqtt Connection
        client = mqtt.connect(options);
        client.subscribe(config.LOCAL_MQTT_TOPIC);
        client.on('message', function(topic, message) {
            if(!SystemStatus)
                return;

            if(!IsJsonString(message))
                return;

            var json = JSON.parse(message);
            if (json.type == 'update') {
                event.emit('SystemPrepare');
                requestDeviceInformation()
            }
        })

        function registerSystemReadyEvent() {
            event.on('SystemReady', function() {
                SystemStatus = true;
            })

            event.on('SystemPrepare', function() {
                SystemStatus = false;
            })
        }
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
})

function requestDeviceInformation() {
    mongo.queryFindAll('Ldevice_address', {}, function(result, devices) {
        if(result) {
            var devicesAry = createDevices(devices)

            mongo.queryFindAll('Ldevice_depend', {}, function(result, depends) {
                if(result) {
                    devicesAry = createDeviceDepends(depends, devicesAry)
                    registerTriggerDevice(devicesAry)
                    console.info(triggerDevices)
                    mongo.queryFindAll('Ldevice_control', {}, function(result, message) {
                        if(result) {
                            deviceControl = []
                            for(var index in message) {
                                deviceControl.push(message[index].gwid);
                            }
                            ClearActionsLog()
                            event.emit('SystemReady');
                        }
                    })
                }
            })
        }
    })
}

function createDevices(jsonArray) {
    var devices = []
    for(index in jsonArray) {
        device = new Device(jsonArray[index]);
        devices.push(device);
    }
    return devices
}

function createDeviceDepends(jsonArray, devices) {
    for(index in devices) {
        device = devices[index]
        device.clearDepend()
        address = device.address()
        for(idx in jsonArray) {
            if (jsonArray[idx].type == 'depend' && address == jsonArray[idx].address) {
                // address 為自己的情況下會有使用者設定的門檻 action
                // 不同 address 的情況下則交給決策樹決定
                if (address != jsonArray[idx].depend) {
                    device.addDepend(jsonArray[idx].depend)
                } else if (address == jsonArray[idx].depend) {
                    device.addDepend(jsonArray[idx].depend);
                    device.addAction(jsonArray[idx].depend, jsonArray[idx].action);    
                }
            }
        }
    }
    return devices
}

function registerTriggerDevice(devices) {
    triggerDevices = []
    deviceObjs = {}
    for(index in devices) {
        deviceObjs[devices[index].address()] = devices[index]
        if (devices[index].depends().length > 0) {
            triggerDevices.push(devices[index])
        }
    }
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function Trigger() {
    if(!SystemStatus)
        return;

    mongo.queryFindAll('Lsystem_status', { }, function (result, message) { 
        actionsDevices = {}
        var datas = {}
        for(var index in message) {
            datas[message[index].address] = message[index]  
        }
        for(var index in triggerDevices) {
            for(var i in triggerDevices[index].depends()) {
                var address = triggerDevices[index].depends()[i]
                var dependDevice = deviceObjs[address]
                switch(true) {
                    case '1' == triggerDevices[index].equip().toString():
                        Equip1Trigger(datas, triggerDevices[index], dependDevice)
                    break;
                    case '2' == triggerDevices[index].equip().toString():
                        Equip2Trigger(datas, triggerDevices[index], dependDevice)
                    break;
                    case '3' == triggerDevices[index].equip().toString():
                    break;
                    case '4' == triggerDevices[index].equip().toString():
                    break;
                    case '5' == triggerDevices[index].equip().toString():
                    break;
                }
            }
        }
        for (var count = 0; count < 4; count++) {
            setTimeout(() => {
                for (var address in actionsDevices) {
                    SendControlSignal(address, actionsDevices[address])
                }
            }, 5000 * count + 1000)
        }
    })
}

function SendControlSignal(address, actions) {
    var topics = deviceControl
    for(var i in topics) {
        var topic = 'GIOT-GW/DL/' + topics[i];
        var data = [];
        var obj = {};
        obj.macAddr = address;
        obj.data = actions.signal;
        obj.id = '9A53FD0358FCC60FE99896B902288182';
        obj.extra = {port: 2, txpara: '22'};
        data.push(obj);
        client.publish(topic, JSON.stringify(data));
        console.log('\x1b[32m SYSTEM : Send auto control data ('+ actions.signal +') to ('+ address +') Trigger: ' + TriggerString(actions.column, actions.data, actions.trigger) + ' \x1b[37m');
        SaveActionsToLog(address, actions)
    }
    debug.log('log/Trigger.log', 'SYSTEM : Send auto control data ('+ actions.signal +') to ('+ address +') on (' + actions.timestamp + ')' + ' Trigger: ' + TriggerString(actions.column, actions.data, actions.trigger));
}

function TriggerString(column, data, trigger) {
    dataString = data.toString().length > 0 ? '(' + data + ')' : '' 
    if (trigger[0] == null)
        return column + dataString + ' < ' + trigger[1]
    else if (trigger[1] == null)
        return column + dataString + ' => ' + trigger[0]
    else
        return trigger[0] + ' <= ' + column + dataString + ' < ' + trigger[1]
}

function SaveActionsToLog(address, actions) {
    if (!actionsLog.hasOwnProperty(address)) {
        actionsLog[address] = {control: {level: -1, trigger: '', timestamp: ''}, timer: {level: -1, trigger: '', timestamp: ''}}
    }
    actionsLog[address][actions.type].level = actions.level
    actionsLog[address][actions.type].trigger = actions.trigger
    actionsLog[address][actions.type].timestamp = actions.timestamp
}

function ClearActionsLog() {
    for(var address in actionsLog) {
        actionsLog[address]['control'].level = -1
        actionsLog[address]['timer'].level = -1
        actionsLog[address]['timer'].trigger = ''
        actionsLog[address]['timer'].timestamp = ''
    }
}

function Equip1Trigger(datas, device, depend) {
    var message = datas[device.address()]
    var timestamp = CurrentTimestamp(0)
    var timestampTomorrow = CurrentTimestamp(1)
    var detectColumns = ['power', 'set_temp']
    if (!checkColumnsCanUsed(message, detectColumns) && !checkDeviceCanUsed(datas[depend.address()]))
        return;

    var timeIntervalLock = GetDeviceTimeIntervalLock(device)
    var today = CurrentDate()
    var tomorrow = CurrentDate(1)
    if ((checkTimestampTrigger(timestamp, timeIntervalLock) || checkTimestampTrigger(timestampTomorrow, timeIntervalLock)) && device.address() != depend.address())
        return;

    switch(true) {
        case '1' == depend.equip().toString():
            var actions = device.actions()[device.address()]
            var actionsAry = []
            var triggers = []
            for(var action in actions) {
                if (action == "timer_s") {
                    var rangeEnd = '23:59:59'
                    if (actions.hasOwnProperty('timer_e')) {
                        var timerStartToday = today +" "+ actions[action]
                        var timerStartTomorrow = tomorrow +" "+ actions[action]
                        if (checkTimestampTrigger(timerStartToday, timeIntervalLock) || checkTimestampTrigger(timerStartTomorrow, timeIntervalLock)) {
                            continue;
                        } else if (timerStartToday < timeIntervalLock[0]) {
                            rangeEnd = actions['timer_e'][0]
                        } 
                    }
                    triggers.push({trigger: 'timestamp', range: [today +" "+ actions[action], today +" "+ rangeEnd], action: {power: {data: 1, base: 10, type: 'Inconsistent'}}, interval: 0})
                } else if (action == "timer_e") {
                    var hasChangeDate = actions['timer_e'][0] < actions['timer_e'][1] ? true : false
                    triggers.push({trigger: 'timestamp', range: timeIntervalLock, action: {power: {data: 0, base: 10, type: 'Inconsistent'}}, interval: 70})
                } else if (action == "threshold") {
                    triggers.push({trigger: 'set_temp', base: 16, range: [null, actions[action]], action: {power: {data: 1, base: 10, type: 'Identical'}, set_temp: {data: parseInt(actions[action]), base: 16, type: 'Inconsistent'}}, interval: 70})
                }
            }
            var supremeLevel = -1
            for(var i in triggers) {
                var index = triggers.length - i - 1
                var col = triggers[index].trigger
                var base = triggers[index].base
                var range = triggers[index].range
                var action = triggers[index].action
                var interval = triggers[index].interval
                var signal = ''
                var data = parseInt(datas[depend.address()][col], base)
                var level = index
                if (col == 'timestamp') {
                    data = datas[depend.address()][col]
                    //console.info(timestamp, timestampTomorrow, range, checkTimestampTrigger(timestamp, range) , checkTimestampTrigger(timestampTomorrow, range) , checkIsNecessaryTimerTrigger(device, range) , checkTriggerIsExpired(timestamp, device, col, interval) , checkIsNecessaryTrigger(datas, device, action))
                    if ((checkTimestampTrigger(timestamp, range) || checkTimestampTrigger(timestampTomorrow, range)) && (checkIsNecessaryTimerTrigger(device, range) || (checkTriggerIsExpired(timestamp, device, col, interval) && checkIsNecessaryTrigger(datas, device, action)))) {
                        console.info(triggers[index])
                        signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+message.set_temp+message.temp+message.cost+'ffffffffff';
                        level = Number.MAX_SAFE_INTEGER-1 
                    }
                } else {
                    if (!checkDependCanUsed(datas[depend.address()], col, action))
                        continue

                    if (range[0] == null && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                        if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                            console.info(triggers[index])
                            signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(action.set_temp.base)+message.temp+message.cost+'ffffffffff';
                        }
                        supremeLevel = level
                    } else if (range[1] == null && range[0] <= data && checkTriggerIsExpired(timestamp, device, col, interval)) {
                        if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                            console.info(triggers[index])
                            signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(action.set_temp.base)+message.temp+message.cost+'ffffffffff';
                        }
                        supremeLevel = level
                    } else if (range[0] <= data && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                        if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                            console.info(triggers[index])
                            signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(action.set_temp.base)+message.temp+message.cost+'ffffffffff';
                        }
                        supremeLevel = level
                    }
                }
                if (signal.length > 0) {
                    addActionToDevice(level, device, signal, col, data, range)
                }
            }
        break;
        case '2' == depend.equip().toString():
        break;
        case '3' == depend.equip().toString():
        break;
        case '4' == depend.equip().toString():
            var triggers = [{trigger: 'maxTemp', base: 16, range: [25, null], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, set_temp: {data: message.set_temp, base: 16, type: 'Inconsistent'}}, interval: 70}]
            var supremeLevel = -1
            for(var i in triggers) {
                var index = triggers.length - i - 1
                var col = triggers[index].trigger
                var base = triggers[index].base
                var range = triggers[index].range
                var action = triggers[index].action
                var interval = triggers[index].interval
                var signal = ''
                var data = parseInt(datas[depend.address()][col], base)
                var level = index
                if (!checkDependCanUsed(datas[depend.address()], col, {}))
                    continue
      
                if (range[0] == null && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(base)+message.temp+message.cost+'ffffffffff';
                    }
                    supremeLevel = level
                } else if (range[1] == null && range[0] <= data && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(base)+message.temp+message.cost+'ffffffffff';
                    }
                    supremeLevel = level
                } else if (range[0] <= data && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '11'+action.power.data+message.status+message.wind+message.wind_dir+action.set_temp.data.toString(base)+message.temp+message.cost+'ffffffffff';
                    }
                    supremeLevel = level
                }
           
                if (signal.length > 0) {
                    addActionToDevice(level, device, signal, col, data, range)
                }
            }
        break;
        case '5' == depend.equip().toString():
        break;
    }
}

function Equip2Trigger(datas, device, depend) {
    var message = datas[device.address()]
    var timestamp = CurrentTimestamp()
    var detectColumns = ['power', 'wind']
    if (!checkColumnsCanUsed(message, detectColumns) && !checkDeviceCanUsed(datas[depend.address()]))
        return;

    switch(true) {
        case '1' == depend.equip().toString():
        break;
        case '2' == depend.equip().toString():
        break;
        case '3' == depend.equip().toString():
            var triggers = [{trigger: 'CO2', base: 16, range: [null, 950], action: {power: {data: 0, base: 10, type: 'Inconsistent'}, wind: {data: message.wind, base: 16, type: 'Inconsistent'}}, interval: 70}, 
                            {trigger: 'CO', base: 16, range: [null, 7], action: {power: {data: 0, base: 10, type: 'Inconsistent'}, wind: {data: message.wind, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'HCHO', base: 16, range: [null, 6], action: {power: {data: 0, base: 10, type: 'Inconsistent'}, wind: {data: message.wind, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'CO2', base: 16, range: [950, 1500], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 1, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'CO', base: 16, range: [7, 8], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 1, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'HCHO', base: 16, range: [6, 7], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 1, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'CO2', base: 16, range: [1500, null], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 2, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'CO', base: 16, range: [8, null], action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 2, base: 16, type: 'Inconsistent'}}, interval: 70},
                            {trigger: 'HCHO', base: 16, range: [7, null],action: {power: {data: 1, base: 10, type: 'Inconsistent'}, wind: {data: 2, base: 16, type: 'Inconsistent'}}, interval: 70}]
            var supremeLevel = -1
            for(var i in triggers) {
                var index = triggers.length - i - 1
                var col = triggers[index].trigger
                var base = triggers[index].base
                var range = triggers[index].range
                var action = triggers[index].action
                var interval = triggers[index].interval
                var signal = ''
                var data = parseInt(datas[depend.address()][col], base)
                var level = index
                if (!checkDependCanUsed(datas[depend.address()], col, {}))
                    continue
      
                if (range[0] == null && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '21'+action.power.data+message.status+action.wind.data+'fffffffffffffffff';
                    } 
                    supremeLevel = level
                } else if (range[1] == null && range[0] <= data && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '21'+action.power.data+message.status+action.wind.data+'fffffffffffffffff';
                    } 
                    supremeLevel = level
                } else if (range[0] <= data && data < range[1] && checkTriggerIsExpired(timestamp, device, col, interval)) {
                    if (level >= supremeLevel && checkIsNecessaryTrigger(datas, device, action)) {
                        console.info(triggers[index])
                        signal = '21'+action.power.data+message.status+action.wind.data+'fffffffffffffffff';
                    }
                    supremeLevel = level
                }
           
                if (signal.length > 0) {
                    addActionToDevice(level, device, signal, col, data, range)
                }
            }
        break;
        case '4' == depend.equip().toString():
        break;
        case '5' == depend.equip().toString():
        break;
    }
}

function checkColumnsCanUsed(data, cols) {
    if (typeof data === 'undefined')
        return false

    for(var index in cols) {
        if (typeof data[cols[index]] === 'undefined') 
            return false
    }
    return true
}

function checkDependCanUsed(data, trigger, action) {
    var columns = Object.keys(action)
    columns.push(trigger)
    return checkColumnsCanUsed(data, columns)
}

function checkDeviceCanUsed(data) {
    if (typeof data === 'undefined')
        return false
    return false
}

function checkTimestampTrigger(timestamp, range) {
    if (range[0] == null && timestamp < range[1]) {
        return true;
    } else if (range[1] == null && range[0] <= timestamp) {
        return true;
    } else if (range[0] <= timestamp && timestamp < range[1]) {
        return true;
    }
    return false;
}

function checkIsNecessaryTrigger(datas, device, action) {
    var InconsistentBoolean = false
    var IdenticalBoolean = true
    var address = device.address()
    for(var col in action) {
        switch(true) {
            case 'Inconsistent' == action[col].type:
                if (datas[address][col].toString() != parseInt(action[col].data, action[col].base).toString(action[col].base)) {
                    InconsistentBoolean |= true
                }
            break;
            case 'Identical' == action[col].type:
                if (datas[address][col].toString() != parseInt(action[col].data, action[col].base).toString(action[col].base)) {
                    IdenticalBoolean &= false
                }
            break;
        }
    }
    return InconsistentBoolean && IdenticalBoolean
}

function checkIsNecessaryTimerTrigger(device, trigger) {
    var boolean = false
    var address = device.address()
    if (!actionsLog.hasOwnProperty(address) || (actionsLog.hasOwnProperty(address) && actionsLog[address]['timer'].trigger.toString() != trigger.toString())) {
        boolean = true
    } 
    return boolean
}

function checkTriggerIsExpired(timestamp, device, column, interval) {
    if (interval == 0) 
        return false

    var boolean = false
    var address = device.address()
    var type = (column == 'timestamp') ? 'timer' : 'control'
    var currentTimestamp = new Date(timestamp)

    if (actionsLog.hasOwnProperty(address) && actionsLog[address][type].timestamp.toString().length > 0) {
        var triggerTimestamp = new Date(actionsLog[address][type].timestamp)
        if (parseInt((currentTimestamp.getTime() - triggerTimestamp.getTime()) / 1000) > interval)
            boolean = true
    } else if(actionsLog.hasOwnProperty(address) && actionsLog[address][type].timestamp.toString().length == 0) {
        boolean = true
    } else if (!actionsLog.hasOwnProperty(address)) {
        boolean = true
    }
    return boolean
}

function addActionToDevice(level, device, action, column, data, trigger) {
    var address = device.address()
    var type = (column == 'timestamp') ? 'timer' : 'control'
    if (!actionsDevices.hasOwnProperty(device.address())) {
        actionsDevices[address] = {level: -1, signal: '', type: type, trigger: '', timestamp: ''}
    }
    if (actionsDevices[address].level < level) {
        actionsDevices[address].level = level
        actionsDevices[address].signal = action
        actionsDevices[address].column = column
        actionsDevices[address].data = data
        actionsDevices[address].type = type
        actionsDevices[address].trigger = trigger
        actionsDevices[address].timestamp = CurrentTimestamp()
    }
}

function GetDeviceTimeIntervalLock(device) {
    var address = device.address()
    var rangeEnd = '23:59:59'
    var today = CurrentDate()
    var tomorrow = CurrentDate(1)
    if (typeof device.actions()[address] !== 'undefined') {
        var actions = device.actions()[address]
        if (actions.hasOwnProperty('timer_e')) {
            var hasChangeDate = actions['timer_e'][0] < actions['timer_e'][1] ? true : false
            return [today +" "+ actions['timer_e'][0], (hasChangeDate ? today : tomorrow) +" "+ actions['timer_e'][1]]
        }
    }
    return [today +' 00:00:00', today +' '+ rangeEnd]
}

function CurrentTimestamp(dayShift = 0) {
    var time = new Date()
    time.setDate(time.getDate()+dayShift)
    var timestamp = time.getFullYear()+"-"+pad(time.getMonth()+1, 2)+"-"+pad(time.getDate(), 2)+" "+pad(time.getHours(), 2)+":"+pad(time.getMinutes(), 2)+":"+pad(time.getSeconds(), 2);
    return timestamp;
    
    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
}

function CurrentDate(dayShift = 0) {
    var time = new Date()
    time.setDate(time.getDate()+dayShift)
    var date = time.getFullYear()+"-"+pad(time.getMonth()+1, 2)+"-"+pad(time.getDate(), 2);
    return date;
    
    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }
}