var config = require('./config')
var mqtt = require('mqtt');
var onTopic = []
var onCallback = []
var Company = config.COMPANY
var Random = Math.floor(Math.random() * 1024) + 1  

var options = {
    port: config.CLOUD_MQTT_PORT,
    host: config.CLOUD_MQTT_SERVER,
    clientId: 'guest_'+Company+'_'+Random,
    username: 'guest_'+Company+'_'+Random,
    password: 'guest_'+Company+'_'+Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

Random = Math.floor(Math.random() * 1024) + 1  
var options2 = {
    port: config.CLOUD_MQTT_PORT,
    host: config.CLOUD_MQTT_SERVER_2,
    clientId: 'guest_'+Company+'_'+Random,
    username: 'guest_'+Company+'_'+Random,
    password: 'guest_'+Company+'_'+Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

var client = mqtt.connect(options);
var client2 = mqtt.connect(options2);

module.exports.publish = function (topic, data) {
    client.publish(topic, JSON.stringify(data));
    client2.publish(topic, JSON.stringify(data));
    console.log('Publish (' + JSON.stringify(data) + ') to High Level Broker *1.')
}


module.exports.subscribe = function (topic, callback) {
    onTopic.push(topic)
    onCallback.push(callback)
    client.subscribe(topic);
}

client.on('message', function(topic, message) {
    var index = onTopic.indexOf(topic);
    if(index > -1) {
        onCallback[index](message);
    }
})