var config = require('./config')
var mongo = require('./db/mongoDB')
var ObjectID = require('mongodb').ObjectID;
var mongod = require('./db/mongoDeviceDB')
var express = require('express')
var app = express();
var mqtt = require('mqtt');
var bodyParser = require("body-parser");
var sleep = require('system-sleep');
var isodate = require('iso-date');
var fs = require('fs');
var util = require('util')
var exec = require('child_process').exec,
    child;
util.inspect.defaultOptions.maxArrayLength = null;

app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.static(__dirname));
app.listen(8080);
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var MQTToptions = {
    port: config.CLOUD_MQTT_PORT,
    host: config.CLOUD_MQTT_SERVER,
    clientId: 'guest',
    username: 'guest',
    password: 'guest',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

var MQTTclient = mqtt.connect(MQTToptions);
mongo.init(27017, config.CLOUD_DATABASE, (OpenStatus) => {
    if (OpenStatus) {
        console.log('\x1b[32m mongodb open success \x1b[37m');

        RegisterMongoDeviceEvent();
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
});

function RegisterMongoDeviceEvent() {
    DeviceSystemStatus = false
    mongod.init(27017, config.CLOUD_DEVICE_DATABASE, (result) => {
        if (result) {
            DeviceSystemStatus = true
            console.log('\x1b[32m mongodb device open success \x1b[37m');
        }
    })
}

app.post('/userRegister', function (req, res) {
    let datas = {
        "type": "object"
    }
    let id;
    var email = req.body.email;
    var password = req.body.password;
    var place = req.body.place;
    var companyID = req.body.companyID;
    var exist_email = false;
    mongo.queryFindAll("useraccount", { "email": email }, (result, msg) => {
        for (let i = 0; i < msg.length; i++) { //檢查是否有重複帳號
            if (msg[i].email == email) {
                exist_email = true;
            }
        }

        if (!exist_email) { //判斷帳號是否重複及正確
            mongo.insert("useraccount", {
                "_id": id,
                "email": email,
                "password": password,
                "place": place,
                "companyID": companyID
            }, (result, msg) => {
                console.log('\x1b[33m mongodb insert result   ' + result + '\x1b[37m');
                console.log('\x1b[33m mongodb insert msg   ' + msg + '\x1b[37m');
                datas.type = "success";
                datas.result = [];
                datas.result.push({
                    "email": email,
                    "place": place,
                    "company": companyID
                });
                res.send(datas);
            })
        } else {
            datas.type = "fail";
            res.send(datas);
        }
    })
});

app.post('/login', function (req, res) {
    let datas = {
        "type": "object"
    }
    let id;
    var email = req.body.email;
    var password = req.body.password;
    mongo.queryFindAll("useraccount", { "email": email }, (result, msg) => {
        for (let i = 0; i < msg.length; i++) { //檢查是否有重複帳號
            if (msg[i].password == password) {
                datas.type = "success";
                datas.result = [];
                datas.result.push({
                    "email": msg[i].email,
                    "place": msg[i].place,
                    "company": msg[i].companyID
                });
                res.send(datas);
            } else {
                datas.type = "fail";
                res.send(datas);
            }
        }
    })
});


app.post('/deviceRegister', function (req, res) { //設備註冊與修改更新
    let datas = {
        "type": "object"
    }
    let id;
    var email = req.body.email;
    var address = req.body.address.toLowerCase();
    var o_address = req.body.o_address;
    var equipId = req.body.equipId;
    var place = req.body.place;
    var floor = req.body.floor;
    var location = req.body.location;
    var companyID = req.body.companyID;

    var exist_address = false;

    if (o_address != "" && typeof o_address !== 'undefined') //判斷是否為修改，如為修改，則先砍掉既有設備，再新增
    {
        mongo.remove("device_address", { "macAddress": o_address }, (result, msg) => { })
    }

    mongo.queryFindAll("device_address", { "macAddress": address }, (result, msg) => {
        for (let i = 0; i < msg.length; i++) { //檢查是否有重複帳號
            if (msg[i].macAddress == address) {
                exist_address = true;
            }
        }

        if (!exist_address) { //判斷帳號是否重複及正確
            mongo.insert("device_address", {
                "_id": id,
                "macAddress": address,
                "equipId": equipId,
                "place": place,
                "floor": floor,
                "location": location,
                "companyID": companyID,
                "owner": email
            }, (result, msg) => {
                console.log('\x1b[33m mongodb insert result   ' + result + '\x1b[37m');
                console.log('\x1b[33m mongodb insert msg   ' + msg + '\x1b[37m');
                datas.type = "success";
                datas.result = [];
                datas.result.push({
                    "owner": email
                });
                res.send(datas);
            })

            mongo.queryFindAll("useraccount", { "email": email }, (result, msg) => {
                for (let i = 0; i < msg.length; i++) { //取得companyID
                    if (msg[i].email == email) {
                        MQTTclient.publish(msg[i].companyID, '{"type":"update"}');
                        sleep(500);
                        MQTTclient.publish(msg[i].companyID, '{"type":"update"}');
                    }
                }
            })

        } else {
            datas.type = "fail";
            res.send(datas);
        }
    })
});

app.get('/RequireEquip', function (req, res) {
    mongo.queryFindAll('system_status', {}, function (result, equipments) {
        if (!result || !equipments) console.log('No equipments found');
        else {
            res.send(JSON.stringify(equipments));
        }
        res.end();
    })
});

app.get('/RequireEquipInformation', function (req, res) {
    var jsonToReturn = {};
    var owner = req.query.owner;
    mongo.queryFindAll('device_address', { "owner": owner }, function (result, equipments) {
        if (!result || !equipments) console.log('No device address found');
        else {
            jsonToReturn.address = equipments;
            mongo.queryFindAll('equipment_map', {}, function (result, mappings) {
                if (!result || !mappings) console.log('No equipments map found');
                else {
                    jsonToReturn.map = mappings;
                    res.send(JSON.stringify(jsonToReturn));
                }
                res.end();
            })
        }
    })
});

app.get('/RequireDeviceAddress', function (req, res) {
    var jsonToReturn = {};
    var companyID = req.query.companyID;
    mongo.queryFindAll('device_address', { "companyID": companyID }, function (result, equipments) {
        if (!result || !equipments) console.log('No device address found');
        else {
            jsonToReturn.address = equipments;
            mongo.queryFindAll('equipment_map', {}, function (result, mappings) {
                if (!result || !mappings) console.log('No equipments map found');
                else {
                    jsonToReturn.map = mappings;
                    mongo.queryFindAll('device_depend', { company: companyID }, function (result, depends) {
                        jsonToReturn.depends = depends;
                        res.send(JSON.stringify(jsonToReturn));
                        res.end();
                    })
                }
            })
        }
    })
});

app.get('/EquipDepend', function (req, res) {
    var address = req.query.address;
    var company = req.query.company;
    var depends = req.query.depend;
    var notifys = req.query.notify;
    var timer_s = req.query.timer_s;
    var timer_e = req.query.timer_e;
    var threshold = req.query.threshold;
    dependObjs = []
    for (depend in depends) {
        if (depend != address) {
            dependObjs.push({ address: address, depend: depends[depend], type: 'depend', company: company })
        }
    }
    for (notify in notifys) {
        if (notify != address) {
            dependObjs.push({ address: address, depend: notifys[notify], type: 'notify', company: company })
        }
    }
    selfDepend = {}
    selfDependTrigger = false;
    if (timer_s !== undefined) { selfDepend['timer_s'] = timer_s; selfDependTrigger = true; }
    if (timer_e !== undefined) { selfDepend['timer_e'] = timer_e; selfDependTrigger = true; }
    if (threshold !== undefined) { selfDepend['threshold'] = threshold; selfDependTrigger = true; }
    if (selfDependTrigger) dependObjs.push({ address: address, depend: address, company: company, type: 'depend', action: selfDepend })
    mongo.remove('device_depend', { address: address, company: company }, function (result, message) {
        if (result) {
            if (dependObjs.length > 0) {
                mongo.insert('device_depend', dependObjs, function (result, message) {
                    MQTTclient.publish(company, '{"type":"update"}');
                    res.send('{"type":"update"}');
                    res.end();
                })
            } else {
                MQTTclient.publish(company, '{"type":"update"}');
                res.send('{"type":"update"}');
                res.end();
            }
        } else {
            //console.log('\x1b[31m Remove System Status Error \x1b[37m');
        }
    })
});

app.get('/ChangeStatus', function (req, res) {
    mongo.update('system_status', { _id: ObjectID(req.query.id) }, { $set: { status: req.query.status, time: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') } }, function (result, updated) {
        if (!result || !updated) console.log('Status not updated');
        else {
            res.send('Status updated');
            res.end();
        }
    });
});

app.get('/RequireHistoryEquip', function (req, res) {
    var time = req.query.time;

    mongo.queryFindAll('system_status', { address: req.query.address, timestamp: { $gte: new Date(time) } }, function (result, temperature) {
        if (!result || !temperature) console.log('No temperatures found');
        else {
            res.send(JSON.stringify(temperature));
        }
        res.end();
    });
});


app.get('/EquipControll', function (req, res) {
    var address = req.query.address;
    var power = req.query.power;
    var status = req.query.status;
    var wind = req.query.wind;
    var wind_dir = req.query.wind_dir;
    var set_temp = req.query.set_temp;
    var topic;

    mongo.queryFindAll("device_address", { "macAddress": address }, (result, msg) => {
        if (!result || !msg) console.log('No device address found');
        else {
            topic = msg[0].companyID;
            mongo.queryFindAll("system_status", { "address": address }, (result, msg) => {
                if (!result || !msg) {
                    console.log('No device address found');
                } else {
                    console.log("msg.equipId=" + msg[0].equipId);
                    if (msg[0].equipId == 1) {
                        data = "11" + power + status + wind + wind_dir + set_temp + msg[0].temp + "ffffffffffff";
                    }
                    else if (msg[0].equipId == 2) {
                        data = "21" + power + status + wind + "fffffffffffffffff";
                    }
                    else if (msg[0].equipId == 5) {
                        data = "51" + power + status + wind + "fffffffffffffffff";
                    }
                }
                res.send('data=' + data);
                MQTTclient.publish(topic, '{"address":"' + address.toString() + '","data":"' + data.toString() + '","type":"control"}');
                sleep(1000);
                MQTTclient.publish(topic, '{"address":"' + address.toString() + '","data":"' + data.toString() + '","type":"control"}');
                sleep(1000);
                MQTTclient.publish(topic, '{"address":"' + address.toString() + '","data":"' + data.toString() + '","type":"control"}');
            })
        }
    })
})

app.get('/RequireHistoryData', function (req, res) { //查詢打撈某時段的資料
    var address = req.query.address;
    var rangeStart = new Date(req.query.rangeStart);
    var rangeEnd = new Date(req.query.rangeEnd);

    mongo.queryFindAll("daily_status", { address: req.query.address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
        if (!result || !msg) console.log('No timestamp Data');
        else {
            res.send(JSON.stringify(msg));
        }
        res.end();
    })
});

app.get('/RequireRealHistoryData', function (req, res) { //查詢打撈一天時段的資料
    var address = req.query.address;
    var rangeStart = new Date(req.query.rangeStart);
    var rangeEnd = new Date(req.query.rangeEnd);

    storage = 'history_status_' + CurrentTimestamp(rangeStart) + '_' + address
    mongod.queryFindAll(storage, { address: req.query.address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
        if (result && msg) {
            res.send(JSON.stringify(msg));
        }
        res.end();
    })

    function CurrentTimestamp(time) {
        var time = new Date(time)
        return time.getUTCFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
});

app.get('/RequireHistoryStatistic', function(req, res) { //查詢歷史資料
    var address = req.query.address
    var rangeStart = new Date(req.query.rangeStart);
    var rangeEnd = new Date(req.query.rangeEnd);

    mongo.queryFindAll("daily_status", { address: address, tempType: 1, date: {$exists: true}, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, message) => {
        if (!result || !message) console.log('No history statistic Data');
        else {
            res.send(JSON.stringify(message));
        }
        res.end();
    })
})

app.get('/RequireHistoryRSSI', function(req, res) { //查詢歷史資料
    var address = req.query.address
    var rangeStart = new Date(req.query.rangeStart);
    var rangeEnd = new Date(req.query.rangeEnd);

    storage = 'history_status_' + CurrentTimestamp(rangeStart) + '_' + address
    mongod.queryFindAll(storage, { address: address, rssi: {$exists: true}, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, message) => {
        if (!result || !message) console.log('No History RSSI Data');
        else {
            res.send(JSON.stringify(message));
        }
        res.end();
    })

    function CurrentTimestamp(time) {
        var time = new Date(time)
        return time.getUTCFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
})

app.get('/RequireAccumulate', function (req, res) {
    var address = req.query.address;
    var date = req.query.date;
    mongo.queryFindOne("daily_status", { address: address, date: date, tempType: 1 }, (result, message) => {
        if (!result || !message) console.log('No Statistic Datas');
        else {
            res.send(JSON.stringify({ timestamp: message.timestamp, accumulate: message.accumulate }));
        }
        res.end();
    })
});

app.get('/RequireFilterHours', function (req, res) {
    var address = req.query.address;
    var date = req.query.date;
    mongo.queryFindOne("daily_status", { address: address, date: date, tempType: 1 }, (result, message) => {
        if (!result || !message) console.log('No Statistic Datas');
        else {
            res.send(JSON.stringify({ timestamp: message.timestamp, filterHours: message.filterHours }));
        }
        res.end();
    })
});

app.get('/update', function (req, res) {
    var topic = req.query.topic;
    MQTTclient.publish(topic, '{"type":"update"}');
    res.send('{"type":"update"}');
});

app.post('/ImportStatusDatas', function(req, res) {
    var address = req.body.address;
    var rangeStart = req.body.rangeStart;
    var rangeEnd = req.body.rangeEnd;
    var data = req.body.data;
    var password = req.body.password;
    if (password != '#tecofinal') {
        res.send('{"type":"error"}');
        res.end();
        return
    }
    var time = new Date()
    storage = 'history_status_' + CurrentTimestamp(rangeStart) + '_' + address
    mongod.queryFindAll(storage, { address: address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
        if (result && msg) {
            var json = JSON.stringify(msg)
            var filedir = './storage/'
            var filename = filedir + storage + ['(', rangeStart, '-', rangeEnd, ')'].join('') + ['[', time.getTime(), ']'].join('')
            fs.writeFile(filename + '.json', json, 'utf8', SaveAfter);
        }
    })

    function SaveAfter() {
        mongod.remove(storage, { address: address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
            if (result) {
                var jsonSave = JSON.parse(data)
                var filedir = './storage/temp/'
                var filename = filedir + storage + ['[', time.getTime(), ']'].join('')
                var parameter = [storage, filename + '.json'].join(' ')
                fs.writeFile(filename + '.json', util.inspect(jsonSave), 'utf8', ()=>{ OutputAfter(parameter) });
            }
        })
    }

    function OutputAfter(parameter) {
        child = exec('bash ./storage/temp/import.sh '+parameter, // command line argument directly in string
            function (error, stdout, stderr) {      // one easy function to capture data/errors
                // console.log('stdout: ' + stdout);
                // console.log('stderr: ' + stderr);
                if (error !== null) {
                    res.send('{"type":"error"}');
                    console.log('exec error: ' + error);
                } else {
                    res.send('{"type":"success"}');
                }
                res.end();
            }
        );
    }

    function CurrentTimestamp(time) {
        var time = new Date(time)
        return time.getUTCFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
});

app.post('/ImportDailyDatas', function(req, res) {
    var address = req.body.address;
    var rangeStart = req.body.rangeStart;
    var rangeEnd = req.body.rangeEnd;
    var data = req.body.data;
    var password = req.body.password;
    if (password != '#tecofinal') {
        res.send('{"type":"error"}');
        res.end();
        return
    }
    var time = new Date()
    storage = 'daily_status'
    mongo.queryFindAll(storage, { address: address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
        if (result && msg) {
            var json = JSON.stringify(msg)
            var filedir = './storage/daily/'
            var filename = filedir + storage + ['_', address].join('') + ['(', rangeStart, '-', rangeEnd, ')'].join('') + ['[', time.getTime(), ']'].join('')
            fs.writeFile(filename + '.json', json, 'utf8', SaveAfter);
        }
    })

    function SaveAfter() {
        mongo.remove(storage, { address: address, timestamp: { $gte: new Date(rangeStart), $lte: new Date(rangeEnd) } }, (result, msg) => {
            if (result) {
                var jsonSave = JSON.parse(data)
                var filedir = './storage/temp/'
                var filename = filedir + storage + ['_', address].join('') + ['[', time.getTime(), ']'].join('')
                var parameter = [storage, filename + '.json'].join(' ')
                fs.writeFile(filename + '.json', util.inspect(jsonSave), 'utf8', ()=>{ OutputAfter(parameter) });
            }
        })
    }

    function OutputAfter(parameter) {
        child = exec('bash ./storage/temp/importDaily.sh '+parameter, // command line argument directly in string
            function (error, stdout, stderr) {      // one easy function to capture data/errors
                // console.log('stdout: ' + stdout);
                // console.log('stderr: ' + stderr);
                if (error !== null) {
                    res.send('{"type":"error"}');
                    console.log('exec error: ' + error);
                } else {
                    res.send('{"type":"success"}');
                }
                res.end();
            }
        );
    }

    function CurrentTimestamp(time) {
        var time = new Date(time)
        return time.getUTCFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
});

