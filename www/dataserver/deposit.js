'use strict'

class Deposit {
    constructor(deviceJson) {
        this.address = deviceJson.address;
        this.equipId = deviceJson.equipId;
        this.date = deviceJson.date;
        this.timestamp = deviceJson.timestamp;
        this.accumulate = deviceJson.accumulate;
        this.data = deviceJson.data;
        this.dataStatus = deviceJson.dataStatus
    }

    data() {
        return {address: this.address,
                equipId: this.equipId,
                date: this.date,
                timestamp: this.timestamp,
                accumulate: this.accumulate,
                data: this.data,
                dataStatus: this.dataStatus}
    }

    status() {
        return this.dataStatus
    }

    status(data) {
        this.dataStatus = data
    }

    check(item) {
        return typeof item !== 'undefined'
    }
}

module.exports = Deposit