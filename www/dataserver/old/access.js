var mongoDB = require('./db/mongoDB')
var express = require('express')
var app = express();
app.use(express.static(__dirname));
app.listen(8080);
app.use(function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

mongoDB.init(27017, 'dataserver', (resultDB) => {
    if (resultDB) {
        console.log('\x1b[32m mongodb open success \x1b[37m');
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
});

var ObjectID = require('mongodb').ObjectID;

app.get('/RequireEquip', function (req, res) {
    mongoDB.queryFindAll('system_status', {}, function (result, equipments) {
        if (!result || !equipments) console.log('No equipments found');
        else {
            res.send(JSON.stringify(equipments));
        }
        res.end();
    })
});

app.get('/ChangeStatus', function(req, res) {
    mongoDB.update('system_status', {_id: ObjectID(req.query.id)}, {$set: {status: req.query.status, time: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') }}, function(result, updated) {
      if( !result || !updated ) console.log('Status not updated');
      else {
          res.send('Status updated');
          res.end();
      }
    });
});

app.get('/request', function(req, res) {
    var time = '0';
    if(req.query.time !== undefined)
        time = req.query.time;

    mongoDB.queryFindAll('temperature', {time: {$gte: time}}, function (result, temperature) {
        if (!result || !temperature) console.log('No temperatures found');
        else {
            res.send(JSON.stringify(temperature));
        }
        res.end();
    });
});