(function umd(root, name, factory) {
    'use strict';

    if ('function' === typeof define && define.amd) {
        // AMD. Register as an anonymous module.
        define(name, ['jquery'], factory);
    } else {
        // Browser globals
        root[name] = factory();
    }
}(this, 'ReportOverviewModule', function UMDFactory() {
    'use strict';

    var ReportOverview = ReportOverviewConstructor;

    reportCircleGraph();

    return ReportOverview;

    function ReportOverviewConstructor(options) {

        var factory = {
            init: init,
            updatePieChart: updatePieChart,
            updateLineChart: updateLineChart,
            getLineChartTime: getCurrentTime,
            getAddress: classNameToAddress
        };

        var _elements = {
            $element: options.element,
            $data: options.data,
            $location: options.address,
            $map: options.map,
            $chart: {},
            $time: {},
            $address: {}
        };

        // reformat variables
        var formats = {}
        _elements.$location.forEach(function (element) {
            formats[element.macAddress] = element.location;
            _elements.$location = formats;
        }, this)
        var formats = {}
        _elements.$map.forEach(function (element) {
            formats[element.equipId] = element.equipName;
            _elements.$map = formats;
        }, this)

        init();

        return factory;

        function init() {
            console.info("Init");

            _elements.$element.append($(getTemplateTitleString()));
            _elements.$data.forEach(function (element) {
                console.info(element);
                switch (Number(element.equipId)) {
                    case 1:
                        initPieChart(element);
                        break;
                    case 2:
                        initPieChart(element);
                        break;
                }
            }, this);

            _elements.$data.forEach(function (element) {
                switch (Number(element.equipId)) {
                    case 1:
                        initLineChart(element, '室溫');
                        break;
                    case 2:
                        initLineChart(element, '室溫');
                        break;
                }
            }, this);
        }

        function initPieChart(element) {
            var className = 'device_' + element.address + '_piechart';
            var title = _elements.$location[element.address] + _elements.$map[element.equipId];
            $('.row').append($(getPieChartTemplateString(className, title, element.timestamp.split(' ')[0], element.timestamp.split(' ')[1])))
            _elements.$address[className] = element.address;
            _elements.$chart[className] = $('\.' + className).percentCircle({
                width: 130,
                trackColor: '#ececec',
                barColor: '#f5ab34',
                titleColor: '#f5ab34',
                numberColor: '#f5ab34',
                barWeight: 5,
                endPercent: parseInt(element.temp, 16) / 100,
                numberTitle: "室溫",
                number: parseInt(element.temp, 16),
                fps: 60
            });
        }

        function initLineChart(element, chartTitle) {
            var className = 'device_' + element.address + '_linechart';
            var title = _elements.$location[element.address] + _elements.$map[element.equipId];
            $('.row').append($(getLineChartTemplateString(className, title, element.timestamp.split(' ')[0], element.timestamp.split(' ')[1])))
            _elements.$time[className] = element.timestamp;
            _elements.$address[className] = element.address;
            _elements.$chart[className] = $('\.' + className).lineChart({
                className: className,
                title: chartTitle,
                dataPoints: [{ x: new Date(element.timestamp), y: parseInt(element.temp, 16) }]
            });
        }

        function updatePieChart(elements) {
            elements.forEach(function (element) {
                var className = 'device_' + element.address + '_piechart';
                var options = {
                    number: parseInt(element.temp, 16),
                    endPercent: parseInt(element.temp, 16) / 100
                }
                _elements.$chart[className].update(options);
                $('\.' + className + '_date').html(element.timestamp.split(' ')[0]);
                $('\.' + className + '_timestamp').html(element.timestamp.split(' ')[1]);
            }, this);
        }

        function updateLineChart(elements) {
            elements.forEach(function (element) {
                var className = 'device_' + element.address + '_linechart';
                var options = { dataPoints: { x: new Date(element.timestamp), y: parseInt(element.temp, 16) } };
                _elements.$time[className] = element.timestamp;
                _elements.$chart[className].update(options);
                $('\.' + className + '_date').html(element.timestamp.split(' ')[0]);
                $('\.' + className + '_timestamp').html(element.timestamp.split(' ')[1]);
            }, this);
        }

        function getCurrentTime(className) {
            if (_elements.$time[className]) {
                return _elements.$time[className];
            }
        }

        function classNameToAddress(className) {
            if (_elements.$address[className]) {
                return _elements.$address[className];
            }
        }

        function getPieChartTemplateString(device, title, date = '', timestamp = '') {
            return [
                '<div class="report-statistic-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class="box-content {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '<div class="box-foot">',
                '<span class="arrow arrow-up"></span>',
                '<div class="box-foot-left">日期<br><span class="box-foot-stats"><strong class="{{device}}_date">{{date}}</strong></span></div>'.replace(/{{date}}/, date).replace(/{{device}}/, device),
                '<span class="arrow arrow-down"></span>',
                '<div class="box-foot-right">時間<br><span class="box-foot-stats"><strong class="{{device}}_timestamp">{{timestamp}}</strong></span></div>'.replace(/{{timestamp}}/, timestamp).replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getLineChartTemplateString(device, title, date = '', timestamp = '') {
            return [
                '<div class="report-linechart-box block" name="{{device}}">'.replace(/{{device}}/, device),
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class="box-content {{device}}">'.replace(/{{device}}/, device),
                '<div id="chartContainer_{{device}}" style="height: 300px; width: 100%;"></div>'.replace(/{{device}}/, device),
                '</div>',
                '<div class="box-foot">',
                '<span class="arrow arrow-up"></span>',
                '<div class="box-foot-left">日期<br><span class="box-foot-stats"><strong class="{{device}}_date">{{date}}</strong></span></div>'.replace(/{{date}}/, date).replace(/{{device}}/, device),
                '<span class="arrow arrow-down"></span>',
                '<div class="box-foot-right">時間<br><span class="box-foot-stats"><strong class="{{device}}_timestamp">{{timestamp}}</strong></span></div>'.replace(/{{timestamp}}/, timestamp).replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getTemplateTitleString() {
            return [
                '<div>',
                '<h2>System Report Overview</h2>',
                '<div class="row">',
                '</div>'
            ].join('');
        }
    }

    function reportCircleGraph() {

        $.fn.percentCircle = function pie(options) {

            var settings = $.extend({
                width: 130,
                trackColor: '#fff',
                barColor: '#fff',
                titleColor: '#fff',
                numberColor: '#fff',
                barWeight: 5,
                startPercent: 0,
                endPercent: 1,
                numberTitle: '',
                number: '',
                fps: 60
            }, options);

            this.css({
                width: settings.width,
                height: settings.width
            });

            var _this = this,
                canvasWidth = settings.width,
                canvasHeight = canvasWidth,
                id = $('canvas').length,
                canvasElement = $('<canvas id="' + id + '" width="' + canvasWidth + '" height="' + canvasHeight + '"></canvas>'),
                canvas = canvasElement.get(0).getContext('2d'),
                centerX = canvasWidth / 2,
                centerY = canvasHeight / 2,
                radius = settings.width / 2 - settings.barWeight / 2,
                counterClockwise = false,
                fps = 1000 / settings.fps,
                update = 0.01;

            this.angle = settings.startPercent;

            this.drawInnerArc = function (startAngle, percentFilled, color) {
                var drawingArc = true;
                canvas.beginPath();
                canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
                canvas.strokeStyle = color;
                canvas.lineWidth = settings.barWeight - 2;
                canvas.stroke();
                drawingArc = false;
            };

            this.drawOuterArc = function (startAngle, percentFilled, color) {
                var drawingArc = true;
                canvas.beginPath();
                canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
                canvas.strokeStyle = color;
                canvas.lineWidth = settings.barWeight;
                canvas.lineCap = 'round';
                canvas.stroke();
                drawingArc = false;
            };

            this.drawNumberTitle = function (text, col, row) {
                var drawingText = true;
                var font = "23px Microsoft JhengHei";
                canvas.font = font;
                canvas.fillStyle = settings.titleColor;
                canvas.fillText(text, col - this.textWidth(text, font) / 2, row);
                drawingText = false;
            }

            this.drawNumber = function (number, col, row) {
                var drawingText = true;
                var font = "17px Microsoft JhengHei";
                canvas.font = font;
                canvas.fillStyle = settings.numberColor;
                canvas.fillText(number, col - this.textWidth(number, font) / 2, row);
                drawingText = false;
            }

            this.fillChart = function (stop) {
                //console.info(_this.angle.toPrecision(2) +":"+ stop.toPrecision(2))
                if (_this.angle.toPrecision(2) != stop.toPrecision(2)) {
                    var loop = setInterval(function () {
                        canvas.clearRect(0, 0, canvasWidth, canvasHeight);
                        _this.drawInnerArc(0, 360, settings.trackColor);
                        _this.drawOuterArc(settings.startPercent, _this.angle, settings.barColor);
                        _this.drawNumberTitle(settings.numberTitle, canvasWidth / 2, canvasHeight / 2);
                        _this.drawNumber(settings.number, canvasWidth / 2, canvasHeight / 2 + 20);

                        _this.angle += update * (_this.angle < stop ? 1 : -1);
                        if (_this.angle.toPrecision(2) == stop.toPrecision(2)) {
                            clearInterval(loop);
                        }
                    }, fps);
                }
            };

            this.update = function (options) {
                settings = $.extend(settings, options);
                this.fillChart(settings.endPercent);
            };

            this.fillChart(settings.endPercent);
            this.append(canvasElement);
            return this;
        };

        $.fn.lineChart = function linechart(options) {
            var settings = $.extend({
                title: '',
                interval: 10,
                type: 'spline',
                dataPoints: []
            }, options);

            var _this = this;
            var chart = new CanvasJS.Chart("chartContainer_" + options.className, {
                title: {
                    text: settings.title,
                    fontSize: '17',
                    fontFamily: 'Microsoft JhengHei'
                },
                axisY: {
                    interval: settings.interval,
                    includeZero: false
                },
                data: [{
                    type: settings.type,
                    dataPoints: settings.dataPoints
                }]
            });

            this.update = function (options) {
                //chart.options.data[0].dataPoints = [];
                //console.info(options.dataPoints);
                settings.dataPoints.push(options.dataPoints);
                chart.render();
            }

            chart.render();
            return this;
        }

        $.fn.textWidth = function (text, font) {
            if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
            return $.fn.textWidth.fakeEl.width();
        };
    }

    function getMockData() {
        return {
            date: '2014-12-01',
            sentTotal: 4120,
            delivered: 3708,
            opened: 3090,
            clicked: 2060,
            conversion: 35000,
            conversionEmails: 100

        };
    }

}));
