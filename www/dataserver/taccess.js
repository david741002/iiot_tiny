var mongoDB = require('./db/mongoDB')
var mongod = require('./db/mongoDeviceDB')
var express = require('express')
var bodyParser = require('body-parser')
var app = express();
app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.static(__dirname));
app.listen(8080);
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

mongod.init(27017, 'IAQ_DEVICE_Db', (resultDB) => {
    if (resultDB) {
        console.log('\x1b[32m mongodb open success \x1b[37m');
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
});

var ObjectID = require('mongodb').ObjectID;

app.get('/RequireEquip', function (req, res) {
    mongoDB.queryFindAll('Lsystem_status', {}, function (result, equipments) {
        if (!result || !equipments) console.log('No equipments found');
        else {
            res.send(JSON.stringify(equipments));
        }
        res.end();
    })
});

app.get('/RequireEquipInfomation', function (req, res) {
    var jsonToReturn = {}
    mongoDB.queryFindAll('Ldevice_address', {}, function (result, equipments) {
        if (!result || !equipments) console.log('No device address found');
        else {
            jsonToReturn.address = equipments;
            mongoDB.queryFindAll('Lequipment_map', {}, function (result, mappings) {
                if (!result || !mappings) console.log('No equipments map found');
                else {
                    jsonToReturn.map = mappings;
                    res.send(JSON.stringify(jsonToReturn));
                }
                res.end();
            })
        }
    })
});



app.get('/ChangeStatus', function (req, res) {
    mongoDB.update('Lsystem_status', { _id: ObjectID(req.query.id) }, { $set: { status: req.query.status, time: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') } }, function (result, updated) {
        if (!result || !updated) console.log('Status not updated');
        else {
            res.send('Status updated');
            res.end();
        }
    });
});

app.get('/RequireLocalRealHistoryData', function (req, res) { //查詢打撈一天時段的資料
    var address = req.query.address;
    var rangeStart = new Date(req.query.rangeStart);
    //var rangeEnd = new Date(req.query.rangeEnd);

    storage = 'Lhistory_status_' + CurrentTimestamp(rangeStart) + '_' + address
    mongod.queryFindAll(storage, { address: req.query.address }, (result, msg) => {
        if (result && msg) {
console.log("11111111111="+JSON.stringify(msg));
            res.send(JSON.stringify(msg));
        }
        res.end();
    })

    function CurrentTimestamp(time) {
        var time = new Date(time)
        return time.getUTCFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
});

app.get('/RequireHistoryEquip', function (req, res) {
    var time = '0';
    if (req.query.time !== undefined)
        time = req.query.time;

    mongoDB.queryFindAll('Lsystem_status', { address: req.query.address, timestamp: { $gte: time } }, function (result, temperature) {
        if (!result || !temperature) console.log('No temperatures found');
        else {
            res.send(JSON.stringify(temperature));
        }
        res.end();
    });
});

app.get('/EquipControl', function (req, res) {
    var address = req.query.address;
    var company = req.query.company;
    var depends = req.query.depend;
    var timer_s = req.query.timer_s;
    var timer_e = req.query.timer_e;
    var threshold = req.query.threshold;
    dependObjs = []
    for(depend in depends) {
        if (depend != address) {
            dependObjs.push({address: address, depend: depends[depend], company: company})
        }
    }
    selfDepend = {}
    selfDependTrigger = false;
    if (timer_s !== undefined) { selfDepend['timer_s'] = timer_s; selfDependTrigger = true; }
    if (timer_e !== undefined) { selfDepend['timer_e'] = timer_e; selfDependTrigger = true; }
    if (threshold !== undefined) { selfDepend['threshold'] = threshold; selfDependTrigger = true; }
    if (selfDependTrigger) dependObjs.push({address: address, depend: address, company: company, action: selfDepend})
    mongoDB.remove('device_depend', { address: address, company: company }, function (result, message) {
        if (result) {
            if (dependObjs.length > 0)
                mongoDB.insert('device_control', dependObjs, function (result, message) { })
        } else {
            //console.log('\x1b[31m Remove System Status Error \x1b[37m');
        }
    })
});

app.get('/RequireControl', function(req, res) {
    var company = req.query.company;
    mongoDB.queryFindAll('device_depend', { company: company }, function (result, message) {
        res.send(JSON.stringify(message));
        res.end();
    })
});
