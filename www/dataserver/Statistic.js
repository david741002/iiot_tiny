var config = require('./config')
var Deposit = require('./deposit')
var Device = require('./device')
var mongo = require('./db/mongoDB')
var event = require('./js/event')
var debug = require('./js/debug')
var hmqtt = require('./H_MQTT')
// var h2mqtt = require('./H2_MQTT')
var mqtt = require('mqtt');

var interval = 600 //s
var timeout = 1 //s

var Random = Math.floor(Math.random() * 1024) + 1
var options = {
    port: config.LOCAL_MQTT_PORT,
    host: config.LOCAL_MQTT_SERVER,
    clientId: 'guest_' + Random,
    username: 'guest_' + Random,
    password: 'guest_' + Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

// Global Variables
var statisticStatus = {}
var deviceObjs = {}
var notifyDevices = []
var notifyStatus = []
var timer = null
var client = null
var SystemStatus = false
var SystemIdle = true
var SystemStatusAry = [false]
var SystemPrepareCheck = false
var SystemInit = true
var FetchRangeBefore = -30 // 歷史資料索引到幾天之前
mongo.init(27017, config.LOCAL_DATABASE, (result) => { //DB init
    if (result) {
        console.log('\x1b[32m mongodb open success \x1b[37m');

        registerSystemReadyEvent()
        registerStatisticEvent()

        // System first request devices information and submit SystemReady event
        event.emit('SystemPrepare');
        requestDeviceDependInformation()
        timer = setInterval(Statistic, interval * 1000)

        // Mqtt Connection
        client = mqtt.connect(options);
        client.subscribe(config.LOCAL_MQTT_TOPIC);
        client.on('message', function (topic, message) {
            if (!IsJsonString(message))
                return;

            if (!SystemIdle || !SystemStatusAry.every(SystemStatusBoolCheck))
                setTimeout(update, 3000)
            else
                update()

            function update() {
                var json = JSON.parse(message);
                if (json.type == 'update') {
                    event.emit('SystemPrepare');
                    requestDeviceDependInformation()
                }
            }
        })

        // System First Startup with short timeout
        setTimeout(Statistic, 1000)

        function registerSystemReadyEvent() {
            event.on('SystemReady', function () {
                SystemIdle = true
            })

            event.on('SystemPrepare', function () {
                SystemStatusAry = SystemStatusAry.map(() => { return false })
            })

            event.on('SystemDoing', function () {
                SystemIdle = false
            })
        }

        function registerStatisticEvent() {
            event.on('StatisticReady', function (day) {
                if (statisticStatus.hasOwnProperty(day)) {
                    statisticStatus[day] = true;
                    //console.info('Ready ', day)

                    var isReady = true
                    for (var day in statisticStatus) {
                        if (!statisticStatus[day]) {
                            isReady = false
                            //console.info('Statistic ', day, ' is not Ready')
                        }
                    }
                    if (isReady) {
                        event.emit('SystemReady')
                    }
                }
            })
        }
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
})

function requestDeviceDependInformation() {
    mongo.queryFindAll('Ldevice_address', {}, function (result, devices) {
        if (result) {
            var devicesAry = createDevices(devices)

            mongo.queryFindAll('Ldevice_depend', {}, function (result, depends) {
                if (result) {
                    devicesAry = createDeviceDepends(depends, devicesAry)
                    registerNotifyDevice(devicesAry)
                    //console.info(notifyDevices)
                    //console.info(deviceObjs)
                    SystemStatusAry[0] = true;
                }
            })
        }
    })
}

function SystemStatusBoolCheck(bool) {
    return bool
}

function createDevices(jsonArray) {
    var devices = []
    for (index in jsonArray) {
        device = new Device(jsonArray[index]);
        devices.push(device);
    }
    return devices
}

function createDeviceDepends(jsonArray, devices) {
    for (index in devices) {
        device = devices[index]
        device.clearNotify()
        address = device.address()
        for (idx in jsonArray) {
            if (jsonArray[idx].type == 'notify' && address == jsonArray[idx].depend) {
                // 僅找出需要被 notify 的設備, 不支援自己 notify self
                if (address != jsonArray[idx].address) {
                    device.addNotify(jsonArray[idx].address)
                }
            }
        }
    }
    return devices
}

function registerNotifyDevice(devices) {
    notifyDevices = []
    deviceObjs = {}
    for (index in devices) {
        deviceObjs[devices[index].address()] = devices[index]
        if (devices[index].notifys().length > 0) {
            notifyDevices.push(devices[index])
        }
    }
}

function Recovery() {
    setTimeout(() => {
        event.emit('SystemReady')
    }, timeout * 1000)
}

function createDeposit(jsonArray) {
    var deposits = {}
    for (index in jsonArray) {
        if (!deposits.hasOwnProperty(jsonArray[index].address)) {
            deposit = new Deposit(jsonArray[index]);
            deposits[deposit.address] = deposit
        }
    }
    return deposits
}

function SplitDatasByDay(jsonArray, day) {
    var historys = {}
    var todays = []
    for (index in jsonArray) {
        if (jsonArray[index].date == day) {
            todays.push(jsonArray[index])
        } else if (!historys.hasOwnProperty(jsonArray[index].address)) {
            historys[jsonArray[index].address] = [jsonArray[index]]
        } else {
            historys[jsonArray[index].address].push(jsonArray[index])
        }
    }
    return { historys: historys, todays: todays }
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function Statistic() {
    if (!SystemIdle || !SystemStatusAry.every(SystemStatusBoolCheck)) {
        Recovery()
        return;
    }

    DoInitStatistic()
}

function DoInitStatistic() {
    event.emit('SystemDoing')
    mongo.queryFindAll('Lsystem_deposit', {}, function (result, message) {
        if (result) {
            message.sort(function (a, b) {
                var aS = new Date(a.timestamp).getTime()
                var bS = new Date(b.timestamp).getTime()
                return bS - aS

            })
            if (message.length == 0) {
                Recovery()
                return
            }
console.log('------------------------------------------------------------');
            // 從前面往後處理資料  
            var shiftTimestamp = new Date(message[0].timestamp) //取最新的時間
            shiftTimestamp.setUTCMinutes(shiftTimestamp.getUTCMinutes() - 10)
            mongo.queryFindAll('Lsystem_deposit', { timestamp: { $lte: new Date(shiftTimestamp.toISOString()) } }, function (result, message) {
console.log('************************************************************');
                if (result) {
                    if (message.length == 0) {
                        Recovery()
                        return;
                    }

                    message.sort(function (a, b) {
                        var aS = new Date(a.timestamp).getTime()
                        var bS = new Date(b.timestamp).getTime()
                        return aS - bS
                    })
                    var days = {}
                    var fetchRange = {}
                    for (var index in message) {
                        var time = new Date(message[index].timestamp)
                        var day = ToLocalDate(time)
                        if (!days.hasOwnProperty(day)) {
                            days[day] = {}
                            fetchRange[day] = []
                            var shift = 0
                            while (shift >= FetchRangeBefore) {
                                fetchRange[day].push(ToLocalDate(time, shift))
                                shift -= 1
                            }
                        }

                        if (!days[day].hasOwnProperty(message[index].address)) {
                            days[day][message[index].address] = []
                        }

                        days[day][message[index].address].push(message[index])
                    }

                    statisticStatus = {}
console.log('1111111111111111111111111111111111111111111111111111111111111111111');
                    for (var day in days) {
console.log('2222222222222222222222222222222222222222222222222222222222222222222');
                        statisticStatus[day] = false
                        //console.info(day)
                        FetchStatistic(day, days[day], fetchRange)
                    }
                } else {
                    Recovery()
                    console.log('\x1b[31m Lsystem_deposit shift queryFindAll fail \x1b[37m');
                }
            })
        } else {
            Recovery()
            console.log('\x1b[31m Lsystem_deposit queryFindAll fail \x1b[37m');
        }
    })
}

function FetchStatistic(day, datas, fetchRange) {
console.log('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
    mongo.queryFindAll('Lsystem_statistic', { date: { $in: fetchRange[day] } }, function (result, message) {
        if (result) {
            message.sort(function (a, b) {
                var aA = a.address
                var bA = b.address
                var aS = new Date(a.timestamp).getTime()
                var bS = new Date(b.timestamp).getTime()
                return aA.localeCompare(bA) == 0 ? bS - aS : aA.localeCompare(bA)
            })
            var splits = SplitDatasByDay(message, day)
            var historys = splits.historys
            var todays = splits.todays
            var lastTimestamp = ''
            var deposits = createDeposit(todays)
            for (var address in datas) {
                if (datas[address].length > 0) {
                    // sort by old timestamp to new timestamp
                    datas[address].sort(function (a, b) {
                        var aS = new Date(a.timestamp).getTime()
                        var bS = new Date(b.timestamp).getTime()
                        return aS - bS
                    })
                    lastTimestamp = DoMax(datas[address][datas[address].length - 1].timestamp, lastTimestamp)

                    // if system_statistic has not today datas, then create it using first data
                    var index = -1
                    var notifyInsertion = []

                    for (index in datas[address]) {
                        if (!deposits.hasOwnProperty(address)) {
                            var deposit = CreateStatistic(datas[address][index], historys)
                            if (deposit == undefined) {
                                continue
                            }
                            deposits[address] = deposit
                            index++;
                        } else if (deposits.hasOwnProperty(address)) {
                            deposits[address] = CheckStatisticFormat(deposits[address], datas[address][index], historys)
                        }
                        break;
                    }

                    for (; index < datas[address].length; index++) {
                        DoStatisticReady(datas[address][index], deposits)
                    }

                }
            }
                                                            
            // 現階段為處理資料與資料之間的溝通, 例如濾網的損耗計算
            StatisticAfter(deposits)

            var insertions = []
            var uploadInsertions = []
            //console.log(JSON.stringify(deposits));
            for (var address in deposits) {
                insertions.push(deposits[address])

                // dataStatus 不需要送到雲端儲存
                var clone = JSON.parse(JSON.stringify(deposits[address]));
                delete clone.dataStatus
                uploadInsertions.push(clone)
            }
            mongo.remove('Lsystem_statistic', { date: day }, function (result, message) {
                //console.log('-----------------')
                //console.log(day + JSON.stringify(message));
                if (result) {
                    mongo.insert('Lsystem_statistic', insertions, function (result, message) {
                        if (result) {
                            for (var address in deposits) {
                                //console.log(new Date(deposits[address].timestamp));
                                mongo.remove('Lsystem_deposit', { address: deposits[address].address, timestamp: { $lte: new Date(deposits[address].timestamp) } }, function (result, message) { })
                            }

                            hmqtt.publish(config.CLOUD_MQTT_PORT_TOPIC, { type: 'statistic', data: uploadInsertions })
                            // h2mqtt.publish(config.CLOUD_MQTT_PORT_TOPIC, {type: 'statistic', data: uploadInsertions})
                            event.emit('StatisticReady', day)
                        } else {
                            debug.log('log/Statistic.log', 'Error: Insert Lsystem_statistic error (' + insertions + ')');
                        }
                    })
                } else {
                    debug.log('log/Statistic.log', 'Error: Remove Lsystem_statistic error (' + { date: day } + ')');
                }
            })
        } else {
            Recovery()
            console.log('\x1b[31m Lsystem_statistic queryFindAll fail \x1b[37m');
        }
    })
}

function StatisticAfter(deposits) {
    var notifyFrom = {}
    var notifyTo = {}
    for (var address in deposits) {
        if (isNeedNotifyDevice(address)) {
            notifyFrom[address] = deposits[address]
        }
        if (isNotifiedDevice(address)) {
            notifyTo[address] = deposits[address]
        }
    }

    for (var notifyAddr in notifyFrom) {
        var notifyToAry = notifyToDevice(notifyAddr)
        var fromFilterFinish = notifyFrom[notifyAddr].dataStatus.filterFinish
        if (fromFilterFinish > 0) {
            notifyToAry.forEach((notifyToAddr) => {
                if (notifyTo.hasOwnProperty(notifyToAddr)) {
                    // 當前十分鐘和最後十分鐘不處理
                    var toFilterIndex = notifyTo[notifyToAddr].dataStatus.filterIndex
                    var toFilterFinish = notifyTo[notifyToAddr].dataStatus.filterFinish
                    if (toFilterIndex < toFilterFinish) {
                        var index = toFilterIndex + 1
                        for (; index < toFilterFinish; index++) {
                            if (notifyTo[notifyToAddr].dataStatus.filterParameter[index][1] == 0) {
                                continue
                            }
                            var multiply = (value) => {
                                switch (value) {
                                    case 0: return 0.7;
                                    case 1: return 0.8;
                                    case 2: return 0.9;
                                    case 3: return 1;
                                    case 4: return 1.01;
                                    case 5: return 1.1;
                                    case 6: return 1.15;
                                    case 7: return 1.2;
                                    case 8: return 1.25;
                                    case 9: return 1.3;
                                    default: return 1.35
                                }
                            }
                            // 計算方法
                            // 開機分鐘 * pm2 對應到倍率表
                            // PM2.5數值	0~10	11~20	21~30	31~40	41~50	51~60	61~70	71~80	81~90	91~100	>101
                            // 倍率	    0.7	    0.8	    0.9	    1	    1.01	1.1	    1.15	1.2	    1.25	1.3	    1.35 
                            var rate = parseFloat(notifyTo[notifyToAddr].dataStatus.filterParameter[index][0])
                            var pm10 = parseInt((parseFloat(notifyFrom[notifyAddr].dataStatus.filterParameter[index] * 0.01) - 1) / 10)
                            notifyTo[notifyToAddr].data.filterHours -= parseFloat(rate * multiply(pm10) / 60)
                        }
                        if (notifyTo[notifyToAddr].data.filterHours < 0) {
                            notifyTo[notifyToAddr].data.filterHours = 0
                        }
                        notifyTo[notifyToAddr].dataStatus.filterIndex = index - 1
                    }
                }
            })
        }
    }
}

function DoStatisticReady(data, deposits) {
    if (deposits.hasOwnProperty(data.address)) {
        var aS = new Date(deposits[data.address].timestamp).getTime()
        var bS = new Date(data.timestamp).getTime()
        if (aS < bS) {
            deposits[data.address] = DoStatistic(data, deposits[data.address])
        }
    }
}

function DoStatistic(doData, deposit) {
    if (typeof doData === 'undefined')
        return undefined

    // DEBUG CODE FOR 仁愛醫院, WORKING RANGE 9AM - 6PM
    var IsWorking = (timestamp) => {
        var zeroTimestamp = new Date(ToLocaleString(new Date(timestamp), 0, true, true))
        var start = new Date(zeroTimestamp.setUTCHours(zeroTimestamp.getUTCHours() + 9))
        var end = new Date(zeroTimestamp.setUTCHours(zeroTimestamp.getUTCHours() + 9))
        if (start <= timestamp && timestamp <= end) {
            return true
        }
        return false
    }

    if (['99958172', '92021164'].indexOf(config.COMPANY) > -1) {
        if (!(IsWorking(doData.timestamp))) {
            deposit.accumulate--;
        }
    }

    deposit.accumulate++;
    deposit.timestamp = doData.timestamp
    var data = deposit.data
    switch (true) {
        case '01' == doData.equipId.toString():
            data.set_temp = DoMax(parseInt(doData.set_temp, 16), data.set_temp)
            data.max_temp = DoMax(parseInt(doData.temp, 16), data.max_temp)
            data.min_temp = DoMin(parseInt(doData.temp, 16), data.min_temp)
            data.acc_cost = DoCost(parseInt(doData.cost, 16), data.cost, data.acc_cost)
            data.cost = parseInt(doData.cost, 16)
            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                var time = new Date(doData.timestamp)
                var _10minIndex = Math.floor(time.getTime() % 86400000 / 600000)
                deposit.dataStatus.filterParameter[_10minIndex][0] += (doData.power == 1 ? 1 : 0)
                deposit.dataStatus.filterParameter[_10minIndex][1]++
                deposit.dataStatus.filterFinish = _10minIndex
            }
            break;
        case '02' == doData.equipId.toString():
            data.acc_cost = DoCost(parseInt(doData.cost, 16), data.cost, data.acc_cost)
            data.cost = parseInt(doData.cost, 16)
            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                var time = new Date(doData.timestamp)
                var _10minIndex = Math.floor(time.getTime() % 86400000 / 600000)
                deposit.dataStatus.filterParameter[_10minIndex][0] += (doData.power == 1 ? 1 : 0)
                deposit.dataStatus.filterParameter[_10minIndex][1]++
                deposit.dataStatus.filterFinish = _10minIndex
            }
            break;
        case '03' == doData.equipId.toString():
            data.max_HCHO = DoMax(parseInt(doData.HCHO, 16), data.max_HCHO)
            data.min_HCHO = DoMin(parseInt(doData.HCHO, 16), data.min_HCHO)
            data.max_CO2 = DoMax(parseInt(doData.CO2, 16), data.max_CO2)
            data.min_CO2 = DoMin(parseInt(doData.CO2, 16), data.min_CO2)
            data.max_CO = DoMax(parseInt(doData.CO, 16), data.max_CO)
            data.min_CO = DoMin(parseInt(doData.CO, 16), data.min_CO)
            data.max_PM10 = DoMax(parseInt(doData.PM10, 16), data.max_PM10)
            data.min_PM10 = DoMin(parseInt(doData.PM10, 16), data.min_PM10)
            data.max_TVOC = DoMax(parseInt(doData.TVOC, 16), data.max_TVOC)
            data.min_TVOC = DoMin(parseInt(doData.TVOC, 16), data.min_TVOC)
            if (IsWorking(doData.timestamp)) {
                data.avg_HCHO = DoAdd(parseInt(doData.HCHO, 16), data.avg_HCHO)
                data.avg_CO2 = DoAdd(parseInt(doData.CO2, 16), data.avg_CO2)
                data.avg_CO = DoAdd(parseInt(doData.CO, 16), data.avg_CO)
                data.avg_PM10 = DoAdd(parseInt(doData.PM10, 16), data.avg_PM10)
            }

            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                var time = new Date(doData.timestamp)
                var _10minIndex = Math.floor(time.getTime() % 86400000 / 600000)
                deposit.dataStatus.filterParameter[_10minIndex] += parseInt(doData.PM10, 16)
                deposit.dataStatus.filterFinish = _10minIndex
            }
            break;
        case '04' == doData.equipId.toString():
            data.max_temp = DoMax(parseInt(doData.temp, 16), data.max_temp)
            data.min_temp = DoMin(parseInt(doData.temp, 16), data.min_temp)
            data.max_maxTemp = DoMax(parseInt(doData.maxTemp, 16), data.max_maxTemp)
            data.min_maxTemp = DoMin(parseInt(doData.maxTemp, 16), data.min_maxTemp)
            data.max_minTemp = DoMax(parseInt(doData.minTemp, 16), data.max_minTemp)
            data.min_minTemp = DoMin(parseInt(doData.minTemp, 16), data.min_minTemp)
            data.max_diffTemp = DoMax(parseInt(doData.diffTemp, 16), data.max_diffTemp)
            data.min_diffTemp = DoMin(parseInt(doData.diffTemp, 16), data.min_diffTemp)
            data.max_irCO2 = DoMax(parseInt(doData.irCO2, 16), data.max_irCO2)
            data.min_irCO2 = DoMin(parseInt(doData.irCO2, 16), data.min_irCO2)
            data.max_irTVOC = DoMax(parseInt(doData.irTVOC, 16), data.max_irTVOC)
            data.min_irTVOC = DoMin(parseInt(doData.irTVOC, 16), data.min_irTVOC)
            break;
        case '05' == doData.equipId.toString():
            data.max_temp = DoMax(parseInt(doData.temp, 16), data.max_temp)
            data.min_temp = DoMin(parseInt(doData.temp, 16), data.min_temp)
            data.acc_cost = DoCost(parseInt(doData.cost, 16), data.cost, data.acc_cost)
            data.cost = parseInt(doData.cost, 16)
            break;
        case '0a' == doData.equipId.toString():
            data.max_temp = DoMax(parseInt(doData.temp, 16), data.max_temp)
            data.min_temp = DoMin(parseInt(doData.temp, 16), data.min_temp)
            data.max_humidity = DoMax(parseInt(doData.humidity, 16), data.max_humidity)
            data.min_humidity = DoMin(parseInt(doData.humidity, 16), data.min_humidity)
            data.max_produce = DoMax(parseInt(doData.produce, 16), data.max_produce)
            break;
    }
    deposit.data = data
    return deposit
}

function CreateStatistic(data, historys) {
    if (typeof data === 'undefined')
        return undefined

    var deposit = undefined
    var time = new Date(data.timestamp)
    var day = ToLocalDate(time)
    var initStatistic = {
        address: data.address,
        equipId: data.equipId,
        accumulate: 1,
        date: day,
        timestamp: data.timestamp,
        data: {},
        dataStatus: {}
    }
    // console.log(data.equipId.toString()[1]);
    switch (true) {
        case '01' == data.equipId.toString():
            var filterHours = findHistorysFiltersHours(data.address, historys)
            initStatistic.data = {
                set_temp: parseInt(data.set_temp, 16),
                max_temp: parseInt(data.temp, 16),
                min_temp: parseInt(data.temp, 16),
                cost: parseInt(data.cost, 16),
                acc_cost: 0,
                filterHours: filterHours
            }
            if (isNeedNotifyDevice(data.address) || isNotifiedDevice(data.address)) {
                initStatistic.dataStatus = {
                    filterIndex: -1,
                    filterFinish: -1,
                    filterParameter: Array(144).fill().map((v, i) => [0, 0])
                }
            }
            deposit = new Deposit(initStatistic)
            break;
        case '02' == data.equipId.toString():
            var filterHours = findHistorysFiltersHours(data.address, historys)
            initStatistic.data = {
                cost: parseInt(data.cost, 16),
                acc_cost: 0,
                filterHours: filterHours
            }
            if (isNeedNotifyDevice(data.address) || isNotifiedDevice(data.address)) {
                initStatistic.dataStatus = {
                    filterIndex: -1,
                    filterFinish: -1,
                    filterParameter: Array(144).fill().map((v, i) => [0, 0])
                }
            }
            deposit = new Deposit(initStatistic)
            break;
        case '03' == data.equipId.toString():
            initStatistic.data = {
                max_HCHO: parseInt(data.HCHO, 16),
                min_HCHO: parseInt(data.HCHO, 16),
                avg_HCHO: [parseInt(data.HCHO, 16), 1],
                max_CO2: parseInt(data.CO2, 16),
                min_CO2: parseInt(data.CO2, 16),
                avg_CO2: [parseInt(data.CO2, 16), 1],
                max_CO: parseInt(data.CO, 16),
                min_CO: parseInt(data.CO, 16),
                avg_CO: [parseInt(data.CO, 16), 1],
                max_PM10: parseInt(data.PM10, 16),
                min_PM10: parseInt(data.PM10, 16),
                avg_PM10: [parseInt(data.PM10, 16), 1],
                max_TVOC: parseInt(data.TVOC, 16),
                min_TVOC: parseInt(data.TVOC, 16),
            }
            if (isNeedNotifyDevice(data.address) || isNotifiedDevice(data.address)) {
                initStatistic.dataStatus = {
                    filterFinish: -1,
                    filterParameter: Array(144).fill().map((v, i) => 0)
                }
            }
            deposit = new Deposit(initStatistic)
            break;
        case '04' == data.equipId.toString():
            initStatistic.data = {
                max_temp: parseInt(data.temp, 16),
                min_temp: parseInt(data.temp, 16),
                max_maxTemp: parseInt(data.maxTemp, 16),
                min_maxTemp: parseInt(data.maxTemp, 16),
                max_minTemp: parseInt(data.minTemp, 16),
                min_minTemp: parseInt(data.minTemp, 16),
                max_diffTemp: parseInt(data.diffTemp, 16),
                min_diffTemp: parseInt(data.diffTemp, 16),
                max_irCO2: parseInt(data.irCO2, 16),
                min_irCO2: parseInt(data.irCO2, 16),
                max_irTVOC: parseInt(data.irTVOC, 16),
                min_irTVOC: parseInt(data.irTVOC, 16),
            }
            deposit = new Deposit(initStatistic)
            break;
        case '05' == data.equipId.toString():
            initStatistic.data = {
                max_temp: parseInt(data.temp, 16),
                min_temp: parseInt(data.temp, 16),
                cost: parseInt(data.cost, 16),
                acc_cost: 0,
            }
            deposit = new Deposit(initStatistic)
            break;
        case '0a' == data.equipId.toString():
            initStatistic.data = {
                max_temp: parseInt(data.temp, 16),
                min_temp: parseInt(data.temp, 16),
                max_humidity: parseInt(data.humidity, 16),
                min_humidity: parseInt(data.humidity, 16),
                max_produce: parseInt(data.produce, 16)
            }
            deposit = new Deposit(initStatistic)
            break;
    }

    function findHistorysFiltersHours(address, historys) {
        if (historys.hasOwnProperty(address)) {
            for (var index in historys[address]) {
                if (historys[address][index].data.hasOwnProperty('filterHours')) {
                    return historys[address][index].data.filterHours
                }
            }
        }
        return 200.0
    }

    return deposit
}

function CheckStatisticFormat(deposit, data, historys) {
    if (typeof data === 'undefined')
        return undefined

    var time = new Date(data.timestamp)
    var day = ToLocalDate(time)
    var address = data.address

    if (!deposit.check(deposit.address)) deposit.address = data.address
    if (!deposit.check(deposit.equipId)) deposit.equipId = data.equipId
    if (!deposit.check(deposit.accumulate)) deposit.accumulate = 1
    if (!deposit.check(deposit.date)) deposit.date = day
    if (!deposit.check(deposit.timestamp)) deposit.timestamp = data.timestamp
    if (!deposit.check(deposit.data)) deposit.data = {}
    if (!deposit.check(deposit.dataStatus)) deposit.dataStatus = {}
    switch (true) {
        case '01' == data.equipId.toString():
            var filterHours = findHistorysFiltersHours(deposit.address, historys)
            if (!deposit.check(deposit.data.set_temp)) deposit.data.set_temp = parseInt(data.set_temp, 16)
            if (!deposit.check(deposit.data.max_temp)) deposit.data.max_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.min_temp)) deposit.data.min_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.cost)) deposit.data.cost = parseInt(data.cost, 16)
            if (!deposit.check(deposit.data.acc_cost)) deposit.data.acc_cost = 0
            if (!deposit.check(deposit.data.filterHours)) deposit.data.filterHours = filterHours
            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                if (!deposit.check(deposit.dataStatus.filterIndex)) deposit.dataStatus.filterIndex = -1
                if (!deposit.check(deposit.dataStatus.filterFinish)) deposit.dataStatus.filterFinish = -1
                if (!deposit.check(deposit.dataStatus.filterParameter)) deposit.dataStatus.filterParameter = Array(144).fill().map((v, i) => [0, 0])
            }
            break;
        case '02' == data.equipId.toString():
            var filterHours = findHistorysFiltersHours(deposit.address, historys)
            if (!deposit.check(deposit.data.cost)) deposit.data.cost = parseInt(data.cost, 16)
            if (!deposit.check(deposit.data.acc_cost)) deposit.data.acc_cost = 0
            if (!deposit.check(deposit.data.filterHours)) deposit.data.filterHours = filterHours
            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                if (!deposit.check(deposit.dataStatus.filterIndex)) deposit.dataStatus.filterIndex = -1
                if (!deposit.check(deposit.dataStatus.filterFinish)) deposit.dataStatus.filterFinish = -1
                if (!deposit.check(deposit.dataStatus.filterParameter)) deposit.dataStatus.filterParameter = Array(144).fill().map((v, i) => [0, 0])
            }
            break;
        case '03' == data.equipId.toString():
            if (!deposit.check(deposit.data.max_HCHO)) deposit.data.max_HCHO = parseInt(data.HCHO, 16)
            if (!deposit.check(deposit.data.min_HCHO)) deposit.data.min_HCHO = parseInt(data.HCHO, 16)
            if (!deposit.check(deposit.data.avg_HCHO)) deposit.data.avg_HCHO = [parseInt(data.HCHO, 16), 1]
            if (!deposit.check(deposit.data.max_CO2)) deposit.data.max_CO2 = parseInt(data.CO2, 16)
            if (!deposit.check(deposit.data.min_CO2)) deposit.data.min_CO2 = parseInt(data.CO2, 16)
            if (!deposit.check(deposit.data.avg_CO2)) deposit.data.avg_CO2 = [parseInt(data.CO2, 16), 1]
            if (!deposit.check(deposit.data.max_CO)) deposit.data.max_CO = parseInt(data.CO, 16)
            if (!deposit.check(deposit.data.min_CO)) deposit.data.min_CO = parseInt(data.CO, 16)
            if (!deposit.check(deposit.data.avg_CO)) deposit.data.avg_CO = [parseInt(data.CO, 16), 1]
            if (!deposit.check(deposit.data.max_PM10)) deposit.data.max_PM10 = parseInt(data.PM10, 16)
            if (!deposit.check(deposit.data.min_PM10)) deposit.data.min_PM10 = parseInt(data.PM10, 16)
            if (!deposit.check(deposit.data.avg_PM10)) deposit.data.avg_PM10 = [parseInt(data.PM10, 16), 1]
            if (!deposit.check(deposit.data.max_TVOC)) deposit.data.max_TVOC = parseInt(data.TVOC, 16)
            if (!deposit.check(deposit.data.min_TVOC)) deposit.data.min_TVOC = parseInt(data.TVOC, 16)
            if (isNeedNotifyDevice(deposit.address) || isNotifiedDevice(deposit.address)) {
                if (!deposit.check(deposit.dataStatus.filterFinish)) deposit.dataStatus.filterFinish = -1
                if (!deposit.check(deposit.dataStatus.filterParameter)) deposit.dataStatus.filterParameter = Array(144).fill().map((v, i) => 0)
            }
            break;
        case '04' == data.equipId.toString():
            if (!deposit.check(deposit.data.max_temp)) deposit.data.max_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.min_temp)) deposit.data.min_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.max_maxTemp)) deposit.data.max_maxTemp = parseInt(data.maxTemp, 16)
            if (!deposit.check(deposit.data.min_maxTemp)) deposit.data.min_maxTemp = parseInt(data.maxTemp, 16)
            if (!deposit.check(deposit.data.max_minTemp)) deposit.data.max_minTemp = parseInt(data.minTemp, 16)
            if (!deposit.check(deposit.data.min_minTemp)) deposit.data.min_minTemp = parseInt(data.minTemp, 16)
            if (!deposit.check(deposit.data.max_diffTemp)) deposit.data.max_diffTemp = parseInt(data.diffTemp, 16)
            if (!deposit.check(deposit.data.min_diffTemp)) deposit.data.min_diffTemp = parseInt(data.diffTemp, 16)
            if (!deposit.check(deposit.data.max_irCO2)) deposit.data.max_irCO2 = parseInt(data.irCO2, 16)
            if (!deposit.check(deposit.data.min_irCO2)) deposit.data.min_irCO2 = parseInt(data.irCO2, 16)
            if (!deposit.check(deposit.data.max_irTVOC)) deposit.data.max_irTVOC = parseInt(data.irTVOC, 16)
            if (!deposit.check(deposit.data.min_irTVOC)) deposit.data.min_irTVOC = parseInt(data.irTVOC, 16)
            break;
        case '05' == data.equipId.toString():
            if (!deposit.check(deposit.data.max_temp)) deposit.data.max_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.min_temp)) deposit.data.min_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.cost)) deposit.data.cost = parseInt(data.cost, 16)
            if (!deposit.check(deposit.data.acc_cost)) deposit.data.acc_cost = 0
            break;
        case '0a' == data.equipId.toString():
            if (!deposit.check(deposit.data.max_temp)) deposit.data.max_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.min_temp)) deposit.data.min_temp = parseInt(data.temp, 16)
            if (!deposit.check(deposit.data.max_humidity)) deposit.data.max_humidity = parseInt(data.humidity, 16)
            if (!deposit.check(deposit.data.min_humidity)) deposit.data.min_humidity = parseInt(data.humidity, 16)
            if (!deposit.check(deposit.data.max_produce)) deposit.data.max_produce = parseInt(data.produce, 16)
            break;
    }

    function findHistorysFiltersHours(address, historys) {
        if (historys.hasOwnProperty(address)) {
            for (var index in historys[address]) {
                if (historys[address][index].data.hasOwnProperty('filterHours')) {
                    return historys[address][index].data.filterHours
                }
            }
        }
        return 200.0
    }

    return deposit
}

// 需要提醒別人的設備
function isNeedNotifyDevice(address) {
    for (var index in notifyDevices) {
        if (notifyDevices[index].address() == address)
            return true
    }
    return false
}

function notifyToDevice(address) {
    var notifyAry = []
    for (var index in notifyDevices) {
        if (notifyDevices[index].address() == address)
            notifyAry = notifyAry.concat(notifyDevices[index].notifys())
    }
    return notifyAry
}

// 需要別人提醒的設備
function isNotifiedDevice(address) {
    for (var index in notifyDevices) {
        if (notifyDevices[index].notifys().indexOf(address) > -1)
            return true
    }
    return false
}

function notifyFromDevice(address) {
    var notifyAry = []
    for (var index in notifyDevices) {
        if (notifyDevices[index].notifys().indexOf(address) > -1)
            notifyAry.push(notifyDevices[index].address())
    }
    return notifyAry
}

function ToLocaleString(time, dayShift = 0, shiftToZero = false, isoFormat = false) {
    if (isoFormat)
        return ToLocalDate(time, dayShift, isoFormat) + 'T' + ToLocalTimestamp(time, shiftToZero) + 'Z'
    else
        return ToLocalDate(time, dayShift, isoFormat) + ' ' + ToLocalTimestamp(time, shiftToZero)
}

function ToLocalDate(time, dayShift = 0, isoFormat = false) {
    if (dayShift != 0) {
        var shiftTime = new Date(time)
        shiftTime.setUTCDate(shiftTime.getUTCDate() + dayShift)
        if (isoFormat)
            return shiftTime.getUTCFullYear() + "-" + pad(shiftTime.getUTCMonth() + 1, 2) + "-" + pad(shiftTime.getUTCDate(), 2)
        else
            return shiftTime.getUTCFullYear() + "-" + (shiftTime.getUTCMonth() + 1) + "-" + shiftTime.getUTCDate()
    }
    if (isoFormat)
        return time.getUTCFullYear() + "-" + pad(time.getUTCMonth() + 1, 2) + "-" + pad(time.getUTCDate(), 2)
    else
        return time.getUTCFullYear() + "-" + (time.getUTCMonth() + 1) + "-" + time.getUTCDate()
}

function ToLocalTimestamp(time, shiftToZero = false) {
    if (shiftToZero) {
        return '00:00:00'
    }
    return pad(time.getUTCHours(), 2) + ":" + pad(time.getUTCMinutes(), 2) + ":" + pad(time.getUTCSeconds(), 2)
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function DoAdd(a, b) {
    if (typeof b === 'undefined')
        return [a, 1]
    b[0] += a
    b[1] += 1
    return b
}

function DoMax(a, b) {
    return a > b ? a : b
}

function DoMin(a, b) {
    return a > b ? b : a
}

function DoCost(a, b, c) {
    return (a - b) > 0 ? c + (a - b) : c
}
