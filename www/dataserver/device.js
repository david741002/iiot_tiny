'use strict'

class Device {
    constructor(deviceJson) {
        this.macAddress = deviceJson.macAddress;
        this.equipId = deviceJson.equipId;
        this.place = deviceJson.place;
        this.floor = deviceJson.floor;
        this.location = deviceJson.location;
        this.owner = deviceJson.owner;
        this.dependsAry = [];
        this.dependsAction = {};
        this.notifysAry = [];
    }

    data() {
        return {macAddress: this.macAddress,
                equipId: this.equipId,
                place: this.place, 
                floor: this.floor,
                location: this.location,
                owner: this.owner}
    }

    address() {
        return this.macAddress;
    }

    depends() {
        return this.dependsAry;
    }

    notifys() {
        return this.notifysAry;
    }

    actions() {
        return this.dependsAction;
    }

    equip() {
        return this.equipId;
    }

    clearDepend() {
        this.dependsAry.length = 0;
    }

    clearNotify() {
        this.notifysAry.length = 0;
    }

    clearAction() {
        this.dependsAction = {}
    }

    addNotify(address) {
        this.notifysAry.push(address);
    }

    addDepend(address) {
        this.dependsAry.push(address);
    }

    addAction(address, action) {
        this.dependsAction[address] = action;
    }
}

module.exports = Device