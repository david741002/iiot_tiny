var config = require('./config')
var mqtt = require('mqtt');
var request = require("request");
var moment = require('moment');
var hmqtt = require('./H_MQTT')
var mongo = require('./db/mongoDB')
var mongod = require('./db/mongoDeviceDB')
var parser = require('./parser')
var devices = require('./devices')
var event = require('./js/event')
var debug = require('./js/debug')
let id;
let Message;

var Company = config.COMPANY
var Random = Math.floor(Math.random() * 1024) + 1

var options = {
    port: config.LOCAL_MQTT_PORT,
    host: config.LOCAL_MQTT_SERVER,
    clientId: 'guest_' + Company + '_' + Random,
    username: 'guest_' + Company + '_' + Random,
    password: 'guest_' + Company + '_' + Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

var client = null;
var SystemStatus = false;
var DeviceSystemStatus = false;
mongo.init(27017, config.LOCAL_DATABASE, (result) => { //DB init
    if (result) {
        console.log('\x1b[32m mongodb open success \x1b[37m');

        client = mqtt.connect(options);
        SetCloudServerEvent();
        RegisterMongoDeviceEvent();
        RegisterSystemReadyEvent();
        RegisterAddrEvent();
        RegisterDataEvent();
        RegisterTimeSignal()

        devices.setRemote(config.CLOUD_SERVER, config.CLOUD_API, Company);
        devices.setMongoDB(mongo);
        // System first request devices information and submit SystemReady event
        event.emit('SystemPrepare');
        devices.initGWID();
        devices.requestRegisterInformation();

        client.subscribe('#');
        client.on('message', function (topic, message) {
		
	//console.log("************************************************************=="+message);
	
            if (!SystemStatus || !DeviceSystemStatus)
                return;

            if (!IsJsonString(message))
                return;

            //var jsonData = JSON.parse(message)[0];
		var jsonDataArray = JSON.parse(message);
		
//console.log("------------------------------------------------------------"+jsonDataArray.length);

            if (typeof jsonData === undefined)

                return;

            if (topic.indexOf('GIOT-GW/UL') == -1)
                return;
	    for(var i=0; i<jsonDataArray.length; i++){
		var jsonData;
		jsonData = jsonDataArray[i];
//console.log("------------------------------------------------------------M="+jsonData.macAddr);
//console.log("------------------------------------------------------------G="+jsonData.gwid);
            	event.emit('CheckMacAddr', jsonData);
	    }
        });

        function RegisterSystemReadyEvent() {
            event.on('SystemReady', () => {
                client.publish(config.LOCAL_MQTT_TOPIC, JSON.stringify({ 'type': 'update' }))
                SystemStatus = true;
            })

            event.on('SystemPrepare', () => {
                SystemStatus = false;
            })
        }
    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
});

function RegisterMongoDeviceEvent() {
    event.on('DeviceSystemReady', () => {
        DeviceSystemStatus = true
    })

    DeviceSystemStatus = false
    mongod.init(27017, config.LOCAL_DEVICE_DATABASE, (result) => {
        if (result) {
            event.emit('DeviceSystemReady')
            console.log('\x1b[32m mongodb device open success \x1b[37m');
        }
    })
}


function RegisterAddrEvent() {
    event.on('CheckMacAddr', function (jsonData) {
        if (devices.isRegister(jsonData.macAddr)) {
            event.emit('CorrectMacAddr', jsonData);
        } else {
            console.log('\x1b[31m Error: mqtt receive unregister mac address ' + jsonData + '\x1b[37m');
            debug.log('log/MQTT.log', 'Error: mqtt receive unregister mac address (' + jsonData.macAddr + ') data (' + jsonData.data + ')');
        }
    })

    event.on('CorrectMacAddr', function (jsonData) {
        event.emit('CheckDataFormat', jsonData);
    })
}
var multiPackets = [];
function concatPacket(data) {

    var isInsert = false;
    if (multiPackets.length == 0) {
        multiPackets.push([]);
        multiPackets[0].push(data);
        console.log('\x1b[31m' + JSON.stringify(data.parse.data) + ' \x1b[37m');
    }
    else {
        for (let i = 0; i < multiPackets.length; i++) {
            if (multiPackets[i][0].parse.address == data.parse.address) {
                multiPackets[i].push(data);

                //check for packet is full
                if (parseInt(data.parse.packet.charAt(0)) == multiPackets[i].length) {
                    //check for duplicates
                    console.log('\x1b[31m' + JSON.stringify(multiPackets[i][0].parse) + ' \x1b[37m');
                    console.log('\x1b[31m' + JSON.stringify(data.parse) + ' \x1b[37m');
                    var temp = multiPackets[i][0];
                    var isDuplicate = false;
                    // for (let j = 1; j < multiPackets[i].length; j++) {
                    //     if (temp.parse.packet == multiPackets[i][j].parse.packet) {
                    //         isDuplicate = true;
                    //         break;
                    //     }
                    // }
                    for (let j = 0; j < multiPackets[i].length; j++) {
                        for (let s = j + 1; s < multiPackets[i].length; s++){
                            if (multiPackets[i][j].parse.packet == multiPackets[i][s].parse.packet) {
                                isDuplicate = true;
                                break;
                            }
                        }
                    }
                    if (isDuplicate) {
                        //is duplicate
                        // console.log(multiPackets[i]);
                        console.log('\x1b[31m ' + multiPackets[i][0].parse.address + '\'s packet is duplicate \x1b[37m ');
                        multiPackets.splice(i, 1);

                        return '';
                    }

                    multiPackets[i].sort(function (a, b) {
                        return parseInt(a.parse.packet.charAt(1)) - parseInt(b.parse.packet.charAt(1));
                    })

                    //concat data
                    for (let j = 1; j < multiPackets[i].length; j++) {
                        multiPackets[i][0].parse.packet = multiPackets[i][j].parse.packet;
                        multiPackets[i][0].parse.data += multiPackets[i][j].parse.data.substring(4, 22);
                    }
                    data = multiPackets[i][0];
                    data.data.data = data.parse.data;
                    data.data.time = data.parse.timestamp;
                    multiPackets.splice(i, 1);

                    console.log('\x1b[32m ' + data.parse.address + '\'s packet is concatenate finish' + '\x1b[37m');
                    return data;

                }
                isInsert = true;
                break;
            }
        }
        if (isInsert == false) {
            multiPackets.push([]);
            multiPackets[multiPackets.length - 1].push(data);
            return '';
        }
    }
}

function RegisterDataEvent() {
    event.on('CheckDataFormat', function (jsonData) {
        parser.parse(jsonData, function (result, data) {
            if (result) {

                if (data.parse.equipId == 'ff') {
                    //設備請求目前時間
                    console.info('Ready to Send cTime Signal')
                    event.emit('TimeSignal', data, jsonData);
                }
                else if (data.parse.packet.charAt(0) != '1') {

                    let data2 = concatPacket(data);
                    if (data2) {
                        event.emit('CorrectDataFormat', data2);
                        // data2.data.data = data2.parse.data;
                    }
                    else
                        console.info('\x1b[32m Waiting packet ->' + jsonData.macAddr + '\x1b[37m ');
                }
                else
                    event.emit('CorrectDataFormat', data);

            } else {
                console.log('\x1b[31m Error: Parse equipment package error ' + jsonData.macAddr + ') data (' + jsonData.data + ') \x1b[37m');
                debug.log('log/Parse.log', 'Error: Parse equipment package error ' + jsonData.macAddr + ') data (' + jsonData.data + ')');
            }
        })
    })

    event.on('CorrectDataFormat', function (data) {
        mongo.queryFindAll('Lsystem_status', { address: data.parse.address }, function (result, message) {
            if (result && message.length > 0) {
                storage = 'Lhistory_status_' + CurrentTimestamp() + '_' + data.parse.address
                mongod.insert(storage, message, function (result, message) { })
                mongo.insert('Lhistory_status', message, function (result, message) { });
            }

            mongo.remove('Lsystem_status', { address: data.parse.address }, function (result, message) {
                if (result) {
                    mongo.insert('Lsystem_status', data.parse, function (result, message) {
                        data.parse.timestamp = new Date(data.parse.timestamp.replace(/([0-9-]+) ([0-9:]+)/, '$1T$2Z'))
                        mongo.insert('Lsystem_deposit', data.parse, function (result, message) {
                            if (!result) {
                                debug.log('log/Deposit.log', 'Error: Save system_deposit error (' + data.parse + ')');
                            }
                        })
			data.data['CompanyID'] = config.CLOUD_MQTT_DOWN_TOPIC;//上傳雲端封包加入Company ID
                        hmqtt.publish(config.CLOUD_MQTT_UP_TOPIC, data.data);
                        devices.isRegisterGWID(data.data.gwid);
                    })
                } else {
                    console.log('\x1b[31m Remove System Status Error \x1b[37m');
                }
            })
        })
    })

    function CurrentTimestamp() {
        var time = new Date()
        return time.getFullYear() + "_" + pad(time.getMonth() + 1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
}

function SetCloudServerEvent() {
    hmqtt.subscribe(config.CLOUD_MQTT_DOWN_TOPIC, function (message) {
        if (IsJsonString(message)) {
            var json = JSON.parse(message)
            if (json.type == 'update') {
                // console.log('\x1b[32m SYSTEM : Receive device address update message \x1b[37m');
                event.emit('SystemPrepare');
                devices.requestRegisterInformation();
            } else if (json.type == 'control') {
                var topics = devices.gwids();
                for (var index in topics) {
                    var topic = 'GIOT-GW/DL/' + topics[index];
                    var data = [];
                    var obj = {};
                    obj.macAddr = json.address;
                    obj.data = json.data;
                    obj.id = '9A53FD0358FCC60FE99896B902288182';
                    obj.extra = { port: 2, txpara: '22' };
                    data.push(obj);
                    //console.log(data);
                    console.log('\x1b[32m SYSTEM : Send control signal (' + json.data + ') to (' + json.address + ') \x1b[37m');
                    client.publish(topic, JSON.stringify(data));
                }
            }
        }
    });
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function RegisterTimeSignal() {
    event.on('TimeSignal', function (data,jsonData) {
        devices.isRegisterGWID(data.data.gwid);
        var topics = devices.gwids();
        for (var index in topics) {
		if (jsonData.gwid == topics[index] ){
            var topic = 'GIOT-GW/DL/' + topics[index];
            var obj = {};
            var date = new Date();//18 -2 -3
            var hex_minute = (date.getHours() * 60 + date.getMinutes()).toString(16);
            if (hex_minute.length == 2)
                hex_minute = '0000000000000000' + hex_minute;
            else if (hex_minute.length == 1)
                hex_minute = '00000000000000000' + hex_minute;
            else
                hex_minute = '000000000000000' + hex_minute;

            obj.macAddr = data.parse.address;
            obj.data = data.parse.equipId + data.parse.packet + hex_minute;
            console.log(obj.data);
            obj.id = '9A53FD0358FCC60FE99896B902288182';
            obj.extra = { port: 2, txpara: '22' };
            client.publish(topic, JSON.stringify([obj]));
            // console.log('\x1b[32m SYSTEM : Send time data (' + obj.data + ') to (' + obj.macAddr + ') '+'('+JSON.stringify([obj])+')'+ ' \x1b[37m');
            console.log('\x1b[32m SYSTEM : ' + '(' + JSON.stringify([obj]) + ')' + ' \x1b[37m');
            // SaveActionsToLog(address, actions)

            // debug.log('log/MQTT.log', 'SYSTEM : Send time data ('+ actions.signal +') to ('+ address +') on (' + actions.timestamp + ')' + ' Trigger: ' + TriggerString(actions.column, actions.data, actions.trigger));
        }}
    })
}
