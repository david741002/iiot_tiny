var mqtt = require('mqtt');
var event = require('../js/event')
let id;
let Message;

var options = {
    port: 1883,
    //host: '140.124.182.58',
    //host: '52.42.235.206',
    host: 'localhost',
    clientId: 'guest1',
    username: 'guest1',
    password: 'guest1',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

var client = mqtt.connect(options);
var equipData = []
var equipInterval = []
equipData['000000000501035f'] = {equipId: 1, data: 26, range: {min: 5, max: 50}}
equipData['000000000501036f'] = {equipId: 1, data: 26, range: {min: 5, max: 50}}
equipData['000000000501040d'] = {equipId: 3, data: {CO2:500, CO:500}, range: {CO2: {min: 100, max: 900}}}

for(equip in equipData) {
    console.info("create interval in " + equip);
    switch(equipData[equip].equipId) {
    case 1:
        equipInterval[equip] = setInterval(publishEquip_1, 1000, equip);
        break;
    case 3:
        equipInterval[equip] = setInterval(publishEquip_3, 1000, equip);
        break;
    }
}


function publishEquip_1(macAddr) {
    var delta = parseInt(Math.random()*3-1.5)
    if(equipData[macAddr].range.min <= (equipData[macAddr].data+delta) && (equipData[macAddr].data+delta) <= equipData[macAddr].range.max) {
        equipData[macAddr].data += delta
    }
    var c = {equipId:equipData[macAddr].equipId.toString(), op:"0", power:"1", status:"2", wind:"2", director:"1", set_temp:'1A', temp:pad(equipData[macAddr].data.toString(16), 2), cost:"FF", error:"00", remain:"00000000"}
    var data = c.equipId+c.op+c.power+c.status+c.wind+c.director+c.set_temp+c.temp+c.cost+c.error+c.remain;
    var d = {"channel":926625000, "sf":10, "time":(new Date()).toISOString().split('.')[0], "gwip":"140.124.182.68", "gwid":"00001c497b48db58", "repeater":"00000000ffffffff", "systype":5, "rssi":-114.0, "snr":-7.0, "snr_max":-5.0, "snr_min":-8.8, "macAddr":macAddr, "data":data, "frameCnt":1, "fport":1}
    var format = [d]
    //console.info(JSON.stringify(format))
    client.publish('123', JSON.stringify(format), function(result, m) { })
}

function publishEquip_3(macAddr) {
    var delta = parseInt(Math.random()*10-5.0)
    if(equipData[macAddr].range.CO2.min <= (equipData[macAddr].data.CO2+delta) && (equipData[macAddr].data.CO2+delta) <= equipData[macAddr].range.CO2.max) {
        equipData[macAddr].data.CO2 += delta
        equipData[macAddr].data.CO += parseInt(Math.random()*10-5.0)
    }
    var c = {equipId:equipData[macAddr].equipId.toString(), op:'0', HCHO:"0000", CO2:pad(equipData[macAddr].data.CO2, 4), CO:pad(equipData[macAddr].data.CO, 4), PM10:'0000', TVOC:'0000'}
    var data = c.equipId+c.op+c.HCHO+c.CO2+c.CO+c.PM10+c.TVOC;
    var d = {"channel":926625000, "sf":10, "time":(new Date()).toISOString().split('.')[0], "gwip":"140.124.182.68", "gwid":"00001c497b48db58", "repeater":"00000000ffffffff", "systype":5, "rssi":-114.0, "snr":-7.0, "snr_max":-5.0, "snr_min":-8.8, "macAddr":macAddr, "data":data, "frameCnt":1, "fport":1}
    var format = [d]
    //console.info(JSON.stringify(format))
    client.publish('123', JSON.stringify(format), function(result, m) { })
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
/**
 * [{"channel":926625000, "sf":10, "time":"2017-05-26T10:26:23", "gwip":"140.124.182.68", 
 * "gwid":"00001c497b48db58", "repeater":"00000000ffffffff", "systype":5, "rssi":-114.0, 
 * "snr":-7.0, "snr_max":-5.0, "snr_min":-8.8, "macAddr":"000000000501035f", 
 * "data":"1001022826ffffffffffff", "frameCnt":1, "fport":1}]
 * 
 */