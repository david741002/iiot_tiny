var debug

(function umd(root, name, factory) {
    'use strict';

    if ('function' === typeof define && define.amd) {
        // AMD. Register as an anonymous module.
        define(name, ['jquery'], factory);
    } else {
        // Browser globals
        root[name] = factory();
    }
}(this, 'ReportOverviewModule', function UMDFactory() {
    'use strict';

    var ReportOverview = ReportOverviewConstructor;

    reportCircleGraph();

    return ReportOverview;

    function ReportOverviewConstructor(options) {
        var company = localStorage['__user__company'];

        var factory = {
            init: init,
            updatePieChart: updatePieChart,
            updateLineChart: updateLineChart,
            updateContentTable: updateContentTable,
            refreshLineChart: refreshLineChart,
            getLineChartTime: getCurrentTime,
            getAddress: classNameToAddress
        };

        var _elements = {
            $element: options.element,
            $data: options.data,
            $location: options.address,
            $map: options.map,
            $depends: options.depends,
            $floors: {},
            $page: options.page,
            $mode: options.mode,
            $chart: {},
            $time: {},
            $address: {},
            email: options.email,
            place: options.place,
            server: options.server
        };
        console.info(_elements.$data);
        // reformat variables
        var formats = {}
        var floors = {}
        _elements.$location.forEach(function (element) {
            formats[element.macAddress] = { location: element.location, floor: element.floor, place: element.place, equipId: element.equipId, companyID: element.companyID, depends: [], notifys: [], actions: {} };
            floors[element.floor] = parseFloor(element.floor);
        }, this)
        _elements.$depends.forEach(function (element) {
            switch (true) {
                case 'notify' == element.type:
                    formats[element.address]['notifys'].push(element.depend)
                    break;
                case 'depend' == element.type:
                default:
                    formats[element.address]['depends'].push(element.depend)
                    formats[element.address]['actions'][element.depend] = typeof element.action !== 'undefined' ? element.action : {}
                    break;
            }
        }, this)
        _elements.$location = formats;
        _elements.$floors = floors;
        console.info(formats)

        formats = {}
        _elements.$map.forEach(function (element) {
            formats[element.equipId] = element.equipName;
        }, this)
        _elements.$map = formats;
        console.info(formats);

        createFloorButton();
        init();

        return factory;

        function init() {
            console.info("Init");

            var $data = []
            _elements.$data.forEach(function (element, index) {
                if (_elements.$location[element.address] !== undefined) {
                    $('a[href="?floor=' + _elements.$location[element.address].floor + '"]').parent().css('display', 'block')
                    _elements.$data[index]["floor"] = _elements.$location[element.address].floor
                    _elements.$data[index]["location"] = _elements.$location[element.address].location
                    $data.push(_elements.$data[index])
                }
            }, this);
            $data.sort(SortByFloorAndLocation)
            _elements.$data = $data
            console.info(_elements.$data)

            _elements.$element.append($(getTitleTemplateString(_elements.place)));

            if (['config', 'list'].indexOf(_elements.$mode) > -1) {
                _elements.$data.forEach(function (element) {
                    var type = typeToShow(element);
                    //console.info(element.address +":"+ element.equipId +":"+ type)
                    if (type.indexOf('PieChart') > -1 && isShowOn('PieChart', element)) {
                        initPieChart(element);
                    }
                    if (type.indexOf('ContentTable') > -1 && isShowOn('ContentTable', element)) {
                        initContentTable(element);
                    }
                    if (type.indexOf('ConfigTable') > -1 && isShowOn('ConfigTable', element)) {
                        initConfigTable(element);
                    }
                    if (type.indexOf('ControlTable') > -1 && isShowOn('ControlTable', element)) {
                        initControlDependTable(element);
                    }
                    if (type.indexOf('LineChart') > -1 && isShowOn('LineChart', element)) {
                        initLineChart(element);
                    }
                }, this);
            } else if (['register', 'modify'].indexOf(_elements.$mode) > -1) {
                initRegisterTable(_elements.$mode);
            } else if (['history'].indexOf(_elements.$mode) > -1) {
                initHistoryTable(_elements.$mode);
            } else if (['information'].indexOf(_elements.$mode) > -1) {
                _elements.$data.forEach(function (element) {
                    if (_elements.$location.hasOwnProperty(element.address)) {
                        var value = _elements.$location[element.address];
                        initDeviceInformation(element.address)
                    }
                });
            }
        }

        function SortByFloorAndLocation(a, b) {
            var aFlr = parseInt(a.floor)
            var bFlr = parseInt(b.floor)
            var aLoc = parseInt(a.location)
            var bLoc = parseInt(b.location)
            var aEquip = parseInt(a.equipId, 16)
            var bEquip = parseInt(b.equipId, 16)
            return ((aFlr < bFlr) ? -1 : ((aFlr == bFlr) ? (aLoc < bLoc ? -1 : ((aLoc == bLoc) ? (aEquip < bEquip ? -1 : 1) : 1)) : 1));
        }

        function isShowOn(type, element) {
            if (_elements.$location[element.address] === undefined)
                return false;
            if (_elements.$mode == 'config' && _elements.$page == element.address) {
                return true;
            } else if (_elements.$mode == 'list') {
                if (type == 'PieChart' || type == 'ContentTable') {
                    if (_elements.$page == 'all')
                        return true;
                    else if (_elements.$page == _elements.$location[element.address].floor)
                        return true;
                }
            }
            return false;
        }

        function typeToShow(element) {
            var type = [];
            if (['config', 'list'].indexOf(_elements.$mode) > -1 && [/*'2', '5'*/].indexOf(element.equipId) == -1) {
                type.push('PieChart')
            }
            if (_elements.$mode == 'config' || [/*'2', '5'*/].indexOf(element.equipId) > -1) {
                type.push('ContentTable')
            }
            if (_elements.$mode == 'config' && ['3', '4', 'a'].indexOf(element.equipId) == -1) {
                type.push('ConfigTable')
            }
            if (_elements.$mode == 'config' && ['3', '4', '5', 'a'].indexOf(element.equipId) == -1) {
                type.push('ControlTable')
            }
            if (_elements.$mode == 'config' && [/*'2', '5'*/].indexOf(element.equipId) == -1) {
                type.push('LineChart')
            }
            return type;
        }

        function createFloorButton() {
            var sidebar = $('.rad-sidebar > ul');
            var topbar = $('ul.list-container');
            for (var floor in _elements.$floors) {
                sidebar.append(getFloorTemplateString().replace('{{floor}}', floor).replace('{{floorChs}}', _elements.$floors[floor]).replace('{{btn-style}}', 'rad-bg-floor-' + parseInt((floor % 4) + 1)))
                topbar.append(getFloorTemplateString().replace('{{floor}}', floor).replace('{{floorChs}}', _elements.$floors[floor]).replace('{{btn-style}}', 'rad-bg-floor-' + parseInt((floor % 4) + 1)))
                $('a[href="?floor=' + floor + '"]').parent().css('display', 'none')
            }
        }

        function initPieChart(element) {
            var className = 'device_' + element.address + '_piechart';
            if (_elements.$location[element.address] !== undefined && $('\.' + className).get(0) === undefined) {
                var title = parseFloor(_elements.$location[element.address].floor) + _elements.$location[element.address].location + _elements.$map[element.equipId];
                var data = parseEquipment(element)
                if (data.title.length > 0) {
                    $('.row').append($(getPieChartTemplateString(className, title, data.date, data.time)))
                    _elements.$address[className] = element.address;
                    _elements.$chart[className] = $('\.' + className).percentCircle({
                        width: 130,
                        trackColor: 'rgb(236, 236, 236)',
                        barColor: 'rgb(245, 170, 50)',
                        titleColor: 'rgb(245, 170, 50)',
                        numberColor: 'rgb(245, 170, 50)',
                        powerOffColor: 'rgb(100, 100, 100)',
                        disconnectColor: 'rgb(185, 0, 0)',
                        barWeight: 5,
                        endPercent: data.endpoint,
                        dataDepend: data.depend,
                        barColorThreshold: data.barColorThreshold,
                        numberTitle: data.title,
                        number: data.data + ' ' + data.unit,
                        fps: 60,
                        address: element.address,
                        power: data.powerStatus,
                        timestamp: data.date + ' ' + data.time,
                        update: data.step
                    });
                    $('\.' + className).click(function (e) {
                        e.stopPropagation();
                        location = '?equipment=' + $(e.target).attr('address');
                    })
                }
            }
        }

        function initLineChart(element) {
            var className = 'device_' + element.address + '_linechart';
            element.className = className;
            if (_elements.$location[element.address] !== undefined && $('\.' + className).get(0) === undefined) {
                var bindContentChart = 'device_' + element.address + '_contenttable';
                var data = parseEquipment(element)
                var title = parseFloor(_elements.$location[element.address].floor) + _elements.$location[element.address].location + _elements.$map[element.equipId];
                $('.row').append($(getLineChartTemplateString(className, title, data.date, data.time, data.displayTypeOnLineChart)))
                _elements.$time[className] = element.timestamp;
                _elements.$address[className] = element.address;
                _elements.$chart[className] = $('\.' + className).lineChart({
                    className: className,
                    address: element.address,
                    title: data.configTitle,
                    dataDepend: data.depend,
                    dataUnit: data.configUnit,
                    dataPoints: [{ x: new Date(data.date + " " + data.time), y: data.data }],
                    display: '.display',
                    depend: '.displayDepend',
                    year: '.year',
                    month: '.month',
                    day: '.day',
                    defaultCallback: fetchDataUtilNow,
                    dependCallback: fetchDataFromDateRange,
                    dependShowType: data.cellDependShowType,
                    dependShowTypeCallback: displayTypeChangeCallback
                });
            }

            function displayTypeChangeCallback(type) {
                if (typeof _elements.$chart[bindContentChart] !== 'undefined') {
                    _elements.$chart[bindContentChart].changeDisplayType(type)
                }
            }
        }

        function initHistoryTable(element) {
            var time = new Date()
            var date = time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate()
            var time = pad(time.getHours(), 2) + ":" + pad(time.getMinutes(), 2) + ":" + pad(time.getSeconds(), 2)
            var selects = ['month']
            var address = localStorage['__device__address']
            var column = localStorage['__device__column']

            var className = 'device_historytable';
            var title = '接收率'
            var dependClassName = { year: 'year', month: 'month' }
            $('.row').append($(getHistoryChartTemplateString(className, '歷史紀錄', ['year', 'month'], dependClassName, date, time)))
            _elements.$chart[className] = $('\.' + className).historyChart({
                className: className,
                address: address,
                column: column,
                title: title,
                display: '.display',
                year: '.' + dependClassName['year'],
                month: '.' + dependClassName['month'],
                depend: 'month',
                dependCallback: fetchAccumulateFromDataRange,
            })
            _elements.$chart[className].displayDependChange('month')

            var rssiClassName = 'device_rssihistorytable';
            var title = '訊號強度(RSSI)'
            var dependClassName = { year: 'rssiYear', month: 'rssiMonth', day: 'rssiDay' }
            $('.row').append($(getHistoryChartTemplateString(rssiClassName, '歷史紀錄', ['year', 'month', 'day'], { year: 'rssiYear', month: 'rssiMonth', day: 'rssiDay' }, date, time)))
            _elements.$chart[rssiClassName] = $('\.' + rssiClassName).historyRSSIChart({
                className: rssiClassName,
                address: address,
                column: column,
                title: title,
                display: '.display',
                year: '.' + dependClassName['year'],
                month: '.' + dependClassName['month'],
                day: '.' + dependClassName['day'],
                depend: 'day',
                dependCallback: fetchRSSIFromDataRange,
            })
            _elements.$chart[rssiClassName].displayDependChange('day')

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
        }

        function initConfigTable(element) {
            var className = 'device_' + element.address + '_configtable';
            element.className = className;
            if (_elements.$location[element.address] !== undefined && $('\.' + className).get(0) === undefined) {
                var data = parseEquipment(element)
                var title = parseFloor(_elements.$location[element.address].floor) + _elements.$location[element.address].location + _elements.$map[element.equipId];
                $('.row').append($(getConfigTemplateString(className, title)))
                _elements.$chart[className] = $('\.' + className).configTable({
                    className: className,
                    title: data.title,
                    element: element,
                    configTitle: data.configTitle,
                    configData: data.configData,
                    cellToControl: data.cellToControl,
                    cellToChangeBase: data.cellToChangeBase,
                    depends: _elements.$location[element.address].depends,
                    server: _elements.server
                });
                $('\.' + className + ' .controlButton').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    Object.keys(data.cellToChangeBase).map(function (key, index) {
                        var dec = parseInt($(e.toElement).parent('form').find("\." + key + "_edit").val())
                        $(e.toElement).parent('form').find("\." + key).val(dec.toString(data.cellToChangeBase[key]))
                    })
                    var parameter = $(e.toElement).parent('form').serialize();
                    var action = $(e.toElement).parent('form').attr('action');
                    var method = $(e.toElement).parent('form').attr('method');
                    $.ajax({
                        url: action + '?' + parameter,
                        method: method,
                        success: function (result) {
                            var $data = result;
                        },
                        error: function (e) {
                            console.info('error');
                        }
                    });
                })
            }
        }

        function initControlDependTable(element) {
            var className = 'device_' + element.address + '_controltable';
            element.className = className;
            if (_elements.$location[element.address] !== undefined && $('\.' + className).get(0) === undefined) {
                var data = parseEquipment(element)
                var title = ['1'].indexOf(element.equipId) >= 0 ? "自動控制" : "互動控制"
                $('.row').append($(getDependControlTemplateString(className, title)))
                _elements.$chart[className] = $('\.' + className).controlTable({
                    company: company,
                    element: element,
                    depend: _elements.$location,
                    server: _elements.server,
                    map: _elements.$map
                });
                var TimestampCheck = () => { if (CheckTimestampIsOnLockRange("#time_s", "#time_es", "#time_ee")) { alert("開機時間和關機時間衝突。"); return true; } else { return false; } }
                CreateTimerPicker("#time_s", "#timer_s", TimestampCheck)
                CreateTimerPicker("#time_es", "#timer_es", TimestampCheck)
                CreateTimerPicker("#time_ee", "#timer_ee", TimestampCheck)
                CreateRelationCheckbox("#timer_es", "#timer_ee")
                CreateThresholdSelect("#threshold", "#threshold_i")
                $('\.' + className + ' .dependControlButton').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    console.info([CheckIsValid("#timer_s"), CheckIsValid("#timer_es"), CheckIsValid("#timer_ee"), CheckIsValid("#threshold_i")])
                    if ([CheckIsValid("#timer_s"), CheckIsValid("#timer_es"), CheckIsValid("#timer_ee"), CheckIsValid("#threshold_i")].indexOf(false) >= 0) {
                        alert("請填入正確的數值。")
                        return;
                    }
                    Object.keys(data.cellToChangeBase).map(function (key, index) {
                        var dec = parseInt($(e.toElement).parent('form').find("\." + key + "_edit").val())
                        $(e.toElement).parent('form').find("\." + key).val(dec.toString(data.cellToChangeBase[key]))
                    })
                    var parameter = $(e.toElement).parent('form').serialize();
                    var action = $(e.toElement).parent('form').attr('action');
                    var method = $(e.toElement).parent('form').attr('method');
                    $.ajax({
                        url: action + '?' + parameter,
                        method: method,
                        success: function (result) {
                            var $data = result;
                            location.reload()
                        },
                        error: function (e) {
                            console.info('error');
                            location.reload()
                        }
                    });
                })
            }

            function CreateTimerPicker(source, target, callback = undefined) {
                var initValueAry = typeof $(target).val() !== 'undefined' ? $(target).val().split(':') : []
                var initValue = initValueAry.length == 3 ? initValueAry[0] + ":" + initValueAry[1] : ''
                $(source).bootstrapMaterialDatePicker({ format: 'HH:mm', date: false }).on('change', function (e, date) {
                    var time = pad(date.hours(), 2) + ":" + pad(date.minutes(), 2) + ":00"
                    if (callback != undefined && callback()) {
                        $(source).val('')
                        $(target).val('')
                        $(target).prop('checked', false)
                        $(target).attr('checked', false)
                    }
                    $(target).val(time)
                })
                $(target).on('change', function (e) {
                    var checked = e.currentTarget.checked
                    if (!checked) {
                        $(source).val('')
                    }
                })
                if (initValue.length > 0) {
                    $(source).val(initValue)
                }
            }

            function CreateThresholdSelect(source, target) {
                $(source).on('change', function (e) {
                    var threshold = e.currentTarget.value
                    $(target).val(threshold)
                });
            }

            function CreateRelationCheckbox(source, target) {
                $(source).on('change', function (e) {
                    $(target).attr('checked', e.currentTarget.checked)
                })
            }

            function CheckTimestampIsOnLockRange(source, targetStart, targetEnd) {
                var today = CurrentDate()
                var tomorrow = CurrentDate(1)
                var sourceStart = $(source).val()
                var sourceToday = today + ' ' + sourceStart
                var sourceTomorrow = tomorrow + ' ' + sourceStart
                var targetStart = $(targetStart).val()
                var targetEnd = $(targetEnd).val()
                if (targetStart.length == 0 || targetEnd.length == 0 || sourceStart.length == 0)
                    return false

                var lockingRange = GetTimeIntervalLock(today, tomorrow, targetStart, targetEnd)
                if (checkTimestampTrigger(sourceToday, lockingRange) || checkTimestampTrigger(sourceTomorrow, lockingRange))
                    return true
                return false
            }

            function CheckIsValid(target) {
                var value = $(target).val()
                var checked = $(target).prop('checked')
                if (!checked || (checked && value.length > 0))
                    return true
                return false
            }

            function GetTimeIntervalLock(today, tomorrow, start, end) {
                var hasChangeDate = start < end ? true : false
                return [today + " " + start, (hasChangeDate ? today : tomorrow) + " " + end]
            }

            function checkTimestampTrigger(timestamp, range) {
                if (range[0] == null && timestamp < range[1]) {
                    return true;
                } else if (range[1] == null && range[0] <= timestamp) {
                    return true;
                } else if (range[0] <= timestamp && timestamp < range[1]) {
                    return true;
                }
                return false;
            }

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
        }

        function initContentTable(element) {
            var className = 'device_' + element.address + '_contenttable';
            element.className = className;
            if (_elements.$location[element.address] !== undefined && $('\.' + className).get(0) === undefined) {
                var bindLineChart = 'device_' + element.address + '_linechart';
                var data = parseEquipment(element)
                var title = parseFloor(_elements.$location[element.address].floor) + _elements.$location[element.address].location + _elements.$map[element.equipId];

                /** 針對cost累積值特別處理 */
                if (typeof data.configData["cost"] !== "undefined") {
                    data.configData.cost.data = "計算中"
                }
                $('.row').append($(getPureContentTemplateString(className, title)))
                _elements.$chart[className] = $('\.' + className).configContentTable({
                    className: className,
                    title: data.title,
                    element: element,
                    mode: _elements.$mode,
                    configTitle: data.configTitle,
                    configData: data.configData,
                    cellToShow: data.cellToShow,
                    cellToRequire: data.cellToRequire,
                    cellDecode: data.cellDecode,
                    decodeContent: data.decodeContent,
                    displayOnLineChart: data.displayCellOnLineChart
                });
                if (data.cellToRequire.length > 0) {
                    for (var index in data.cellToRequire) {
                        var cell = data.cellToRequire[index]

                        if (_elements.$location[element.address].notifys.length > 0) {
                            $.ajax({
                                url: _elements.server + '/' + data.configData[cell].format.url,
                                method: data.configData[cell].format.method,
                                data: data.configData[cell].format.data,
                                success: function (result) {
                                    var json = JSON.parse(result)
                                    if (typeof json !== 'undefined') {
                                        $('#' + data.configData[cell].to).html(json[data.configData[cell].from].toFixed(1) + ' 小時')
                                    }
                                },
                                error: function (e) {
                                    $('#' + data.configData[cell].to).html('尚無資料')
                                }
                            })
                        } else {
                            $('#' + data.configData[cell].to).html('尚未啟動')
                        }
                    }
                }
                $('\.' + className + ' .editButton').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    location = '?equipment=' + $(e.target).attr('address');
                })
                $('\.' + className + ' td.from[title=' + data.depend + ']').parent().addClass('breath-light')
                $('\.' + className + ' tr').map(function () {
                    if (data.displayCellOnLineChart['default'].indexOf($(this).find('.from').attr('title')) > -1) {
                        $(this).click(function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                            if (_elements.$chart[className].currentIsClickable($(this), _elements.$chart[bindLineChart])) {
                                $(this).parent().find('.breath-light').removeClass('breath-light')
                                $(this).addClass('breath-light')
                                _elements.$chart[className].clickCallbackToLineChart(e, _elements.$chart[bindLineChart])
                            }
                        })
                    }
                })
            }
        }

        function initRegisterTable(mode) {
            var className = 'device_registertable';
            var prefix = 'register'.indexOf(mode) > -1 ? '註冊' : '編輯';
            var title = prefix + '設備';
            $('.row').append($(getRegisterTemplateString(className, title)))
            console.info(_elements.$map)
            var registerTable = $('\.' + className).configRegisterTable({
                className: className,
                mode: mode,
                email: _elements.email,
                equip: _elements.$map,
                server: _elements.server
            });
            $('\.' + className + ' .submitButton').click(function (e) {
                e.stopPropagation();
                e.preventDefault();
                var array = $(e.toElement).parent('form').serializeArray();
                var action = $(e.toElement).parent('form').attr('action');
                var method = $(e.toElement).parent('form').attr('method');
                var parameter = {};
                for (var index in array) {
                    parameter[array[index].name] = array[index].value;
                }
                $.ajax({
                    url: action,
                    method: method,
                    data: parameter,
                    success: function (result) {
                        var $data = result;
                        if ($data.type == 'success') {
                            alert('成功' + prefix + '設備。')
                            location = "?list=all";
                        } else {
                            alert('設備' + prefix + '失敗。')
                            location = "?list=all";
                        }
                    },
                    error: function (e) {
                        alert('設備' + prefix + '失敗。')
                        location = "?list=all";
                    }
                });
            })
        }

        function initDeviceInformation(address) {
            var className = 'device_' + address + '_informationtable';
            if (_elements.$location[address] !== undefined && $('\.' + className).get(0) === undefined) {
                var title = parseFloor(_elements.$location[address].floor) + _elements.$location[address].location;// + _elements.$map[element.equipId];
                var company = _elements.$location[address].companyID
                $('.row').append($(getInformationTemplateString(className, title)))
                _elements.$chart[className] = $('\.' + className).configInfoTable({
                    className: className,
                    address: address,
                    location: _elements.$location[address].location,
                    c_floor: parseFloor(_elements.$location[address].floor),
                    floor: _elements.$location[address].floor,
                    place: _elements.$location[address].place,
                    equipId: _elements.$location[address].equipId,
                    companyID: _elements.$location[address].companyID,
                    equip: _elements.$map,
                });
                $('\.' + className + ' .editButton').click(function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    $(e.currentTarget).parent().find('.device_info').map(function () {
                        localStorage['__device__' + $(this).attr('title')] = $(this).attr('data')
                    })
                    location = '?action=modify'
                })
                if (inDatas(address)) {
                    $('\.' + className + ' .viewButton').click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        location = '?equipment=' + address
                    })
                } else {
                    $('#viewButton_' + address).prop('disabled', true)
                    $('#viewButton_' + address).val('尚無資訊')
                }
                /* Fetch Accumulate By Address */
                var date = CurrentDate(0, false)

                // DEBUG CODE FOR 仁愛醫院, WORKING RANGE 9AM - 6PM
                // FETCH PREVIOUS DAY DATA IF BEFORE 9AM
                if (['99958172', '92021164'].indexOf(company) > -1) {
                    var time = new Date()
                    if (time.getHours() < 9) {
                        date = CurrentDate(-1, false)
                    }
                }
                FetchAccumulate(address, date)

                function FetchAccumulate(address, date) {
                    $.ajax({
                        url: _elements.server + '/RequireAccumulate',
                        method: 'GET',
                        data: {
                            address: address,
                            date: date
                        },
                        success: function (result) {
                            if (result.length > 0) {
                                var data = JSON.parse(result)
                                var time = new Date()
                                var timestamp = new Date(data.timestamp)
                                var minutes = timestamp.getUTCHours() * 60 + timestamp.getUTCMinutes()
                                // DEBUG CODE FOR 仁愛醫院, WORKING RANGE 9AM - 6PM
                                if (['99958172', '92021164'].indexOf(company) > -1) {
                                    if (timestamp.getUTCHours() >= 9 && timestamp.getUTCHours() < 18) {
                                        minutes -= 9 * 60
                                    } else {
                                        minutes = 9 * 60
                                    }
                                }
                                var percent = parseFloat(data.accumulate / minutes) * 100
                                percent = percent > 100 ? 100 : percent.toFixed(2)
                                $('#accumulate_' + address).text(percent + '%')
                            } else {
                                $('#accumulate_' + address).text('尚無資料')
                            }
                            $('#accumulate_' + address).click((e) => {
                                e.stopPropagation()
                                e.preventDefault()
                                var address = $(e.currentTarget).attr("data")
                                var title = $(e.currentTarget).attr("title")
                                localStorage['__device__address'] = address
                                localStorage['__device__column'] = title
                                location = '?action=history'
                            })
                        },
                        error: function (e) {

                        }
                    });

                    function pad(n, width, z) {
                        z = z || '0';
                        n = n + '';
                        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
                    }
                }

                function inDatas(address) {
                    for (var index in _elements.$data) {
                        if (_elements.$data[index].address == address)
                            return true
                    }
                    return false
                }
            }
        }

        function updatePieChart(elements) {
            elements.forEach(function (element) {
                var className = 'device_' + element.address + '_piechart';
                element.className = className;
                if (typeof _elements.$chart[className] !== 'undefined') {
                    var data = parseEquipment(element);
                    var options = {
                        number: data.data + ' ' + data.unit,
                        endPercent: data.endpoint,
                        power: data.powerStatus,
                        timestamp: data.date + ' ' + data.time
                    }

                    _elements.$chart[className].update(options);
                    $('\.' + className + '_date').html(data.date);
                    $('\.' + className + '_timestamp').html(data.time);
                }
            }, this);
        }

        function updateContentTable(elements) {
            elements.forEach(function (element) {
                var className = 'device_' + element.address + '_contenttable';
                var linechartClassName = 'device_' + element.address + '_linechart';
                element.className = className;
                if (typeof _elements.$chart[className] !== 'undefined') {
                    var data = parseEquipment(element);
                    /** 針對cost累積值特別處理 */
                    if (typeof data.configData["cost"] !== "undefined" && _elements.$chart[linechartClassName].hasHisDataPoint()) {
                        var cntDataPoint = _elements.$chart[linechartClassName].cntDataPoint()
                        data.configData.cost.data = cntDataPoint.cost
                    } else if (typeof data.configData["cost"] !== "undefined" && !_elements.$chart[linechartClassName].hasHisDataPoint()) {
                        data.configData.cost.data = "計算中"
                    }

                    var options = {
                        configData: data.configData
                    }
                    _elements.$chart[className].update(options);
                }
            }, this);
        }

        function updateLineChart(elements) {
            elements.forEach(function (element) {
                var className = 'device_' + element.address + '_linechart';
                element.className = className;
                if (typeof _elements.$chart[className] !== 'undefined') {
                    var depend = _elements.$chart[className].attr('data-depend')
                    var data = parseData(element).datas
                    var options = { dataPoints: { x: new Date(data.date + " " + data.time), y: data[depend] } };
                    /** 針對cost累積值特別處理 */
                    if (typeof data["cost"] !== "undefined" && _elements.$chart[className].hasHisDataPoint()) {
                        var cntDataPoint = _elements.$chart[className].cntDataPoint()
                        var delta = parseFloat(options.dataPoints.y) - parseFloat(cntDataPoint.p_cost)
                        delta = delta > 0 && depend == "cost" ? delta : 0
                        data.p_cost = data.cost
                        data.cost = parseFloat((parseFloat(cntDataPoint.cost) + delta).toFixed(1))
                        _elements.$chart[className].setHisDataPoint(data)
                        if (depend == "cost")
                            options.dataPoints.y = data.cost
                    }
                    _elements.$time[className] = element.timestamp;
                    _elements.$chart[className].update(options);
                    $('\.' + className + '_date').html(data.date);
                    $('\.' + className + '_timestamp').html(data.time);
                }
            }, this);
        }

        function fetchAccumulateFromDataRange(className, address, column, start, end) {
            $.ajax({
                url: uploc + '/RequireHistoryStatistic',
                dataType: 'json',
                method: 'GET',
                data: {
                    address: address,
                    rangeStart: start,
                    rangeEnd: end
                },
                success: function (result) {
                    var $data = result;
                    var datas = []
                    var today = CurrentDate()
                    var minutes = 24 * 60
                    $data.sort(SortByTimestamp)
                    for (var index in $data) {
                        var time = FormatDate($data[index]['date'])
                        var accumulate = parseInt($data[index][column])
                        var percent = 0;
                        var timestamp = new Date($data[index]['timestamp'])
                        if (time == today) {
                            minutes = timestamp.getUTCHours() * 60 + timestamp.getUTCMinutes()
                        } else {
                            minutes = 24 * 60
                        }
                        // DEBUG CODE FOR 仁愛醫院, WORKING RANGE 9AM - 6PM
                        if (['99958172', '92021164'].indexOf(company) > -1) {
                            if (time == today) {
                                continue
                            } else {
                                minutes = 9 * 60
                            }
                        }
                        percent = parseFloat(accumulate / minutes) * 100
                        percent = percent > 100 ? 100 : percent.toFixed(2)
                        datas.push({ x: new Date($data[index]['date']), y: parseFloat(percent) })
                    }
                    if (typeof _elements.$chart[className] !== 'undefined') {
                        _elements.$chart[className].update(datas)
                    }
                }
            });
        }

        function fetchRSSIFromDataRange(className, address, column, start, end) {
            $.ajax({
                url: uploc + '/RequireHistoryRSSI',
                dataType: 'json',
                method: 'GET',
                data: {
                    address: address,
                    rangeStart: start,
                    rangeEnd: end
                },
                success: function (result) {
                    var $data = result;
                    var datas = []
                    $data.sort(SortByTimestamp)
                    for (var index in $data) {
                        var time = new Date($data[index]['timestamp'])
                        var localTime = new Date(time.setHours(time.getHours() - 8))
                        datas.push({ x: localTime, y: parseFloat($data[index]['rssi']) })
                    }
                    if (typeof _elements.$chart[className] !== 'undefined') {
                        _elements.$chart[className].update(datas)
                    }
                }
            });
        }

        function fetchDataFromDateRange(className, address, start, end) {
            //console.info(className, ':', address, ':', start, ' - ', end)
            $.ajax({
                url: uploc + '/RequireHistoryData',
                dataType: 'json',
                method: 'GET',
                data: {
                    address: address,
                    rangeStart: start,
                    rangeEnd: end
                },
                success: function (result) {
                    var $data = result;
                    addHistoryDatasToLinechart(className, $data)
                }
            });
        }

        function fetchDataUtilNow(className, address, start, end) {
            $.ajax({
                url: uploc + '/RequireRealHistoryData',
                dataType: 'json',
                method: 'GET',
                data: {
                    address: address,
                    rangeStart: start,
                    rangeEnd: end
                },
                success: function (result) {
                    var $data = result;
                    $data.sort(SortByTimestamp)
                    dependHistoryDatasToLinechart(className, $data)
                }
            });
        }

        function addHistoryDatasToLinechart(className, datas) {
            if (typeof _elements.$chart[className] !== 'undefined') {
                if (_elements.$chart[className].currentDisplayType() == 'month') {
                    var HdataPoints = []
                    var LdataPoints = []
                    var AVGdataPoints = []
                    var depend = _elements.$chart[className].attr('data-depend')
                    datas.sort(SortByTimestamp)
                    for (var i = 0; i < datas.length; i++) {
                        datas[i].className = className;
                        var $data = parseData(datas[i])
                        var time = new Date($data.datas.date);
                        time.setHours(0)
                        time.setMinutes(0)
                        time.setSeconds(0)
                        switch (true) {
                            case parseInt(datas[i]['tempType']) == 1:
                                HdataPoints.push({ x: time, y: parseFloat($data.datas[depend]) })
                                if ($data.datas.hasOwnProperty('AVG_' + depend)) {
                                    AVGdataPoints.push({ x: time, y: parseFloat($data.datas['AVG_' + depend]) })
                                }
                                break;
                            case parseInt(datas[i]['tempType']) == 0:
                                LdataPoints.push({ x: time, y: parseFloat($data.datas[depend]) })
                                break;
                        }
                    }
                } else if (_elements.$chart[className].currentDisplayType() == 'year') {
                    var HAdataPoints = {}
                    var LAdataPoints = {}
                    var HdataPoints = []
                    var LdataPoints = []
                    var AVGdataPoints = []
                    var depend = _elements.$chart[className].attr('data-depend')
                    datas.sort(SortByTimestamp)
                    for (var i = 0; i < datas.length; i++) {
                        datas[i].className = className;
                        var $data = parseData(datas[i])
                        var time = new Date($data.datas.date);
                        time.setHours(0)
                        time.setMinutes(0)
                        time.setSeconds(0)
                        var month = $data.datas['date'].replace(/([0-9]+)-([0-9]+)-([0-9]+)/, '$2')
                        if (HAdataPoints.hasOwnProperty(month) == false)
                            HAdataPoints[month] = 0
                        if (LAdataPoints.hasOwnProperty(month) == false)
                            LAdataPoints[month] = 0
                        switch (true) {
                            case parseInt(datas[i]['tempType']) == 1:
                                HAdataPoints[month] += parseFloat($data.datas[depend])
                                break;
                            case parseInt(datas[i]['tempType']) == 0:
                                LAdataPoints[month] += parseFloat($data.datas[depend])
                                break;
                        }
                    }
                    for (var month in HAdataPoints) {
                        HdataPoints.push({ label: month + '月份', y: parseFloat(HAdataPoints[month].toFixed(2)) })
                    }
                    for (var month in LAdataPoints) {
                        LdataPoints.push({ label: month + '月份', y: parseFloat(LAdataPoints[month].toFixed(2)) })
                    }
                }
                _elements.$chart[className].insertHighHistory(HdataPoints);
                _elements.$chart[className].insertLowHistory(LdataPoints);
                if (AVGdataPoints.length > 0) {
                    _elements.$chart[className].insertAVGHistory(AVGdataPoints);
                }
                _elements.$chart[className].refresh()
            }
        }

        function dependHistoryDatasToLinechart(className, datas) {
            if (typeof _elements.$chart[className] !== 'undefined') {
                var $data = []
                var dataPoints = []
                var delta = 0
                var depend = _elements.$chart[className].attr('data-depend')

                /** 針對第一筆特別處理 */
                if (datas.length > 0) {
                    $data.push(parseData(datas[0]).datas)
                    $data[0].p_cost = $data[0].cost;
                    $data[0].cost = 0;
                    for (var i = 1; i < datas.length; i++) {
                        datas[i].className = className;
                        $data.push(parseData(datas[i]).datas)
                        if (i > 0) {
                            $data[i].p_cost = $data[i].cost
                            delta = ($data[i].cost - $data[i - 1].p_cost)
                            $data[i].cost = parseFloat(($data[i - 1].cost + (delta > 0 ? delta : 0)).toFixed(1))
                        }
                        var time = new Date($data[i].date + ' ' + $data[i].time)
                        dataPoints.push({ x: time, y: parseFloat($data[i][depend]) })
                    }
                }
                _elements.$chart[className].insertHistory(dataPoints);
                if (!_elements.$chart[className].hasHisDataPoint() && $data.length > 0) {
                    _elements.$chart[className].setHisDataPoint($data[$data.length - 1])
                }
            }
        }

        function SortByTimestamp(a, b) {
            var aT = new Date(a.timestamp)
            var bT = new Date(b.timestamp)
            return (aT.getTime() < bT.getTime()) ? -1 : 1;
        }

        function refreshLineChart() {
            var elements = $('.report-linechart-box')
            $.each(elements, function (index) {
                var name = $(elements[index]).attr('name')
                _elements.$chart[name].refresh()
            })
        }

        function getCurrentTime(className) {
            if (_elements.$time[className]) {
                return _elements.$time[className];
            }
        }

        function classNameToAddress(className) {
            if (_elements.$address[className]) {
                return _elements.$address[className];
            }
        }

        function CurrentDate(dayShift = 0, padding = true) {
            var time = new Date()
            time.setDate(time.getDate() + dayShift)
            var date = ""
            if (padding)
                date = time.getFullYear() + "-" + pad(time.getMonth() + 1, 2) + "-" + pad(time.getDate(), 2);
            else
                date = time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate();
            return date;

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
        }

        function FormatDate(timestamp, padding = true) {
            var time = new Date(timestamp)
            var date = ""
            if (padding)
                date = time.getFullYear() + "-" + pad(time.getMonth() + 1, 2) + "-" + pad(time.getDate(), 2);
            else
                date = time.getFullYear() + "-" + (time.getMonth() + 1) + "-" + time.getDate();
            return date

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
        }

        function getPieChartTemplateString(device, title, date = '', timestamp = '') {
            return [
                '<div class="report-statistic-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class="box-content {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '<div class="box-foot">',
                '<span class="arrow arrow-up"></span>',
                '<div class="box-foot-left">日期<br><span class="box-foot-stats"><strong class="{{device}}_date">{{date}}</strong></span></div>'.replace(/{{date}}/, date).replace(/{{device}}/, device),
                '<span class="arrow arrow-down"></span>',
                '<div class="box-foot-right">時間<br><span class="box-foot-stats"><strong class="{{device}}_timestamp">{{timestamp}}</strong></span></div>'.replace(/{{timestamp}}/, timestamp).replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getLineChartTemplateString(device, title, date = '', timestamp = '', displayType = {}) {
            options = []
            Object.keys(displayType).map((key, index) => {
                options.push('  <option value="' + key + '">' + displayType[key] + '</option>')
            })
            return [
                '<div class="report-linechart-box block" name="{{device}}">'.replace(/{{device}}/, device),
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class="box-content {{device}}">'.replace(/{{device}}/, device),
                '<div id="chartContainer_{{device}}" style="height: 300px; width: 100%;"></div>'.replace(/{{device}}/, device),
                '</div>',
                '<div class="box-foot">',
                '<div class="box-foot-left">日期<br><span class="box-foot-stats"><strong class="{{device}}_date">{{date}}</strong></span></div>'.replace(/{{date}}/, date).replace(/{{device}}/, device),
                options.length > 0 ? [
                    '<select class="selectpicker show-tick displayDepend">',
                    options.join(''),
                    '</select>'] : '',
                '<select class="selectpicker show-tick display year">',
                '</select>',
                '<select class="selectpicker show-tick display month">',
                '</select>',
                '<select class="selectpicker show-tick display day">',
                '</select>',
                '<div class="box-foot-right">時間<br><span class="box-foot-stats"><strong class="{{device}}_timestamp">{{timestamp}}</strong></span></div>'.replace(/{{timestamp}}/, timestamp).replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getHistoryChartTemplateString(device, title, type = [], className = {}, date = '', timestamp = '') {
            return [
                '<div class="report-linechart-box block" name="{{device}}">'.replace(/{{device}}/, device),
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class="box-content {{device}}">'.replace(/{{device}}/, device),
                '<div id="chartContainer_{{device}}" style="height: 300px; width: 100%;"></div>'.replace(/{{device}}/, device),
                '</div>',
                '<div class="box-foot">',
                '<div class="box-foot-left">日期<br><span class="box-foot-stats"><strong class="{{device}}_date">{{date}}</strong></span></div>'.replace(/{{date}}/, date).replace(/{{device}}/, device),
                (type.indexOf('year') > -1 ? ['<select class="selectpicker show-tick display ' + className['year'] + '">',
                    '</select>'].join('') : ''),
                (type.indexOf('month') > -1 ? ['<select class="selectpicker show-tick display ' + className['month'] + '">',
                    '</select>'].join('') : ''),
                (type.indexOf('day') > -1 ? ['<select class="selectpicker show-tick display ' + className['day'] + '">',
                    '</select>'].join('') : ''),
                '<div class="box-foot-right">時間<br><span class="box-foot-stats"><strong class="{{device}}_timestamp">{{timestamp}}</strong></span></div>'.replace(/{{timestamp}}/, timestamp).replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getPureContentTemplateString(device, title) {
            return [
                '<div class="report-statistic-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class=" {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getConfigTemplateString(device, title) {
            return [
                '<div class="report-config-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class=" {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getDependControlTemplateString(device, title) {
            return [
                '<div class="report-depend-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class=" {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getInformationTemplateString(device, title) {
            return [
                '<div class="report-information-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class=" {{device}}">'.replace(/{{device}}/, device),
                '</div>',
                '</div>',
            ].join('');
        }

        function getRegisterTemplateString(className, title) {
            return [
                '<div class="report-register-box block">',
                '<div class="box-header">{{title}}</div>'.replace(/{{title}}/, title),
                '<div class=" {{className}}">'.replace(/{{className}}/, className),
                '</div>',
                '</div>',
            ].join('');
        }

        function getTitleTemplateString(title) {
            return [
                '<div>',
                '<h2 style="margin:0 0 20px 0;">{{title}}</h2>'.replace('{{title}}', title),
                '<div class="row">',
                '</div>'
            ].join('');
        }

        function getFloorTemplateString() {
            return '<li><a href="?floor={{floor}}"><i class="glyphicon glyphicon-chevron-right"><span class="icon-bg {{btn-style}}"></span></i><span class="rad-sidebar-item">{{floorChs}}</span></a></li>';
        }

        function parseData(__datas) {
            var datas = {};
            var unit = {}
            var base = {}
            var parsed = { datas: null, unit: null, base: null, equipId: null }
            switch (true) {
                case 1 == __datas.equipId:
                    unit.temp = 1
                    unit.cost = 0.1
                    unit.set_temp = 1
                    base.cost = 16
                    base.temp = 16
                    base.set_temp = 16

                    datas.power = parseInt(__datas.power)
                    datas.status = parseInt(__datas.status)
                    datas.wind = parseInt(__datas.wind)
                    datas.wind_dir = parseInt(__datas.wind_dir)
                    datas.set_temp = parseInt(__datas.set_temp, base.set_temp) * unit.set_temp
                    datas.temp = parseInt(__datas.temp, base.temp) * unit.temp
                    datas.cost = (parseInt(__datas.cost, base.cost) * unit.cost).toFixed(1)
                    datas.errorCode = __datas.errorCode
                    datas.remain = __datas.remain
                    break;
                case 2 == __datas.equipId:
                    unit.cost = 0.1
                    base.cost = 16

                    datas.power = parseInt(__datas.power)
                    datas.status = parseInt(__datas.status)
                    datas.wind = parseInt(__datas.wind)
                    datas.cost = (parseInt(__datas.cost, base.cost) * unit.cost).toFixed(1)
                    datas.errorCode = __datas.errorCode
                    break;
                case 3 == __datas.equipId:
                    unit.HCHO = 0.01
                    unit.CO2 = 1
                    unit.CO = 1
                    unit.PM10 = 0.1
                    unit.TVOC = 0.01
                    base.HCHO = 16
                    base.CO2 = 16
                    base.CO = 16
                    base.PM10 = 16
                    base.TVOC = 16

                    datas.HCHO = parseInt(__datas.HCHO, base.HCHO) * unit.HCHO
                    datas.CO2 = parseInt(__datas.CO2, base.CO2) * unit.CO2
                    datas.CO = parseInt(__datas.CO, base.CO) * unit.CO
                    datas.PM10 = parseInt(__datas.PM10, base.PM10) * unit.PM10
                    datas.TVOC = (parseInt(__datas.TVOC, base.TVOC) * unit.TVOC).toFixed(2)

                    if (CheckAvg(__datas, 'HCHO')) datas.AVG_HCHO = ParseAvg(__datas, 'HCHO') * unit.HCHO
                    if (CheckAvg(__datas, 'CO2')) datas.AVG_CO2 = ParseAvg(__datas, 'CO2') * unit.CO2
                    if (CheckAvg(__datas, 'CO')) datas.AVG_CO = ParseAvg(__datas, 'CO') * unit.CO
                    if (CheckAvg(__datas, 'PM10')) datas.AVG_PM10 = ParseAvg(__datas, 'PM10') * unit.PM10
                    break;
                case 4 == __datas.equipId:
                    unit.temp = 1
                    unit.maxTemp = 1
                    unit.minTemp = 1
                    unit.diffTemp = 1
                    unit.irCO2 = 1
                    unit.irTVOC = 0.01
                    base.temp = 16
                    base.maxTemp = 16
                    base.minTemp = 16
                    base.diffTemp = 16
                    base.irCO2 = 16
                    base.irTVOC = 16

                    datas.temp = parseInt(__datas.temp, base.temp) * unit.temp
                    datas.maxTemp = parseInt(__datas.maxTemp, base.maxTemp) * unit.maxTemp
                    datas.minTemp = parseInt(__datas.minTemp, base.minTemp) * unit.minTemp
                    datas.diffTemp = parseInt(__datas.diffTemp, base.diffTemp) * unit.diffTemp
                    datas.irCO2 = parseInt(__datas.irCO2, base.irCO2) * unit.irCO2
                    datas.irTVOC = (parseInt(__datas.irTVOC, base.irTVOC) * unit.irTVOC).toFixed(2)
                    break;
                case 5 == parseInt(__datas.equipId):
                    unit.cost = 0.1
                    unit.temp = 1
                    base.cost = 16
                    base.temp = 16

                    datas.power = parseInt(__datas.power)
                    datas.status = parseInt(__datas.status)
                    datas.wind = parseInt(__datas.wind)
                    datas.temp = parseInt(__datas.temp, base.temp) * unit.temp
                    datas.cost = (parseInt(__datas.cost, base.cost) * unit.cost).toFixed(1)
                    datas.errorCode = __datas.errorCode
                    break;
                case 'a' == __datas.equipId:
                    unit.temp = 1
                    unit.humidity = 1
                    unit.produce = 1
                    base.temp = 16
                    base.humidity = 16
                    base.produce = 16

                    datas.power = parseInt(__datas.power)
                    datas.status = parseInt(__datas.status)
                    datas.error = __datas.error
                    datas.temp = parseInt(__datas.temp, base.temp) * unit.temp
                    datas.humidity = parseInt(__datas.humidity, base.humidity) * unit.humidity
                    datas.produce = parseInt(__datas.produce, base.produce) * unit.produce
                    break;
            }

            /** parsed same columnes */
            var time = new Date(__datas.timestamp)
            datas.date = __datas.timestamp == null ? '<span style="color:red; font-size:7px;">未取得資料<span>' : time.getUTCFullYear() + "-" + (time.getUTCMonth() + 1) + "-" + time.getUTCDate()
            datas.time = __datas.timestamp == null ? '<span style="color:red; font-size:7px;">未取得資料<span>' : pad(time.getUTCHours(), 2) + ":" + pad(time.getUTCMinutes(), 2) + ":" + pad(time.getUTCSeconds(), 2)

            parsed.datas = datas
            parsed.unit = unit
            parsed.base = base
            parsed.equipId = __datas.equipId
            return parsed

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }

            function CheckAvg(data, label) {
                if (!data.hasOwnProperty('AVG_' + label))
                    return false
                if (data['AVG_' + label][1] <= 10) {
                    return false
                }
                return true
            }

            function ParseAvg(data, label) {
                if (!data.hasOwnProperty('AVG_' + label))
                    return 0
                var array = data['AVG_' + label]
                var number = array[1] > 0 ? array[1] : 1
                return array[0] / number
            }
        }

        function parseEquipment(element) {
            var parsed = parseData(element)
            var data = {}
            var unit = parsed.unit
            var base = parsed.base
            var title = {}
            var config = {}
            var barColorThreshold = {}
            var decodeContent = {}

            /** same solumns set before */
            data.date = parsed.datas.date
            data.time = parsed.datas.time
            switch (true) {
                case 1 == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'temp'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.temp);
                    data.unit = '度';
                    data.endpoint = isNaN(parsed.datas.temp) ? 1 : parseFloat(parsed.datas.temp) / 100;
                    data.title = '室溫';
                    data.step = 1.0 / 100.0

                    title.power = '電源'
                    title.status = '運轉模式'
                    title.wind = '風速'
                    title.wind_dir = '風向'
                    title.set_temp = '溫度設定'
                    title.temp = '室內溫度'
                    title.cost = '耗能'
                    title.errorCode = '保養/故障碼'
                    title.remain = '保留'
                    title.filterHours = '濾網清洗'

                    config.power = { type: 'select', data: parsed.datas.power, select: ['關', '開'] };
                    config.status = { type: 'select', data: parsed.datas.status, select: ['冷氣', '除溼', '送風', undefined, '暖氣'] };
                    config.wind = { type: 'select', data: parsed.datas.wind, select: ['自動', '低速', '中速', '高速'] };
                    config.wind_dir = { type: 'select', data: parsed.datas.wind_dir, select: ['自動', '角度1', '角度2', '角度3', '角度4', '角度5'] };
                    config.set_temp = { type: 'select', data: parsed.datas.set_temp + ' 度', select: function () { var N = 17, S = 15; return { value: Array(N).fill().map((e, i) => pad((i + S).toString(16), 2)), data: Array(N).fill().map((e, i) => i + S) }; } };
                    config.temp = { type: 'cell', data: parsed.datas.temp + ' 度' };
                    config.cost = { type: 'cell', data: parsed.datas.cost };
                    config.errorCode = { type: 'cell', data: parseInt(parsed.datas.errorCode) };
                    config.remain = { type: 'cell', data: parsed.datas.remain };
                    config.filterHours = { type: 'request', default: '取得中', from: 'filterHours', to: element.address + '_filterHours', format: { url: 'RequireFilterHours', mehtod: 'GET', data: { address: element.address, date: data.date } } }

                    // right : <= ; left : <
                    barColorThreshold.temp = [{ 'right': 22, 'color': 'rgb(255, 0, 0)' }, { 'left': 23, 'right': 25, 'color': 'rgb(245, 170, 50)' }, { 'left': 26, 'color': 'rgb(0, 100, 220)' }]

                    decodeContent = {
                        errorCode: (errorCode) => {
                            switch (parseInt(errorCode)) {
                                case 0: return "正常";
                                case 1: return "室外吐出溫(讀取值)異常";
                                case 2: return "室外AC电压異常(过低或过高)停機";
                                case 3: return "室內外通訊異常";
                                case 4: return "室外驱动通信故障";
                                case 5: return "壓縮機驅動異常";
                                case 6: return "機種&壓縮機配置錯誤";
                                case 7: return "室外散熱片/壓縮機吸入溫(讀取值)異常";
                                case 8: return "室外環境溫(讀取值)異常";
                                case 9: return "室外盤管溫(讀取值)異常";
                                case 10: return "室內室溫(讀取值)異常";
                                case 11: return "室內主管溫(讀取值)異常";
                                case 12: return "室內副管溫(讀取值)異常";
                                case 13: return "室外電流檢知器(讀取值)異常";
                                case 14: return "電壓檢知器(讀取值)異常";
                                case 15: return "IPM故障";
                                case 16: return "其它內機與外機通訊故障";
                                case 17: return "室內風扇故障";
                                case 18: return "室內EEPROM故障";
                                case 19: return "直流風機無回饋 / 外風機故障";
                                case 20: return "室外EEPROM故障";
                                case 21: return "室內中管溫(讀取值)異常";
                                case 200: return "濾網清洗通知";
                                default: return "未定義";
                            }
                        }

                    }

                    data.powerStatus = parsed.datas.power
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['power', 'status', 'wind', 'wind_dir', 'set_temp', 'temp', 'cost', 'filterHours', 'errorCode'];
                    data.cellToRequire = ['filterHours']
                    data.cellToControl = ['power', 'set_temp', 'status', 'wind', 'wind_dir'];
                    data.cellToChangeBase = { 'set_temp': 16 }
                    data.cellDependShowType = { 'month': { 'cost': 'column' }, 'year': { 'cost': 'column' } }
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['set_temp', 'temp', 'cost'], year: ['cost'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示', 'year': '年顯示' }
                    data.cellDecode = ['errorCode']
                    break;
                case 2 == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'cost'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.cost).toFixed(1)
                    data.unit = '度';
                    data.endpoint = isNaN(parsed.datas.cost) ? 1 : parseFloat(parsed.datas.cost) / 25.5;
                    data.title = '耗能';
                    data.step = 1.0 / 25.5

                    title.power = '電源'
                    title.status = '運轉模式'
                    title.wind = '風速'
                    title.cost = '耗能'
                    title.errorCode = '保養/故障碼'
                    title.filterHours = '濾網清洗'

                    config.power = { type: 'select', data: parsed.datas.power, select: ['關', '開'] };
                    config.status = { type: 'select', data: parsed.datas.status, select: ['換氣', '循環清淨', '負離子清淨'] };
                    config.wind = { type: 'select', data: parsed.datas.wind, select: ['自動', '低速', '中速', '高速'] };
                    config.cost = { type: 'cell', data: parsed.datas.cost + ' 度' };
                    config.errorCode = { type: 'cell', data: parsed.datas.errorCode };
                    config.filterHours = { type: 'request', default: '取得中', from: 'filterHours', to: element.address + '_filterHours', format: { url: 'RequireFilterHours', mehtod: 'GET', data: { address: element.address, date: data.date } } }

                    barColorThreshold.cost = [{ 'right': 22, 'color': 'rgb(255, 0, 0)' }, { 'left': 23, 'right': 25, 'color': 'rgb(245, 170, 50)' }, { 'left': 26, 'color': 'rgb(0, 100, 220)' }]

                    data.powerStatus = parsed.datas.power
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['power', 'status', 'wind', 'cost', 'filterHours'];
                    data.cellToRequire = ['filterHours']
                    data.cellToControl = ['power', 'status', 'wind'];
                    data.cellToChangeBase = {}
                    data.cellDependShowType = { 'month': { 'cost': 'column' }, 'year': { 'cost': 'column' } }
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['cost'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示', 'year': '年顯示' }
                    data.cellDecode = []
                    break;
                case 3 == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'CO2'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.CO2);
                    data.unit = 'PPM';
                    data.endpoint = isNaN(parsed.datas.CO2) ? 1 : parseFloat(parsed.datas.CO2) / 1500;
                    data.title = 'CO2'
                    data.step = 1.0 / 150.0

                    title.HCHO = 'HCHO'
                    title.CO2 = 'CO2'
                    title.CO = 'CO'
                    title.PM10 = 'PM2.5'
                    title.TVOC = '溫度'

                    config.HCHO = { type: 'cell', data: parsed.datas.HCHO + ' ppm' };
                    config.CO2 = { type: 'cell', data: parsed.datas.CO2 + ' ppm' };
                    config.CO = { type: 'cell', data: parsed.datas.CO + ' ppm' };
                    config.PM10 = { type: 'cell', data: parsed.datas.PM10 + ' ug/m3' };
                    config.TVOC = { type: 'cell', data: parsed.datas.TVOC + ' 度' };

                    // right : <= ; left : <
                    barColorThreshold.CO2 = [{ 'right': 1000, 'color': 'rgb(0, 100, 220)' }, { 'left': 1000, 'right': 1500, 'color': 'rgb(245, 170, 50)' }, { 'left': 1500, 'color': 'rgb(255, 0, 0)' }]

                    data.powerStatus = 1 // always 1
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['HCHO', 'CO2', 'CO', 'PM10', 'TVOC'];
                    data.cellToRequire = []
                    data.cellToControl = [];
                    data.cellToChangeBase = {}
                    data.cellDependShowType = {}
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['HCHO', 'CO2', 'CO', 'PM10', 'TVOC'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示' }
                    data.cellDecode = []
                    break;
                case 4 == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'minTemp'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.minTemp);
                    data.unit = '度';
                    data.endpoint = isNaN(parsed.datas.minTemp) ? 1 : parseFloat(parsed.datas.minTemp) / 100;
                    data.title = '環境平均溫度'
                    data.step = 1.0 / 100.0

                    title.temp = '感測器溫度'
                    title.maxTemp = '環境最高溫度'
                    title.minTemp = '環境平均溫度'
                    title.diffTemp = '溫度差'
                    title.irCO2 = 'CO2'
                    title.irTVOC = 'TVOC'

                    config.temp = { type: 'cell', data: parsed.datas.temp + ' 度' };
                    config.maxTemp = { type: 'cell', data: parsed.datas.maxTemp + ' 度' };
                    config.minTemp = { type: 'cell', data: parsed.datas.minTemp + ' 度' };
                    config.diffTemp = { type: 'cell', data: parsed.datas.diffTemp + ' 度' };
                    config.irCO2 = { type: 'cell', data: parsed.datas.irCO2 + ' ppm' };
                    config.irTVOC = { type: 'cell', data: parsed.datas.irTVOC + ' ppm' };

                    // right : <= ; left : <
                    barColorThreshold.minTemp = [{ 'right': 22, 'color': 'rgb(255, 0, 0)' }, { 'left': 23, 'right': 25, 'color': 'rgb(245, 170, 50)' }, { 'left': 26, 'color': 'rgb(0, 100, 220)' }]

                    data.powerStatus = 1 // always 1
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['temp', 'maxTemp', 'minTemp', 'diffTemp', 'irCO2', 'irTVOC'];
                    data.cellToRequire = []
                    data.cellToControl = [];
                    data.cellToChangeBase = {}
                    data.cellDependShowType = {}
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['temp', 'maxTemp', 'minTemp', 'diffTemp', 'irCO2', 'irTVOC'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示' }
                    data.cellDecode = []
                    break;
                case 5 == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'temp'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.temp).toFixed(1)
                    data.unit = '度';
                    data.endpoint = isNaN(parsed.datas.temp) ? 1 : parseFloat(parsed.datas.temp) / 100;
                    data.title = '溫度';
                    data.step = 1.0 / 100.0

                    title.power = '電源'
                    title.status = '運轉模式'
                    title.wind = '風速'
                    title.temp = '溫度'
                    title.cost = '耗能'
                    title.errorCode = '保養/故障碼'

                    config.power = { type: 'select', data: parsed.datas.power, select: ['關', '開'] };
                    config.status = { type: 'select', data: parsed.datas.status, select: ['舒適風', '睡眠風', '自然風', 'ECO'] };
                    config.wind = { type: 'select', data: parsed.datas.wind, select: [undefined, '速度L', '速度2', '速度4', '速度5', '速度6', '速度H'] };
                    config.temp = { type: 'cell', data: parsed.datas.temp + ' 度' };
                    config.cost = { type: 'cell', data: parsed.datas.cost + ' 度' };
                    config.errorCode = { type: 'cell', data: parsed.datas.errorCode };

                    barColorThreshold.temp = [{ 'right': 22, 'color': 'rgb(255, 0, 0)' }, { 'left': 23, 'right': 25, 'color': 'rgb(245, 170, 50)' }, { 'left': 26, 'color': 'rgb(0, 100, 220)' }]

                    data.powerStatus = parsed.datas.power
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['power', 'status', 'wind', 'temp', 'cost'];
                    data.cellToRequire = []
                    data.cellToControl = ['power', 'status', 'wind'];
                    data.cellToChangeBase = {}
                    data.cellDependShowType = { 'month': { 'cost': 'column' }, 'year': { 'cost': 'column' } }
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['temp', 'cost'], year: ['cost'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示', 'year': '年顯示' }
                    data.cellDecode = []
                    break;
                case 'a' == parsed.equipId:
                    if (typeof _elements.$chart[element.className] === 'undefined') {
                        data.depend = 'produce'
                    } else {
                        data.depend = _elements.$chart[element.className].attr('data-depend')
                    }

                    data.data = parseFloat(parsed.datas.produce).toFixed(0)
                    data.unit = ' 台';
                    data.endpoint = isNaN(parsed.datas.produce) ? 1 : parseFloat(parsed.datas.produce) / 2000.0;
                    data.title = '生產台數';
                    data.step = 1.0 / 200.0

                    title.power = '電源'
                    title.status = '運轉模式'
                    title.error = '異常'
                    title.temp = '溫度'
                    title.humidity = '濕度'
                    title.produce = '生產台數'

                    config.power = { type: 'select', data: parsed.datas.power, select: ['關', '開'] };
                    config.status = { type: 'select', data: parsed.datas.status, select: ['暫停', '啟動'] };
                    config.error = { type: 'cell', data: ((error) => { var map = {}; Array(16).fill().map((e, i) => map[i.toString(16)] = i > 0 ? i.toString(16) : '正常'); return map[error]; })(parsed.datas.error) }
                    config.temp = { type: 'cell', data: parsed.datas.temp + ' 度' };
                    config.humidity = { type: 'cell', data: parsed.datas.humidity + ' %' };
                    config.produce = { type: 'cell', data: parsed.datas.produce };

                    barColorThreshold.produce = [{ 'left': -1, 'color': 'rgb(0, 100, 195)' }]

                    data.powerStatus = parsed.datas.power
                    data.configTitle = title;
                    data.configData = config;
                    data.configUnit = unit;
                    data.cellToShow = ['power', 'status', 'error', 'temp', 'humidity', 'produce'];
                    data.cellToRequire = []
                    data.cellToControl = [];
                    data.cellToChangeBase = {}
                    data.cellDependShowType = { 'month': { 'produce': 'column' } }
                    data.barColorThreshold = barColorThreshold
                    data.decodeContent = decodeContent
                    data.displayCellOnLineChart = { default: ['temp', 'humidity', 'produce'] }
                    data.displayTypeOnLineChart = { 'now': '即時顯示', 'day': '日顯示', 'month': '月顯示' }
                    data.cellDecode = []
                    break;
            }

            return data;

            function pad(n, width, z) {
                z = z || '0';
                n = n + '';
                return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
            }
        }

        function parseFloor(floor) {
            var pre = Math.abs(floor) >= 10 ? '十' : '';
            var ground = Number(floor) > 0 ? '' : '地下';
            if (Number(floor) == 10) return ground + pre + '樓';
            switch ((Number(floor)) % 10) {
                case 0: return '室外';
                case 1: return ground + pre + '一樓';
                case 2: return ground + pre + '二樓';
                case 3: return ground + pre + '三樓';
                case 4: return ground + pre + '四樓';
                case 5: return ground + pre + '五樓';
                case 6: return ground + pre + '六樓';
                case 7: return ground + pre + '七樓';
                case 8: return ground + pre + '八樓';
                case 9: return ground + pre + '九樓';
            }
        }
    }

    function reportCircleGraph() {

        $.fn.percentCircle = function pie(options) {

            var settings = $.extend({
                width: 130,
                trackColor: '#fff',
                barColor: '#fff',
                titleColor: '#fff',
                numberColor: '#fff',
                powerOffColor: '#fff',
                disconnectColor: '#fff',
                barWeight: 5,
                dataDepend: '',
                barColorThreshold: {},
                startPercent: 0,
                endPercent: 1,
                numberTitle: '',
                number: '',
                fps: 60,
                address: '',
                power: 0,
                timestamp: '',
                update: 0.01
            }, options);

            this.attr('data-depend', settings.dataDepend)

            this.css({
                width: settings.width,
                height: settings.width
            });

            var _this = this,
                canvasWidth = settings.width,
                canvasHeight = canvasWidth,
                id = $('canvas').length,
                canvasElement = $('<canvas id="' + id + '" address="' + settings.address + '"width="' + canvasWidth + '" height="' + canvasHeight + '"></canvas>'),
                canvas = canvasElement.get(0).getContext('2d'),
                centerX = canvasWidth / 2,
                centerY = canvasHeight / 2,
                radius = settings.width / 2 - settings.barWeight / 2,
                counterClockwise = false,
                fps = 1000 / settings.fps,
                update = settings.update;

            this.angle = settings.startPercent;

            this.drawInnerArc = function (startAngle, percentFilled, color) {
                var drawingArc = true;
                canvas.beginPath();
                canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
                canvas.strokeStyle = color;
                canvas.lineWidth = settings.barWeight - 2;
                canvas.stroke();
                drawingArc = false;
            };

            this.drawOuterArc = function (startAngle, percentFilled, color) {
                var drawingArc = true;
                canvas.beginPath();
                canvas.arc(centerX, centerY, radius, (Math.PI / 180) * (startAngle * 360 - 90), (Math.PI / 180) * (percentFilled * 360 - 90), counterClockwise);
                canvas.strokeStyle = color;
                canvas.lineWidth = settings.barWeight;
                canvas.lineCap = 'round';
                canvas.stroke();
                drawingArc = false;
            };

            this.drawNumberTitle = function (text, col, row, color = settings.titleColor) {
                var drawingText = true;
                var font = "20px Microsoft JhengHei";
                canvas.font = font;
                canvas.fillStyle = color;
                canvas.fillText(text, col - this.textWidth(text, font) / 2, row);

                canvas.strokeStyle = 'rgba(' + color.substring(color.indexOf('(') + 1, color.indexOf(')')) + ', 0.7)';
                canvas.lineWidth = 1.7;
                canvas.strokeText(text, col - this.textWidth(text, font) / 2, row);
                drawingText = false;
            }

            this.drawNumber = function (number, col, row, color = settings.numberColor) {
                var drawingText = true;
                var font = "17px Microsoft JhengHei";
                canvas.font = font;
                canvas.fillStyle = color;
                canvas.fillText(number, col - this.textWidth(number, font) / 2, row);

                canvas.strokeStyle = 'rgba(' + color.substring(color.indexOf('(') + 1, color.indexOf(')')) + ', 0.7)';
                canvas.lineWidth = 1.7;
                canvas.strokeText(number, col - this.textWidth(number, font) / 2, row);
                drawingText = false;
            }

            this.fillChart = function (stop) {
                if (_this.angle.toPrecision(2) != stop.toPrecision(2) && (_this.angle - stop) < 0) {
                    var loop = setInterval(function () {
                        canvas.clearRect(0, 0, canvasWidth, canvasHeight);

                        if (parseInt(settings.power) > 0) {
                            _this.drawInnerArc(0, 360, settings.trackColor);
                            _this.drawOuterArc(settings.startPercent, _this.angle, _this.color());
                            _this.drawNumberTitle(settings.numberTitle, canvasWidth / 2, canvasHeight / 2, _this.color());
                            _this.drawNumber(settings.number, canvasWidth / 2, canvasHeight / 2 + 20, _this.color());
                        } else {
                            _this.drawInnerArc(0, 360, settings.trackColor);
                            _this.drawOuterArc(settings.startPercent, _this.angle, settings.powerOffColor);
                            _this.drawNumberTitle("關閉", canvasWidth / 2, canvasHeight / 2 + 10, settings.powerOffColor);
                        }

                        _this.angle += update;
                        if (_this.angle.toPrecision(2) == stop.toPrecision(2) || ((_this.angle - stop) > 0)) {
                            clearInterval(loop);
                        }
                    }, fps);
                }
            };

            this.update = function (options) {
                settings = $.extend(settings, options);
                canvas.clearRect(0, 0, canvasWidth, canvasHeight);

                var current = new Date()
                var timestamp = new Date(settings.timestamp)
                if ((current - timestamp) / 1000 > 600) {
                    _this.drawInnerArc(0, 360, settings.trackColor);
                    _this.drawOuterArc(settings.startPercent, _this.angle, settings.disconnectColor);
                    _this.drawNumberTitle("離線", canvasWidth / 2, canvasHeight / 2 + 10, settings.disconnectColor);
                    return
                }

                if (parseInt(settings.power) > 0) {
                    _this.drawInnerArc(0, 360, settings.trackColor);
                    _this.drawOuterArc(settings.startPercent, _this.angle, _this.color());
                    _this.drawNumberTitle(settings.numberTitle, canvasWidth / 2, canvasHeight / 2, _this.color());
                    _this.drawNumber(settings.number, canvasWidth / 2, canvasHeight / 2 + 20, _this.color());
                } else {
                    _this.drawInnerArc(0, 360, settings.trackColor);
                    _this.drawOuterArc(settings.startPercent, _this.angle, settings.powerOffColor);
                    _this.drawNumberTitle("關閉", canvasWidth / 2, canvasHeight / 2 + 10, settings.powerOffColor);
                }

                this.fillChart(settings.endPercent);
            };

            this.color = function () {
                var color = settings.barColor
                var number = parseInt(settings.number)
                if (typeof settings.barColorThreshold[settings.dataDepend] === 'undefined') {
                    return color
                } else {
                    settings.barColorThreshold[settings.dataDepend].forEach(function (threshold) {
                        var left = typeof threshold['left'] === 'undefined' ? undefined : threshold['left'];
                        var right = typeof threshold['right'] === 'undefined' ? undefined : threshold['right'];
                        if (left == undefined) {
                            if (number <= right) {
                                //console.info(number, "<=", right)
                                color = threshold.color;
                            }
                        } else if (right == undefined) {
                            if (left < number) {
                                //console.info(left, "<", number)
                                color = threshold.color;
                            }
                        } else {
                            if (left < number && number <= right) {
                                //console.info(left, '<', number, '&&', number, '<=', right)
                                color = threshold.color;
                            }
                        }
                    })
                    return color
                }
            }

            this.update()
            this.append(canvasElement);
            return this;
        };

        $.fn.lineChart = function linechart(options) {
            var settings = $.extend({
                className: '',
                title: '',
                address: '',
                type: 'spline',
                dataDepend: '',
                dataUnit: {},
                hisDataPoint: null, // history datapoint to saved
                dataPoints: [],
                HdataPoints: [],
                LdataPoints: [],
                AVGdataPoints: [],
                historyLastData: null,
                display: '',
                depend: '',
                month: '',
                day: '',
                dependCallback: '',
                defaultCallback: '',
                dependShowTypeCallback: null,
                dependShowType: {},
                lastCallFunction: null, // if reset function is called, then this last call function cat help to recovery data
                insertCntData: true, // insert current data 
                defaultShowType: 'spline',
                combineHLColumn: ['cost', 'produce']
            }, options);
            var _this = this;

            this.attr('data-depend', settings.dataDepend)
            $(settings.day).append('<option value="-1">未選擇</option>')
            for (var i = 1; i <= 31; i++) {
                $(settings.day).append('<option value="' + i + '">' + i + '日</option>')
            }
            $(settings.day).selectpicker({ 'size': 10 })
            $('div' + settings.day).on('changed.bs.select', function (e) {
                _this.displayDependDay();
            });

            $(settings.month).append('<option value="-1">未選擇</option>')
            for (var i = 1; i <= 12; i++) {
                $(settings.month).append('<option value="' + i + '">' + i + '月</option>')
            }
            $(settings.month).selectpicker({})
            $('div' + settings.month).on('changed.bs.select', function (e) {
                var depend = $(settings.depend).selectpicker('val');
                switch (true) {
                    case depend == 'day':
                        _this.displayDependDay();
                        break;
                    case depend == 'month':
                        _this.displayDependMonth();
                        break;
                }
            });

            $(settings.year).append('<option value="-1">未選擇</option>')
            for (var year = 2017; year <= parseInt(new Date().getFullYear()); year++) {
                $(settings.year).append('<option value="' + year + '">' + year + '年</option>')
            }
            $(settings.year).selectpicker({})
            $('div' + settings.year).on('changed.bs.select', function (e) {
                var depend = $(settings.depend).selectpicker('val');
                switch (true) {
                    case depend == 'year':
                        _this.displayDependYear();
                        break;
                }
            });

            $(settings.depend).selectpicker({})
            $('div' + settings.depend).on('changed.bs.select', function (e) {
                _this.displayDependChange($(e.currentTarget).find('.selectpicker').val())
            });
            $('div' + settings.depend).on('loaded.bs.select', function (e) {
                _this.displayDependChange($(e.currentTarget).find('.selectpicker').val())
            });

            var chart = new CanvasJS.Chart("chartContainer_" + options.className, {
                title: {
                    text: settings.title[settings.dataDepend],
                    fontSize: '17',
                    fontFamily: 'Microsoft JhengHei'
                },
                axisY: {
                    interval: settings.dataPoints.length == 1 ? 1 : calInterval(settings.dataPoints),
                    includeZero: false
                },
                dataPointMaxWidth: 20,
                data: [{
                    type: settings.type,
                    dataPoints: settings.dataPoints
                }, {
                    type: settings.type,
                    color: "green",
                    dataPoints: settings.HdataPoints
                }, {
                    type: settings.type,
                    dataPoints: settings.LdataPoints
                }, {
                    type: settings.type,
                    dataPoints: settings.AVGdataPoints
                }]
            });

            this.update = function (options) {
                if (settings.insertCntData) {
                    settings.dataPoints.push(options.dataPoints);
                    if (settings.dataPoints.length > 0) {
                        chart.options.axisY.interval = calInterval(settings.dataPoints)
                    } else {
                        chart.options.axisY.interval = 1
                    }
                    chart.render();
                }
            }

            function calInterval(datas) {
                var copy = JSON.parse(JSON.stringify(datas));
                copy.sort(SortByYAxis)
                if (copy.length == 0)
                    return 0
                var interval = (copy[copy.length - 1].y - copy[0].y) / 5
                var round = Math.round(interval / 10) * 10
                return interval

                function SortByYAxis(a, b) {
                    return (parseFloat(a.y) < parseFloat(b.y)) ? -1 : 1;
                }
            }

            this.hasHisDataPoint = function () {
                return settings.hisDataPoint != null ? true : false
            }

            this.setHisDataPoint = function (dataPoint) {
                settings.hisDataPoint = dataPoint

                if (['now'].indexOf($(settings.depend).selectpicker('val')) >= 0) {
                    settings.insertCntData = true
                }
            }

            this.cntDataPoint = function () {
                return settings.hisDataPoint
            }

            this.insertHistory = function (dataPoints) {
                if (['now', 'day'].indexOf($(settings.depend).selectpicker('val')) == -1)
                    return;

                settings.dataPoints = []
                chart.options.data[0].dataPoints = settings.dataPoints
                Array.prototype.push.apply(settings.dataPoints, dataPoints)
                chart.options.axisY.interval = calInterval(settings.dataPoints)
                chart.render();
            }

            this.insertHighHistory = function (dataPoints) {
                if ($(settings.depend).selectpicker('val') == 'now')
                    return;

                settings.HdataPoints = []
                chart.options.data[1].dataPoints = settings.HdataPoints
                Array.prototype.push.apply(settings.HdataPoints, dataPoints)
                // chart.render();
            }

            this.insertLowHistory = function (dataPoints) {
                if ($(settings.depend).selectpicker('val') == 'now')
                    return;
                if (settings.combineHLColumn.indexOf(this.attr('data-depend')) >= 0)
                    return;

                settings.LdataPoints = []
                chart.options.data[2].dataPoints = settings.LdataPoints
                Array.prototype.push.apply(settings.LdataPoints, dataPoints)
                // chart.render();
            }

            this.insertAVGHistory = function (dataPoints) {
                if ($(settings.depend).selectpicker('val') == 'now')
                    return;
                if (settings.combineHLColumn.indexOf(this.attr('data-depend')) >= 0)
                    return;
                settings.AVGdataPoints = []
                chart.options.data[3].dataPoints = settings.AVGdataPoints
                Array.prototype.push.apply(settings.AVGdataPoints, dataPoints)
            }

            this.reset = function (callback = null) {
                var currentShowDepend = $(settings.depend).selectpicker('val')
                // debug = chart
                settings.dataPoints = []
                settings.HdataPoints = []
                settings.LdataPoints = []
                settings.AVGdataPoints = []
                settings.dataDepend = _this.attr('data-depend')
                chart.title.set("text", settings.title[settings.dataDepend])
                chart.options.data[0].dataPoints = settings.dataPoints
                chart.options.data[1].dataPoints = settings.HdataPoints
                chart.options.data[2].dataPoints = settings.LdataPoints
                chart.options.data[3].dataPoints = settings.AVGdataPoints
                if (settings.dependShowType[currentShowDepend] !== undefined && settings.dependShowType[currentShowDepend] !== 'undefined' &&
                    settings.dependShowType[currentShowDepend][settings.dataDepend] !== undefined && settings.dependShowType[currentShowDepend][settings.dataDepend] !== 'undefined') {
                    chart.data[1].set("type", settings.dependShowType[currentShowDepend][settings.dataDepend])
                    chart.data[2].set("type", settings.dependShowType[currentShowDepend][settings.dataDepend])
                    chart.data[3].set("type", settings.dependShowType[currentShowDepend][settings.dataDepend])
                } else {
                    chart.data[1].set("type", settings.defaultShowType)
                    chart.data[2].set("type", settings.defaultShowType)
                    chart.data[3].set("type", settings.defaultShowType)
                }
                chart.render()

                if (callback != null) {
                    callback()
                } else if (settings.lastCallFunction) {
                    settings.lastCallFunction()
                }
            }

            this.refresh = function () {
                var datas = []
                var interval = 0
                if (['now', 'day'].indexOf($(settings.depend).selectpicker('val')) == -1) {
                    datas = settings.LdataPoints.concat(settings.HdataPoints)
                } else {
                    datas = settings.dataPoints
                }

                interval = calInterval(datas)
                if (datas.length > 0) {
                    chart.options.axisY.interval = interval
                } else {
                    chart.options.axisY.interval = 1
                }
                chart.render()
            }

            this.displayDependChange = function (depend) {
                var time = new Date()
                if (settings.dependShowTypeCallback != null)
                    settings.dependShowTypeCallback(depend)
                switch (true) {
                    case depend == 'now':
                        $(settings.display).selectpicker('hide');

                        settings.insertCntData = false
                        _this.displayCurrentData()
                        break;
                    case depend == 'year':
                        $(settings.display).selectpicker('hide');
                        $(settings.day).selectpicker('hide');
                        $(settings.month).selectpicker('hide');
                        $(settings.year).selectpicker('val', time.getFullYear());
                        $(settings.year).selectpicker('show');
                        settings.insertCntData = false
                        _this.displayDependYear()
                        break;
                    case depend == 'month':
                        $(settings.day).selectpicker('hide');
                        $(settings.month).selectpicker('val', time.getMonth() + 1)
                        $(settings.month).selectpicker('show');
                        $(settings.year).selectpicker('val', time.getFullYear());
                        $(settings.year).selectpicker('show');
                        settings.insertCntData = false
                        _this.displayDependMonth()
                        break;
                    case depend == 'day':
                        $(settings.month).selectpicker('val', time.getMonth() + 1)
                        $(settings.month).selectpicker('show');
                        $(settings.day).selectpicker('val', time.getDate())
                        $(settings.day).selectpicker('show');
                        $(settings.year).selectpicker('val', time.getFullYear());
                        $(settings.year).selectpicker('show');
                        settings.insertCntData = false
                        _this.displayDependDay()
                        break;
                }
            }

            this.displayCurrentData = function () {
                var date = new Date()
                var RgStart = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-') + ' 00:00:00'
                var RgEnd = [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-') + ' ' + [date.getHours(), date.getMinutes(), date.getSeconds()].join(':')

                settings.lastCallFunction = _this.displayCurrentData;
                _this.reset(function () {
                    settings.defaultCallback(settings.className, settings.address, RgStart, RgEnd)
                })
            }

            this.displayDependYear = function () {
                var date = new Date()
                var year = parseInt($(settings.year).selectpicker('val'))
                if (year >= 0) {
                    var RgStart = [year, 1, 1].join('-') + ' 00:00:00'
                    var RgEnd = [year, 12, 31].join('-') + ' 23:59:59'

                    settings.lastCallFunction = _this.displayDependYear;
                    _this.reset(function () {
                        settings.dependCallback(settings.className, settings.address, RgStart, RgEnd)
                    })
                }
            }

            this.displayDependMonth = function () {
                /*** This Code is used for only Month select, not for day presentation select */
                var date = new Date()
                var month = parseInt($(settings.month).selectpicker('val'))
                var year = parseInt($(settings.year).selectpicker('val'))
                var lastDay = new Date(date)
                lastDay.setMonth(month)
                lastDay.setDate(0)

                if (month >= 0) {
                    var RgStart = [year, month, 1].join('-') + ' 00:00:00'
                    var RgEnd = [year, month, lastDay.getDate()].join('-') + ' 23:59:59'

                    settings.lastCallFunction = _this.displayDependMonth;
                    _this.reset(function () {
                        settings.dependCallback(settings.className, settings.address, RgStart, RgEnd)
                    })
                }
            }

            this.displayDependDay = function () {
                var date = new Date()
                var day = parseInt($(settings.day).selectpicker('val'))
                var month = parseInt($(settings.month).selectpicker('val'))
                var year = parseInt($(settings.year).selectpicker('val'))

                if (day >= 0 && month >= 0) {
                    var RgStart = [year, month, day].join('-') + ' 00:00:00'
                    var RgEnd = [year, month, day].join('-') + ' 23:59:59'

                    settings.lastCallFunction = _this.displayDependDay;
                    _this.reset(function () {
                        settings.defaultCallback(settings.className, settings.address, RgStart, RgEnd)
                    })
                }
            }

            this.currentDisplayType = function () {
                return $(settings.depend).selectpicker('val')
            }

            chart.render();
            return this;
        }

        $.fn.historyChart = function historyChart(options) {
            var settings = $.extend({
                className: '',
                address: '',
                column: '',
                show: '',
                title: '',
                dataPoints: [],
                display: '',
                depend: '',
                year: '',
                month: '',
                dependCallback: undefined,
                defaultType: 'line'
            }, options);
            var _this = this;
            var lastCallFunction = undefined // if reset function is called, then this last call function cat help to recovery data

            this.attr('data-depend', settings.depend)

            $(settings.month).append('<option value="-1">未選擇</option>')
            for (var i = 1; i <= 12; i++) {
                $(settings.month).append('<option value="' + i + '">' + i + '月</option>')
            }
            $(settings.month).selectpicker({})
            $('div' + settings.month).on('changed.bs.select', function (e) {
                var depend = settings.depend
                switch (true) {
                    case depend == 'month':
                        _this.displayDependMonth();
                        break;
                }
            });

            $(settings.year).append('<option value="-1">未選擇</option>')
            for (var year = 2017; year <= parseInt(new Date().getFullYear()); year++) {
                $(settings.year).append('<option value="' + year + '">' + year + '年</option>')
            }
            $(settings.year).selectpicker({})
            $('div' + settings.year).on('changed.bs.select', function (e) {
                var depend = settings.depend
                switch (true) {
                    case depend == 'month':
                        _this.displayDependMonth();
                        break;
                }
            });

            var chart = new CanvasJS.Chart("chartContainer_" + settings.className, {
                title: {
                    text: settings.title,
                    fontSize: '17',
                    fontFamily: 'Microsoft JhengHei'
                },
                axisY: {
                    includeZero: false,
                    maximum: 100,
                    minimum: 0
                },
                dataPointMaxWidth: 20,
                data: [{
                    type: settings.defaultType,
                    dataPoints: settings.dataPoints
                }]
            });

            this.update = function (dataPoints) {
                settings.dataPoints = []
                chart.options.data[0].dataPoints = settings.dataPoints

                Array.prototype.push.apply(settings.dataPoints, dataPoints)
                chart.render();
            }

            this.reset = function (callback = null) {
                var currentShowDepend = $(settings.depend).selectpicker('val')
                // debug = chart
                settings.dataPoints = []
                settings.dataDepend = _this.attr('data-depend')
                chart.title.set("text", settings.title)
                chart.options.axisY.interval = 10;
                chart.options.data[0].dataPoints = settings.dataPoints
                chart.render()

                if (callback != null) {
                    callback()
                } else if (settings.lastCallFunction) {
                    settings.lastCallFunction()
                }
            }

            this.refresh = function () {
                chart.render();
            }

            this.displayDependChange = function (depend) {
                var time = new Date()
                switch (true) {
                    case depend == 'month':
                        $(settings.day).selectpicker('hide');
                        $(settings.month).selectpicker('val', time.getMonth() + 1)
                        $(settings.month).selectpicker('show');
                        $(settings.year).selectpicker('val', time.getFullYear());
                        $(settings.year).selectpicker('show');
                        settings.insertCntData = false
                        _this.displayDependMonth()
                        break;
                }
            }

            this.displayDependMonth = function () {
                /*** This Code is used for only Month select, not for day presentation select */
                var date = new Date()
                var month = parseInt($(settings.month).selectpicker('val'))
                var year = parseInt($(settings.year).selectpicker('val'))
                var lastDay = new Date(date)
                lastDay.setMonth(month)
                lastDay.setDate(0)

                if (month >= 0 && year >= 0) {
                    var RgStart = [year, month, 1].join('-') + ' 00:00:00'
                    var RgEnd = [year, month, lastDay.getDate()].join('-') + ' 23:59:59'
                    console.info(RgStart, RgEnd)

                    settings.lastCallFunction = _this.displayDependMonth;
                    _this.reset(function () {
                        if (settings.dependCallback !== undefined) {
                            settings.dependCallback(settings.className, settings.address, settings.column, RgStart, RgEnd)
                        }
                    })
                }
            }

            chart.render();
            return this;
        }

        $.fn.historyRSSIChart = function historyChart(options) {
            var settings = $.extend({
                className: '',
                address: '',
                column: '',
                show: '',
                title: '',
                dataPoints: [],
                display: '',
                depend: '',
                year: '',
                month: '',
                dependCallback: undefined,
                defaultType: 'line'
            }, options);
            var _this = this;
            var lastCallFunction = undefined // if reset function is called, then this last call function cat help to recovery data

            this.attr('data-depend', settings.depend)

            $(settings.day).append('<option value="-1">未選擇</option>')
            for (var i = 1; i <= 31; i++) {
                $(settings.day).append('<option value="' + i + '">' + i + '日</option>')
            }
            $(settings.day).selectpicker({ 'size': 10 })
            $('div' + settings.day).on('changed.bs.select', function (e) {
                _this.displayDependDay();
            });

            $(settings.month).append('<option value="-1">未選擇</option>')
            for (var i = 1; i <= 12; i++) {
                $(settings.month).append('<option value="' + i + '">' + i + '月</option>')
            }
            $(settings.month).selectpicker({})
            $('div' + settings.month).on('changed.bs.select', function (e) {
                var depend = settings.depend
                switch (true) {
                    case depend == 'day':
                        _this.displayDependDay();
                        break;
                }
            });

            $(settings.year).append('<option value="-1">未選擇</option>')
            for (var year = 2017; year <= parseInt(new Date().getFullYear()); year++) {
                $(settings.year).append('<option value="' + year + '">' + year + '年</option>')
            }
            $(settings.year).selectpicker({})
            $('div' + settings.year).on('changed.bs.select', function (e) {
                var depend = settings.depend
                switch (true) {
                    case depend == 'day':
                        _this.displayDependDay();
                        break;
                }
            });

            var chart = new CanvasJS.Chart("chartContainer_" + settings.className, {
                title: {
                    text: settings.title,
                    fontSize: '17',
                    fontFamily: 'Microsoft JhengHei'
                },
                axisY: {
                    includeZero: false
                },
                dataPointMaxWidth: 20,
                data: [{
                    type: settings.defaultType,
                    dataPoints: settings.dataPoints
                }]
            });

            this.update = function (dataPoints) {
                settings.dataPoints = []
                chart.options.data[0].dataPoints = settings.dataPoints

                Array.prototype.push.apply(settings.dataPoints, dataPoints)
                chart.render();
            }

            this.reset = function (callback = null) {
                var currentShowDepend = $(settings.depend).selectpicker('val')
                // debug = chart
                settings.dataPoints = []
                settings.dataDepend = _this.attr('data-depend')
                chart.title.set("text", settings.title)
                chart.options.axisY.interval = 10;
                chart.options.data[0].dataPoints = settings.dataPoints
                chart.render()

                if (callback != null) {
                    callback()
                } else if (settings.lastCallFunction) {
                    settings.lastCallFunction()
                }
            }

            this.refresh = function () {
                chart.render();
            }

            this.displayDependChange = function (depend) {
                var time = new Date()
                switch (true) {
                    case depend == 'day':
                        $(settings.month).selectpicker('val', time.getMonth() + 1)
                        $(settings.month).selectpicker('show');
                        $(settings.day).selectpicker('val', time.getDate())
                        $(settings.day).selectpicker('show');
                        $(settings.year).selectpicker('val', time.getFullYear());
                        $(settings.year).selectpicker('show');
                        settings.insertCntData = false
                        _this.displayDependDay()
                        break;
                }
            }

            this.displayDependDay = function () {
                var date = new Date()
                var day = parseInt($(settings.day).selectpicker('val'))
                var month = parseInt($(settings.month).selectpicker('val'))
                var year = parseInt($(settings.year).selectpicker('val'))

                if (day >= 0 && month >= 0) {
                    var RgStart = [year, month, day].join('-') + ' 00:00:00'
                    var RgEnd = [year, month, day].join('-') + ' 23:59:59'
                    console.info(RgStart, RgEnd)

                    settings.lastCallFunction = _this.displayDependMonth;
                    _this.reset(function () {
                        if (settings.dependCallback !== undefined) {
                            settings.dependCallback(settings.className, settings.address, settings.column, RgStart, RgEnd)
                        }
                    })
                }
            }

            chart.render();
            return this;
        }

        $.fn.configTable = function config(options) {
            var settings = $.extend({
                className: '',
                title: '',
                data: '',
                configTitle: [],
                configData: [],
                element: null,
                cellToControl: [],
                cellToChangeBase: {},
                depends: [],
                server: ''
            }, options);

            var _this = this;
            var classToSend = 'device_' + settings.element.address + '_configsend';
            var isLocking = settings.depends.length > 0 && ['1'].indexOf(settings.element.equipId) == -1 ? true : false;
            console.info(settings.element.address)

            this.tableTemplate = function () {
                var tr = [];
                for (var cell in settings.configTitle) {
                    if (settings.cellToControl.indexOf(cell) > -1) {
                        tr.push('<tr><td>' + settings.configTitle[cell] + '</td><td>' + (settings.configData[cell].type == 'cell' ? this.buildInputCell(settings.configData[cell].data, cell) : (typeof settings.configData[cell].select === "function" ? this.buildSelectCellByFunction(settings.configData[cell].data, settings.configData[cell].select, cell) : this.buildSelectCell(settings.configData[cell].data, settings.configData[cell].select, cell))) + '</td></td>');
                    }
                }
                return [
                    '<div style="height:220px; overflow-y:auto">',
                    '<form method="get" action="{{server}}/EquipControll">'.replace('{{server}}', settings.server),
                    '<table class="config-table table table-condensed table-hover">',
                    '<input class="controlButton" type="button" style="width:100%; height: 33px; ' + (isLocking ? 'color: rgba(255,0,0,0.5);' : '') + '" value="' + (isLocking ? '互動控制鎖定' : '修改狀態') + '" ' + (isLocking ? 'disabled="disabled"' : '') + '>',
                    '<input type="text" style="display:none" name="address" value="{{address}}">'.replace('{{address}}', settings.element.address),
                    tr.join(''),
                    '</form>',
                    '</table>',
                    '</div>'
                ].join('');
            };

            this.buildSelectCell = function (value, data, cell) {
                value = value.toString().split(/ +/)[0]
                var options = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == null)
                        continue;
                    options.push('<option value="' + i + '" ' + (value == i ? 'selected="selected"' : '') + '>' + data[i] + '</option>');
                }
                return [
                    '<select name="' + cell + '" style="width: 100%;" ' + (isLocking ? 'disabled="disabled"' : '') + '>',
                    options.join(''),
                    '</select>'
                ].join('');
            }

            this.buildSelectCellByFunction = function (value, callback, cell) {
                value = value.toString().split(/ +/)[0]
                var obj = callback()
                var data = obj.data
                var values = obj.value
                var options = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i] == null)
                        continue;
                    options.push('<option value="' + values[i] + '" ' + (value == data[i] ? 'selected="selected"' : '') + '>' + data[i] + '</option>');
                }
                return [
                    '<select name="' + cell + '" style="width: 100%;" ' + (isLocking ? 'disabled="disabled"' : '') + '>',
                    options.join(''),
                    '</select>'
                ].join('');
            }

            this.buildInputCell = function (value, cell) {
                value = value.toString().split(/ +/)[0]
                if (typeof settings.cellToChangeBase[cell] !== 'undefined')
                    return ['<input type="number" step="1" class="' + cell + '_edit" value="' + value + '" style="width: 100%; height: 24px" ' + (isLocking ? 'disabled="disabled"' : '') + '>',
                    '<input type="text" class="' + cell + '" name="' + cell + '" value="' + value.toString(settings.cellToChangeBase[cell]) + '" style="display: none">'].join()
                else
                    return '<input type="number" step="1" name="' + cell + '" value="' + value + '" style="width: 100%; height: 24px" ' + (isLocking ? 'disabled="disabled"' : '') + '>'
            }

            this.update = function (options) {
                settings = $.extend(settings, options);
            };


            var tableElement = this.tableTemplate();
            this.append(tableElement);
            return this;
        }

        $.fn.controlTable = function config(options) {
            var settings = $.extend({
                company: '',
                element: null,
                server: '',
                depend: undefined,
                map: null
            }, options);

            var _this = this;
            var classToSend = 'device_' + settings.element.address + '_controlsend';
            var isLocking = settings.depend[settings.element.address].depends.length > 0 ? true : false
            var addressCanDepend = {}
            var addressCanNotify = {}
            $.each(settings.depend, function (index, value) {
                // 可以把其他裝置當作控制依據
                if (value.floor == settings.depend[settings.element.address].floor &&
                    ((settings.element.equipId == 2 && value.equipId == 3) || (settings.element.equipId == 1 && value.equipId == 4))) {
                    addressCanDepend[index] = value
                } else if (settings.element.equipId == 1) {
                    addressCanDepend[settings.element.address] = settings.element;
                }

                // 可以把其他裝置當作提醒依據
                if (value.floor == settings.depend[settings.element.address].floor &&
                    ((settings.element.equipId == 1 && value.equipId == 3) || (settings.element.equipId == 2 && value.equipId == 3))) {
                    addressCanNotify[index] = value
                }
            })
            this.tableTemplate = function () {
                var tr = [];
                $.each(addressCanDepend, function (index, element) {
                    var dependAddr = index
                    var actionsObjs = settings.depend[dependAddr].actions
                    if (dependAddr == settings.element.address) {
                        var actions = actionsObjs[dependAddr]
                        var hasTimer_s = !isEmptyObject(actionsObjs) && typeof actions['timer_s'] !== 'undefined' ? true : false
                        var hasTimer_e = !isEmptyObject(actionsObjs) && typeof actions['timer_e'] !== 'undefined' ? true : false
                        var hasThreshold = !isEmptyObject(actionsObjs) && typeof actions['threshold'] !== 'undefined' ? true : false
                        tr.push('<tr><td><input type="checkbox" id="timer_s" name="timer_s" value="' + (hasTimer_s ? actions['timer_s'] : '') + '" ' + (hasTimer_s ? 'checked' : '') + '></td><td>開機</td><td colspan="2"><input type="text" id="time_s" class="form-control floating-label" style="height: 23px !important;" placeholder="Time"></td></td>')
                        tr.push('<tr><td><input type="checkbox" id="timer_es" name="timer_e[]" value="' + (hasTimer_e ? actions['timer_e'][0] : '') + '" ' + (hasTimer_e ? 'checked' : '') + '><input type="checkbox" id="timer_ee" name="timer_e[]" style="display: none" value="' + (hasTimer_e ? actions['timer_e'][1] : '') + '" ' + (hasTimer_e ? 'checked' : '') + '></td><td>關機</td><td><input type="text" id="time_es" class="form-control floating-label" style="height: 23px !important;" placeholder="Time"></td><td><input type="text" id="time_ee" class="form-control floating-label" style="height: 23px !important;" placeholder="Time"></td></td>')
                        if (settings.element.equipId == 1) {
                            var selectItems = function () { var N = 17, S = 15; return { value: Array(N).fill().map((e, i) => pad((i + S), 2)), data: Array(N).fill().map((e, i) => i + S) }; }
                            var value = hasThreshold ? parseInt(actions['threshold']) : 0
                            tr.push('<tr><td><input type="checkbox" id="threshold_i" name="threshold" value="' + (hasThreshold ? actions['threshold'] : '') + '" ' + (hasThreshold ? 'checked' : '') + '></td><td>最低設溫限制</td><td colspan="2">' + buildSelectCellByFunction(value, selectItems) + '</td></td>')
                        }
                    } else if (dependAddr != settings.element.address) {
                        tr.push('<tr><td><input type="checkbox" name="depend[]" value="' + dependAddr + '" ' + (settings.depend[settings.element.address].depends.indexOf(dependAddr) >= 0 ? 'checked' : '') + '></td><td colspan="3">' + element.location + settings.map[element.equipId] + '</td></td>')
                    }
                })
                $.each(addressCanNotify, function (index, element) {
                    var notifyAddr = index
                    if (notifyAddr != settings.element.address) {
                        tr.push('<tr><td><input type="checkbox" name="notify[]" value="' + notifyAddr + '" ' + (settings.depend[settings.element.address].notifys.indexOf(notifyAddr) >= 0 ? 'checked' : '') + '></td><td colspan="3">' + element.location + settings.map[element.equipId] + '</td></td>')
                    }
                })
                return [
                    '<div style="height:220px; overflow-y:auto">',
                    '<form method="get" action="{{server}}/EquipDepend">'.replace('{{server}}', settings.server),
                    '<table class="depend-table table table-condensed table-hover">',
                    '<input class="dependControlButton" type="button" style="width:100%; height: 33px; ' + (isLocking ? 'color: red' : '') + '" value="修改狀態">',
                    '<input type="text" style="display:none" name="address" value="{{address}}">'.replace('{{address}}', settings.element.address),
                    '<input type="text" style="display:none" name="company" value="{{company}}">'.replace('{{company}}', settings.company),
                    tr.join(''),
                    '</form>',
                    '</table>',
                    '</div>'
                ].join('');

                function isEmptyObject(obj) {
                    return Object.getOwnPropertyNames(obj).length === 0
                }

                function buildSelectCellByFunction(value, callback) {
                    value = value.toString().split(/ +/)[0]
                    var obj = callback()
                    var data = obj.data
                    var values = obj.value
                    var options = [];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i] == null)
                            continue;
                        options.push('<option value="' + values[i] + '" ' + (value == data[i] ? 'selected="selected"' : '') + '>' + data[i] + '</option>');
                    }
                    return [
                        '<select id="threshold" class="form-control" style="height: 23px !important; padding: 0 8px">',
                        options.join(''),
                        '</select>'
                    ].join('');
                }

                function pad(n, width, z) {
                    z = z || '0';
                    n = n + '';
                    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
                }
            };

            this.update = function (options) {
                settings = $.extend(settings, options);
            };


            var tableElement = this.tableTemplate();
            this.append(tableElement);
            return this;
        }

        $.fn.configContentTable = function config(options) {
            var settings = $.extend({
                className: '',
                title: '',
                data: '',
                element: null,
                mode: '',
                configTitle: [],
                configData: [],
                cellToShow: [],
                cellToRequire: [],
                cellDecode: [],
                decodeContent: {},
                displayOnLineChart: {}
            }, options);

            var _this = this;
            var cellAlertify = ['errorCode']
            var notDefaultToAlertify = { errorCode: { default: 0, alertify: false } }

            this.tableTemplate = function () {
                var EquipShowName;
                $.ajax({
                    url: uploc + '/RequireEquipShowName',
                    dataType: 'json',
                    method: 'GET',
                    async: false,
                    data: {
                        address: settings.element.address
                    },
                    success: function (result) {
                        console.log(result);
                        EquipShowName = result;
                    },
                    error: function (e) {
                        EquipShowName = {
                            accumulate: "",
                            address: "",
                            equipId: "",
                            errorCode: "",
                            customized_cell: [],
                            customized_isDisplay: []
                        };
                    }
                });

                var cellToSend = ['temp', 'cost']
                var tr = [];
                var value = undefined
                for (var cellIndex in settings.cellToShow) {
                    var cell = settings.cellToShow[cellIndex]
                    if (settings.cellToRequire.indexOf(cell) == -1) {
                        if (settings.cellDecode.indexOf(cell) == -1) {
                            value = (settings.configData[cell].type == 'cell' ? settings.configData[cell].data : (typeof settings.configData[cell].select === "function" ? settings.configData[cell].data : settings.configData[cell].select[settings.configData[cell].data]));
                        } else if (settings.cellDecode.indexOf(cell) > -1) {
                            value = (settings.configData[cell].type == 'cell' ? settings.decodeContent[cell](settings.configData[cell].data) : (typeof settings.configData[cell].select === "function" ? settings.configData[cell].data : settings.configData[cell].select[settings.configData[cell].data]));
                        }
                        value = (typeof value === "undefined" ? '未定義狀態' : value)
                        if (EquipShowName.customized_isDisplay[cellIndex] == "1" || typeof EquipShowName.customized_isDisplay[cellIndex] === "undefined") tr.push('<tr><td class="from" title="' + cell + '">' + settings.configTitle[cell] + '</td><td class="' + cell + '">' + value + '</td></td>');
                    } else if (settings.cellToRequire.indexOf(cell) > -1) {
                        value = settings.configData[cell].default
                        value = (typeof value === "undefined" ? '未定義狀態' : value)
                        if (EquipShowName.customized_isDisplay[cellIndex] == "1" || typeof EquipShowName.customized_isDisplay[cellIndex] === "undefined") tr.push('<tr><td class="from" title="' + cell + '">' + settings.configTitle[cell] + '</td><td class="' + cell + '" id="' + settings.configData[cell].to + '">' + value + '</td></td>');
                    }
                }
                return [
                    '<div style="height:220px; overflow-y:auto">',
                    (settings.mode == 'list' ? '<input class="editButton" type="button" style="width:100%; height: 33px;" address="' + settings.element.address + '" value="設備管理">' : ''),
                    '<table class="config-table table table-condensed table-hover">',
                    tr.join(''),
                    '</table>',
                    '</div>'
                ].join('');

            };

            this.update = function (options) {
                settings = $.extend(settings, options);
                var value = undefined
                for (var cellIndex in settings.cellToShow) {
                    var cell = settings.cellToShow[cellIndex]
                    if (settings.cellToRequire.indexOf(cell) == -1) {
                        if (settings.cellToShow.indexOf(cell) > -1) {
                            if (settings.cellDecode.indexOf(cell) == -1) {
                                value = (settings.configData[cell].type == 'cell' ? settings.configData[cell].data : (typeof settings.configData[cell].select === "function" ? settings.configData[cell].data : settings.configData[cell].select[settings.configData[cell].data]));
                            } else if (settings.cellDecode.indexOf(cell) > -1) {
                                value = (settings.configData[cell].type == 'cell' ? settings.decodeContent[cell](settings.configData[cell].data) : (typeof settings.configData[cell].select === "function" ? settings.configData[cell].data : settings.configData[cell].select[settings.configData[cell].data]));
                            }
                            value = (typeof value === "undefined" ? '未定義狀態' : value)
                            $('\.' + settings.className).find('\.' + cell).html(value);
                        }
                    }

                    this.alertifyCheck(cell)
                }
            };

            this.currentIsClickable = function (clickColumn, target) {
                if (typeof target === 'undefined')
                    return false

                var type = target.currentDisplayType()
                var title = clickColumn.find('.from').attr('title')
                if ((Object.keys(settings.displayOnLineChart).indexOf(type) > -1 && settings.displayOnLineChart[type].indexOf(title) > -1) ||
                    (Object.keys(settings.displayOnLineChart).indexOf(type) == -1 && settings.displayOnLineChart['default'].indexOf(title) > -1))
                    return true
                return false
            }

            this.clickCallbackToLineChart = function (e, target) {
                if (typeof target !== 'undefined') {
                    var displayType = target.currentDisplayType()
                    target.attr('data-depend', $(e.currentTarget).find('.from').attr('title'))
                    target.reset()
                }
            }

            this.changeDisplayType = function (type) {
                var column = $(_this).find('.breath-light')
                var title = column.find('.from').attr('title')
                if (Object.keys(settings.displayOnLineChart).indexOf(type) > -1 && settings.displayOnLineChart[type].indexOf(title) == -1) {
                    column.removeClass('breath-light')
                    var defaultColumn = $(_this).find('.from[title=' + settings.displayOnLineChart[type][0] + ']').parent()
                    defaultColumn.click()
                }
            }

            this.alertifyCheck = function (cell) {
                if (cellAlertify.indexOf(cell) > -1 && !notDefaultToAlertify[cell].alertify && notDefaultToAlertify[cell].default != parseInt(settings.configData[cell].data)) {
                    notDefaultToAlertify[cell].alertify = true
                    alert(settings.decodeContent[cell](settings.configData[cell].data))
                    alertify.log(settings.decodeContent[cell](settings.configData[cell].data), "error", 0)
                }
            }

            var tableElement = this.tableTemplate();
            this.append(tableElement);
            return this;
        }

        $.fn.configInfoTable = function config(options) {
            var settings = $.extend({
                className: '',
                adderss: '',
                location: '',
                c_floor: '',
                floor: '',
                place: '',
                equipId: '',
                companyID: '',
                equip: ''
            }, options);

            var _this = this;

            this.tableTemplate = function () {
                return [
                    '<div>',
                    '<input class="viewButton" id="viewButton_' + settings.address + '" type="button" style="width:50%; height: 33px;" address="' + settings.address + '" value="觀看設備">',
                    '<input class="editButton" type="button" style="width:50%; height: 33px;" address="' + settings.address + '" value="修改設備">',
                    '<table class="config-table table table-condensed table-hover" style="margin: 0">',
                    '<tr><td>網卡</td><td style="color:green;" class="device_info" title="address" data="' + settings.address + '">' + settings.address + '</td></tr>',
                    '<tr><td>設備</td><td style="color:green;" class="device_info" title="equipId" data="' + settings.equipId + '">' + settings.equip[settings.equipId] + '</td></tr>',
                    '<tr><td>地點</td><td style="color:green;" class="device_info" title="place" data="' + settings.place + '">' + settings.place + '</td></tr>',
                    '<tr><td>樓層</td><td style="color:green;" class="device_info" title="floor" data="' + settings.floor + '">' + settings.c_floor + '</td></tr>',
                    '<tr><td>位置</td><td style="color:green;" class="device_info" title="location" data="' + settings.location + '">' + settings.location + '</td></tr>',
                    '<tr><td>公司</td><td style="color:green;" class="device_info" title="companyID" data="' + settings.companyID + '">' + settings.companyID + '</td></tr>',
                    '<tr><td>接收率</td><td><div class="notice_wrap"><a id="accumulate_' + settings.address + '" style="color:red;" class="device_info" title="accumulate" href="#" data="' + settings.address + '">統計中</a><!--div class="notice">點擊觀看</div--></div></td></tr>',
                    '</table>',
                    '</div>'
                ].join('');
            };

            var tableElement = this.tableTemplate();
            this.append(tableElement);
            return this;
        }

        $.fn.configRegisterTable = function config(options) {
            var settings = $.extend({
                className: '',
                mode: '',
                email: '',
                equip: [],
                server: ''
            }, options);

            var _this = this;
            var isRegister = 'register'.indexOf(settings.mode) > -1 ? true : false;

            this.tableTemplate = function () {
                return [
                    '<div>',
                    '<form method="post" action="{{server}}/deviceRegister">'.replace('{{server}}', settings.server),
                    '<table class="config-table table table-condensed table-hover" style="margin: 0">',
                    '<input type="input" style="display: none" name="email" value="{{email}}">'.replace('{{email}}', settings.email),
                    '<tr><td>網卡</td><td><input type="input" style="width: 100%" ' + (isRegister ? '' : 'value="' + localStorage['__device__address'] + '"') + ' name="address" placeholder="ex: 000000000123456A">' + (isRegister ? '' : '<input type="input" style="display:none" value="' + localStorage['__device__address'] + '" name="o_address">') + '</td></tr>',
                    '<tr><td>設備</td><td>' + this.buildSelectCell() + '</td></tr>',
                    '<tr><td>地點</td><td><input type="input" style="width: 100%" ' + (isRegister ? '' : 'value="' + localStorage['__device__place'] + '"') + ' name="place" placeholder="ex: 台北市"></td></tr>',
                    '<tr><td>樓層</td><td><input type="number" style="width: 100%" ' + (isRegister ? '' : 'value="' + localStorage['__device__floor'] + '"') + ' name="floor" placeholder="ex: 1"></td></tr>',
                    '<tr><td>位置</td><td><input type="input" style="width: 100%" ' + (isRegister ? '' : 'value="' + localStorage['__device__location'] + '"') + ' name="location" placeholder="ex: 辦公室"></td></tr>',
                    '<tr><td>公司</td><td><input type="number" min="0" style="width: 100%" ' + (isRegister ? '' : 'value="' + localStorage['__device__companyID'] + '"') + ' name="companyID" placeholder="ex: 12345678"></td></tr>',
                    '</table>',
                    '<input class="submitButton" type="button" style="width:100%; height: 33px;" value="送出">',
                    '</form>',
                    '</div>'
                ].join('');
            };

            this.buildSelectCell = function () {
                var options = [];
                for (var i in settings.equip) {
                    options.push('<option value="' + i + '" ' + (isRegister ? '' : (i == localStorage['__device__equipId'] ? 'selected="selected"' : '')) + '>' + settings.equip[i] + '</option>');
                }
                return [
                    '<select name="equipId" style="width: 100%; height: 28px;">',
                    options.join(''),
                    '</select>'
                ].join('');
            }

            var tableElement = this.tableTemplate();
            this.append(tableElement);
            return this;
        }

        $.fn.textWidth = function (text, font) {
            if (!$.fn.textWidth.fakeEl) $.fn.textWidth.fakeEl = $('<span>').hide().appendTo(document.body);
            $.fn.textWidth.fakeEl.text(text || this.val() || this.text()).css('font', font || this.css('font'));
            return $.fn.textWidth.fakeEl.width();
        };
    }

    function getMockData() {
        return {
            date: '2014-12-01',
            sentTotal: 4120,
            delivered: 3708,
            opened: 3090,
            clicked: 2060,
            conversion: 35000,
            conversionEmails: 100

        };
    }

}));
