$(function () {
    if(typeof localStorage['__user__email'] == "undefined" || typeof localStorage['__user__place'] == "undefined" ||
        localStorage['__user__email'].length == 0 || localStorage['__user__place'].length == 0) {
        alert('系統尚未登入，即將導至登入頁面。')
        location = 'SystemLogin.html'
    }

    /*$(".rad-sidebar").removeClass("rad-nav-min")
    $(".rad-body-wrapper").removeClass("rad-nav-min");*/

    $(document).on("click", function (e) {
        var $item = $(".rad-dropmenu-item");
        if ($item.hasClass("active")) {
            $item.removeClass("active");
        }
        $item = $(".rad-sidebar");
        if (!$(e.target).hasClass("rad-toggle-btn") && !$item.hasClass("rad-nav-min")) {
            $(".rad-sidebar").addClass("rad-nav-min");
            $(".rad-body-wrapper").addClass("rad-nav-min");
        }
        if (typeof $status !== 'undefined')
            setTimeout(function() { $status.refreshLineChart() }, 200);
    });

    $(".rad-toggle-btn, .rad-toggle-btn > i").on('click', function (e) {
        e.stopPropagation();
        $(".rad-sidebar").toggleClass("rad-nav-min");
        $(".rad-body-wrapper").toggleClass("rad-nav-min");
        if (typeof $status !== 'undefined')
            setTimeout(function() { $status.refreshLineChart() }, 200);
    });

    $(".rad-dropdown >.rad-menu-item").on('click', function (e) {
        e.stopPropagation();
        $(".rad-dropmenu-item").removeClass("active");
        $(this).next(".rad-dropmenu-item").toggleClass("active");
    });

    $(".rad-top-nav-container .logout").on('click', function(e) {
        e.stopPropagation();
        localStorage['__user__email'] = '';
        localStorage['__user__place'] = '';
        location = 'SystemLogin.html'
    });
})