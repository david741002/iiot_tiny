var debug;
var $storage = {};

function activateReportOverviewModule() {
    var email = localStorage['__user__email'];
    var place = localStorage['__user__place'];
    var company = localStorage['__user__company'];

    'use strict';
    $.ajax({
        //url: uploc + '/RequireEquipInformation?owner='+email,
        url: uploc + '/RequireDeviceAddress?companyID='+company,
        dataType: 'json',
        method: 'GET',
        success: function (result) {
            var $address = result.address;
            var $map = result.map;
            var $depends = result.depends;
            var $page = location.href.split('?').length == 1 ? 'all' : location.href.split('?')[1].split('&')[0].split('=')[1]
            var $mode = null;
            var action = location.href.split('?').length == 1 || location.href.split('?')[1].split('&')[0].split('=')[0];
            switch(true) {
                case action == 'equipment':
                    $mode = 'config';
                break;
                case action == 'floor':
                    $mode = 'list';
                break;
                case $page == 'register':
                    $mode = 'register';
                break;
                case $page == 'history':
                    $mode = 'history';
                break;
                case $page == 'modify':
                    $mode = 'modify';
                break;
                case $page == 'information':
                    $mode = 'information';
                break;
                default:
                    $mode = 'list';
                break;
            }
            $.ajax({
                url: uploc + '/RequireEquip',
                dataType: 'json',
                method: 'GET',
                success: function (result) {
                    var $array = result;
                    var $el = $('.container-fluid');
                    $array.sort(SortByAddr)
                    $status = new ReportOverviewModule({
                        element: $el,
                        data: $array,
                        address: $address,
                        map: $map,
                        depends: $depends,
                        page: $page,
                        mode: $mode,
                        email: email,
                        place: place,
                        server: uploc
                    });

                    if(['config', 'list'].indexOf($mode) > -1)
                        setInterval(function () { updateChart() }, 2000);
                }
            });
        }
    });
};

function updateChart() {
    if($('.report-statistic-box').toArray().length > 0) {
        $.ajax({
            url: uploc + '/RequireEquip',
            dataType: 'json',
            method: 'GET',
            success: function (result) {
                var $data = result;
                $status.updatePieChart($data);
                $status.updateContentTable($data);
            }
        });
    }

    $(".report-linechart-box").toArray().forEach(function (element) {
        var className = $(element).attr('name');
        var timestamp = $status.getLineChartTime(className);
        var address = $status.getAddress(className);
        $.ajax({
            url: uploc + '/RequireHistoryEquip',
            dataType: 'json',
            method: 'GET',
            data: {
                address: address,
                time: timestamp
            },
            success: function (result) {
                var $data = result;
                $status.updateLineChart($data);
            }
        });
    }, this);
}

function SortByAddr(a, b){
    var aAddr = a.address.toLowerCase();
    var bAddr = b.address.toLowerCase(); 
    return ((aAddr < bAddr) ? -1 : ((aAddr > bAddr) ? 1 : 0));
}
