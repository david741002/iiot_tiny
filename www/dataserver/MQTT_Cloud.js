var config = require('./config')
var mqtt = require('mqtt');
var ObjectID = require('mongodb').ObjectID
var mongo = require('./db/mongoDB')
var mongod = require('./db/mongoDeviceDB')
var parser = require('./parser_Cloud')
var event = require('./js/event')
var debug = require('./js/debug')

var Random = Math.floor(Math.random() * 1024) + 1  
var options = {
    port: config.CLOUD_MQTT_PORT,
    host: config.CLOUD_MQTT_SERVER,
    clientId: 'Cloud_'+Random,
    username: 'Cloud_'+Random,
    password: 'Cloud_'+Random,
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
};

var DeviceSystemStatus = false;
mongo.init(27017, config.CLOUD_DATABASE, (OpenStatus) => { //DB init
    if (OpenStatus) {
        console.log('\x1b[32m mongodb open success \x1b[37m');
        
        var client = mqtt.connect(options);
        RegisterMongoDeviceEvent();
        RegisterAddrEvent();
        RegisterDataEvent();
        RegisterPortEvent()

        client.subscribe('#');
        client.on('message', function (topic, message) {
            if(!DeviceSystemStatus)
                return;

            if (!IsJsonString(message))
                return;

            var jsonData = JSON.parse(message);
            if (typeof jsonData === undefined)
                return;
            
            switch(true) {
                case topic == config.CLOUD_MQTT_UP_TOPIC:
                    event.emit('CheckMacAddr', jsonData);
                break;
                case topic == config.CLOUD_MQTT_PORT_TOPIC:
                    event.emit('CheckPortEvent', jsonData)
                break;
            }
        });

    } else {
        console.log('\x1b[31m mongodb open fail \x1b[37m');
        process.exit();
    }
});

function RegisterMongoDeviceEvent() {
    event.on('DeviceSystemReady', () => {
        DeviceSystemStatus = true
    })

    DeviceSystemStatus = false
    mongod.init(27017, config.CLOUD_DEVICE_DATABASE, (result) => {
        if (result) {
            event.emit('DeviceSystemReady')
            console.log('\x1b[32m mongodb device open success \x1b[37m');
        }
    })
}

function RegisterAddrEvent() {
    event.on('CheckMacAddr', function (jsonData) {
        if (!(typeof jsonData === undefined)) {
            mongo.queryFindOne('device_address', { macAddress: jsonData.address }, function (result, message) {
                if (result && message) {
                    event.emit('CheckDataFormat', jsonData);
                } else {
                    //console.log('\x1b[31m Error: mqtt receive unregister mac address '+ jsonData.address+'\x1b[37m');
                    debug.log('log/MQTT.log', 'Error: mqtt receive unregister mac address (' + jsonData.address + ') data (' + jsonData.data + ')');
                }
            })
        }
    })
}

function RegisterDataEvent() {
    event.on('CheckDataFormat', function (jsonData) {
        parser.parse(jsonData, function (result, data) {
            if (result && CheckIsValidString(data.parse.address)) {
                event.emit('CorrectDataFormat', data);
            } else {
                //console.log('\x1b[31m Error: Parse equipment package error ' + jsonData.address + ') data (' + jsonData.data + ') \x1b[37m');
                debug.log('log/Parse.log', 'Error: Parse equipment package error ' + jsonData.address + ') data (' + jsonData.data + ')');
            }
        })
    })

    event.on('CorrectDataFormat', function (data) {
        mongo.queryFindAll('system_status', {address: data.parse.address}, function (result, message) {
            // mongo.insert('history_status', message, function (result, history_message) { });
            // 每天歷史資料
            // mongo.insert('history_status_1', message, function (result, history_message) { });
            // 分散儲存
            var date = new Date(data.parse.timestamp)
            storage = 'history_status_' + CurrentTimestamp(date) + '_' + data.parse.address
            mongod.insert(storage, message, function(result, history_message) { })

            mongo.remove('system_status', { address: data.parse.address }, function (result, message) {
                if (result) {
                    data.parse.timestamp = new Date(data.parse.timestamp);
                    mongo.insert('system_status', data.parse, function (result, message) { })
                } else {
                    //console.log('\x1b[31m Remove System Status Error \x1b[37m');
                }
            })
        })
    })

    function CurrentTimestamp(time) {
        return time.getFullYear()+"_"+pad(time.getUTCMonth()+1, 2)

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
    }
}

function RegisterPortEvent() {
    event.on('CheckPortEvent', function(json) {
        switch(true) {
            case json.type == 'statistic':
                event.emit('UpdateStatistic', json.data)
            break;
            default:
                console.log('\x1b[31m Get Unregister Port Event ('+ json.type +') \x1b[37m');
            break;
        }
    })

    event.on('UpdateStatistic', function(datas) {
        for(var index in datas) {
            var data = FormatStatistic(datas[index])
            if (data.length > 0) {
                event.emit('DoUpdateStatistic', datas[index].address, datas[index].date, data)
            }
        }
    })

    event.on('DoUpdateStatistic', function(address, date, data) {
        mongo.remove('daily_status', { address: address, date: date }, function (result, message) {
            if (result) {
                mongo.insert('daily_status', data, function (result, message) { })
            } else {
                console.log('\x1b[31m Update Statistic Error ('+ data +') \x1b[37m');
                setTimeout(() => { 
                    event.emit('DoUpdateStatistic', address, date, data)}, 10000)
            }
        })
    })
}

function FormatStatistic(data) {
    var datas = []
    switch(true) {
        case data.equipId.toString() == '1':
        var set_temp = ParseData(data.data, 'set_temp')
        var temp = ParseMaxMin(data.data, 'temp')
        var cost = ParseData(data.data, 'acc_cost')
        var filterHours = ParseFloat(data.data, 'filterHours', 200)
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, wind_dir: 0, set_temp: set_temp[0], temp: temp[0], cost: cost[0], errorCode: "00", remain: "ffffffff", filterHours: filterHours[0], tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, wind_dir: 0, set_temp: set_temp[1], temp: temp[1], cost: cost[1], errorCode: "00", remain: "ffffffff", filterHours: filterHours[1], tempType: 1}]
        break;
        case data.equipId.toString() == '2':
        var cost = ParseData(data.data, 'acc_cost')
        var filterHours = ParseFloat(data.data, 'filterHours', 200)
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, cost: cost[0], remain7 : "0", remain89 : "00", remain1011 : "00", errorCode : "00", remain : "00000000", filterHours: filterHours[0], tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, cost: cost[1], remain7 : "0", remain89 : "00", remain1011 : "00", errorCode : "00", remain : "00000000", filterHours: filterHours[1], tempType: 1}]
        break;
        case data.equipId.toString() == '3':
        var HCHO = ParseMaxMin(data.data, 'HCHO')
        var CO2 = ParseMaxMin(data.data, 'CO2')
        var CO = ParseMaxMin(data.data, 'CO')
        var PM10 = ParseMaxMin(data.data, 'PM10')
        var TVOC = ParseMaxMin(data.data, 'TVOC')
        var AVG_HCHO = ParseAvg(data.data, 'HCHO')
        var AVG_CO2 = ParseAvg(data.data, 'CO2')
        var AVG_CO = ParseAvg(data.data, 'CO')
        var AVG_PM10 = ParseAvg(data.data, 'PM10')
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, HCHO: HCHO[0], CO2: CO2[0], CO: CO[0], PM10: PM10[0], TVOC: TVOC[0], AVG_HCHO: AVG_HCHO[0], AVG_CO2: AVG_CO2[0], AVG_CO: AVG_CO[0], AVG_PM10: AVG_PM10[0], tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, HCHO: HCHO[1], CO2: CO2[1], CO: CO[1], PM10: PM10[1], TVOC: TVOC[1], AVG_HCHO: AVG_HCHO[1], AVG_CO2: AVG_CO2[1], AVG_CO: AVG_CO[1], AVG_PM10: AVG_PM10[1], tempType: 1}]
        break;
        case data.equipId.toString() == '4':
        var temp = ParseMaxMin(data.data, 'temp')
        var maxTemp = ParseMaxMin(data.data, 'maxTemp')
        var minTemp = ParseMaxMin(data.data, 'minTemp')
        var diffTemp = ParseMaxMin(data.data, 'diffTemp')
        var irCO2 = ParseMaxMin(data.data, 'irCO2')
        var irTVOC = ParseMaxMin(data.data, 'irTVOC')
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, temp: temp[0], maxTemp: maxTemp[0], minTemp: minTemp[0], diffTemp: diffTemp[0], irCO2: irCO2[0], irTVOC: irTVOC[0], other: "fffff", tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, temp: temp[1], maxTemp: maxTemp[1], minTemp: minTemp[1], diffTemp: diffTemp[1], irCO2: irCO2[1], irTVOC: irTVOC[1], other: "fffff", tempType: 1}]
        break;
        case data.equipId.toString() == '5':
        var temp = ParseMaxMin(data.data, 'temp')
        var cost = ParseData(data.data, 'acc_cost')
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, cost: cost[0], temp: temp[0], remain9 : "f", remain1011 : "ff", errorCode: "ff", remain: "ffffffff", tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, wind: 0, cost: cost[1], temp: temp[1], remain9 : "f", remain1011 : "ff", errorCode: "ff", remain: "ffffffff", tempType: 1}]
        break;
        case data.equipId.toString() == 'a':
        var temp = ParseMaxMin(data.data, 'temp')
        var humidity = ParseMaxMin(data.data, 'humidity')
        var produce = ParseData(data.data, 'max_produce')
        datas = [   {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, error: 0, temp: temp[0], humidity: humidity[0], produce: produce[0], remain: "fffffffff", tempType: 0}, 
                    {address: data.address, equipId: data.equipId.toString(), date: data.date, accumulate: data.accumulate, timestamp: new Date(data.timestamp), opCode: 0, power: 0, status: 0, error: 0, temp: temp[1], humidity: humidity[1], produce: produce[1], remain: "fffffffff", tempType: 1}]
        break;
    }
    return datas;
}

function ParseAvg(data, label) {
    if (typeof data === 'undefined')
        return {0: [0, 1], 1: [0, 1]}
    var array = data.hasOwnProperty('avg_'+label) ? data['avg_'+label] : [0, 1]
    return {0: array, 1: array}
}

function ParseMaxMin(data, label, base = 16) {
    if (typeof data === 'undefined')
        return {0: 0, 1: 0}
    var min = data.hasOwnProperty('min_'+label) ? data['min_'+label].toString(base) : 0
    var max = data.hasOwnProperty('max_'+label) ? data['max_'+label].toString(base) : 0
    return {0: min, 1: max}
}

function ParseData(data, label, base = 16) {
    if (typeof data === 'undefined')
        return {0: 0, 1: 0}
    var data = data.hasOwnProperty(label) ? data[label].toString(base) : 0
    return {0: data, 1: data}
}

function ParseFloat(data, label, defaultValue=0) {
    if (typeof data === 'undefined')
        return {0: defaultValue, 1: defaultValue}
    var data = data.hasOwnProperty(label) ? parseFloat(data[label]) : defaultValue
    return {0: data, 1: data}
}

function CheckIsValidString(string) {
    if (typeof string === 'undefined' || string == 0) {
        return false
    }
    return true
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
