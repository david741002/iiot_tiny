## Install NodeJS
+ sudo apt-get update
+ sudo apt-get install build-essential
+ wget https://github.com/nodejs/node/archive/v6.10.3.zip
+ unzip v6.10.3
+ ./configure
+ make -j4
+ make test
+ make doc
+ ./node -e "console.log('Hello from Node.js ' + process.version)"
+ make install

## Install MongoDB (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
+ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
+ Ubuntu14
	- echo "deb [ arch=amd64 ] http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
+ Ubuntu16
	- echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
+ sudo apt-get update
+ sudo apt install -y mongodb-org
+ if mongod service is start
	- sudo service mongod stop

## Install mongoc driver
+ wget https://github.com/mongodb/mongo-c-driver/releases/download/1.6.2/mongo-c-driver-1.6.2.tar.gz
+ tar xzf mongo-c-driver-1.6.2.tar.gz
+ ./configure --disable-automatic-init-and-cleanup
+ make -j4
+ sudo make install

## Install mongocxx driver
+ wget https://github.com/mongodb/mongo-cxx-driver/archive/r3.1.1.zip
+ unzip r3.1.1
+ cd to build directory in the r3.1.1
+ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ..
+ sudo make EP_mnmlstc_core
+ make -j4
+ sudo make install

## Install Qt Env
+ sudo apt install qt5-default qtcreator 

## For Serial Port on Ubuntu
# non-root user must has permission on dialout group
+ sudo adduser second_user dialout
+ sudo reboot
+ sudo chmod a+rw /dev/ttyUSB0
