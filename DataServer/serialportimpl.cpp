#include "serialportimpl.h"
#include <iostream>
#include <stdio.h>      // standard input / output functions
#include <stdlib.h>
#include <string.h>     // string function definitions
#include <unistd.h>     // UNIX standard function definitions
#include <fcntl.h>      // File control definitions
#include <errno.h>      // Error number definitions
#include <termios.h>    // POSIX terminal control definitions
#include <QTimer>
#include <QDebug>

#define SERIAL_PORT "/dev/ttyUSB0"
#define END '\r'

SerialPortImpl::SerialPortImpl(QObject *parent) :
    QThread(parent)
{
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(initializeReTimer()));

    cout << "Thread : " << this->currentThreadId() << endl;
    isOpened = initialize();

    if(isOpened == false) {
        timer->setSingleShot(true);
        qDebug() << "Timer Start";
        timer->start(1000);
    }
}

SerialPortImpl::~SerialPortImpl()
{
    if(USB >= 0) {
        close(USB);
    }

    if(timer->isActive()) {
        timer->stop();
        timer->destroyed();
    }
}

void SerialPortImpl::run() {
    while(1) {
        //cout << "Run" << endl;
        string result;
        receive(result);
        QString qresult = QString::fromStdString(result);
        if(qresult.length() > 0)
            emit messageReceiveAfter(qresult);
        //cout << "Run End" << endl;
    }
}

bool SerialPortImpl::initialize()
{
    USB = -1;
    USB = open( SERIAL_PORT, O_RDWR | O_NOCTTY );


    struct termios tty;
    struct termios tty_old;
    memset (&tty, 0, sizeof tty);

    /* Error Handling */
    if ( tcgetattr ( USB, &tty ) != 0 ) {
       std::cout << "Error(1) " << errno << " from tcgetattr: " << strerror(errno) << std::endl;
       return false;
    }

    /* Save old tty parameters */
    tty_old = tty;

    /* Set Baud Rate */
    cfsetospeed (&tty, (speed_t)B9600);
    cfsetispeed (&tty, (speed_t)B9600);

    /* Setting other Port Stuff */
    tty.c_cflag     &=  ~PARENB;            // Make 8n1
    tty.c_cflag     &=  ~CSTOPB;
    tty.c_cflag     &=  ~CSIZE;
    tty.c_cflag     |=  CS8;

    tty.c_cflag     &=  ~CRTSCTS;           // no flow control
    tty.c_cc[VMIN]   =  1;                  // read doesn't block
    tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
    tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

    /* Make raw */
    cfmakeraw(&tty);

    /* Flush Port, then applies attributes */
    tcflush( USB, TCIFLUSH );
    if ( tcsetattr ( USB, TCSANOW, &tty ) != 0) {
       std::cout << "Error(2) " << errno << " from tcsetattr" << std::endl;
       return false;
    }
    return true;
}

void SerialPortImpl::initializeReTimer() {
    isOpened = initialize();
    if(isOpened == false) {
        timer->setSingleShot(true);
        timer->start(3000);
    }
}

void SerialPortImpl::receive(string &result)
{
    result = "";
    int n = 0,
        spot = 0;
    char buf = '\0';

    if(isOpened == false) {
        return;
    }

    /* Whole response*/
    char response[1024];
    memset(response, '\0', sizeof response);

    do {
        n = read( USB, &buf, 1 );
        if(buf != END) {
            sprintf( &response[spot], "%c", buf );
            spot += n;
        }
    } while( buf != END && n > 0);

    if (n < 0) {
        std::cout << "Error reading: " << strerror(errno) << std::endl;
    }
    else if (n == 0) {
        std::cout << "Read nothing!" << std::endl;
    }
    else {
        std::cout << "Response: " << response << std::endl;
        result = response;
    }
}

void SerialPortImpl::send(string data)
{
    cout << "Send: " << data << endl;
    QString qdata = QString::fromStdString(data);
    emit messageSendBefore(qdata);

    data.push_back(END);
    int n_written = 0,
        spot = 0;

    do {
        n_written = write( USB, &data[spot], 1 );
        spot += n_written;
    } while (data[spot-1] != END && n_written > 0);
}
