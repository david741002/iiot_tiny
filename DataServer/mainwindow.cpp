#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QtDebug>
#include <QTimer>
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::concatenate;

//#define MQTT_SERVER "140.124.182.58"
#define MQTT_SERVER "localhost"
#define MQTT_PORT 1883
#define MQTT_SUBSCRIBE "456"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // system init
    init();
    init_statusmanager();

    // equip thread start
    init_serialport();
    if(serialportImpl) {
        serialportImpl->start();
    }

    if (!createConnection()) {
        qDebug() << "Not connected!";
    }
    else{
        qDebug() << "Connected!";

        init_mqttclient();
        init_timer();

        mqttClient->connect();

        // message refresh immediately
        systemStatusManager->fetchSystemStatus();

        // timer start
        // systemUpdateTimer->start(1000);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
    // systemUpdateTimer->stop();
    mqttClient->disconnect();

    delete systemStatusManager;
    delete mqttClient;
}


bool MainWindow::createConnection()
{
    conn = mongocxx::client{mongocxx::uri{}};

    bool ssmConnStatus = false;
    ssmConnStatus = systemStatusManager->createConnection(conn);

    return ssmConnStatus;
}

void MainWindow::init()
{
    equipAirCond.registerId(1);
    equipBlower.registerId(2);
}

void MainWindow::init_serialport()
{
    serialportImpl = NULL;
    serialportImpl = new SerialPortImpl(this);
    connect(serialportImpl, SIGNAL(messageReceiveAfter(QString)), this, SLOT(handleMessageReceiveAfter(QString)));
    connect(serialportImpl, SIGNAL(messageSendBefore(QString)), this, SLOT(handleMessageSendBefore(QString)));
    // 线程结束后，自动销毁
    connect(serialportImpl, SIGNAL(finished()), serialportImpl, SLOT(deleteLater()));
}

void MainWindow::init_statusmanager() {
    systemStatusManager = NULL;
    systemStatusManager = new SystemStatusManager();

    connect(systemStatusManager, SIGNAL(fetchSystemStatusAfter()), this, SLOT(updateSystemStatus()));
}

void MainWindow::init_mqttclient()
{
    mqttClient = NULL;
    mqttClient = new MqttClient(MQTT_SERVER, MQTT_PORT);
    mqttClient->setTopic(MQTT_SUBSCRIBE);
    mqttClient->setQos("0");

    QObject::connect(mqttClient, &MqttClient::connected, mqttClient, &MqttClient::showConnected);
    QObject::connect(mqttClient, &MqttClient::connected, mqttClient, &MqttClient::subscribeTo);
    QObject::connect(mqttClient, &MqttClient::disconnected, mqttClient, &MqttClient::showDisConnected);
    QObject::connect(mqttClient, &MqttClient::subscribed, mqttClient, &MqttClient::showSubscribed);
    QObject::connect(mqttClient, &MqttClient::received, this, &MainWindow::handleMqttMessageReceive);
}

void MainWindow::init_timer()
{
//    systemUpdateTimer = new QTimer(this);

//    connect(systemUpdateTimer, SIGNAL(timeout()), this, SLOT(checkSystemStatus()));
}

void MainWindow::on_MQTTSend_clicked()
{
    QString mqttmessage = ui->MQTTInput->text();
    QString topic = ui->MQTTTopic->text();
    if(mqttmessage.length() < 1)
        qDebug() << "Mqtt message length must > 0";
    if(topic.length() < 1)
        qDebug() << "Mqtt topic length must > 0";

    Message message(qrand(), topic, mqttmessage.toUtf8());
    mqttClient->publish(message);
}

void MainWindow::on_load_clicked()
{
    vector<Equipment> historyStatus = systemStatusManager->fecthSystemHistoryStatus();
    QString message("");
    for(unsigned int i = 0; i < historyStatus.size(); i++) {
        message = historyStatus.at(i).c_timestamp() + " - (" + historyStatus.at(i).c_equipId() + ") " + historyStatus.at(i).c_temp() + "\n" + message;
    }
    ui->message->setPlainText(message);
}

void MainWindow::handleMqttMessageReceive(const QMQTT::Message &message)
{
    QString data(message.payload());
    cout << "received data: " << data.toStdString() << endl;
    systemStatusManager->fetchSystemStatus();
}

void MainWindow::updateSystemStatus() {
    std::cout << "Update System Status" << std::endl;
    if(systemStatusManager->refreshEquipmentStatus(equipAirCond)) {
        qDebug() << "Air Cond Update";
        ui->power_1->setText(equipAirCond.c_power());
        ui->status_1->setText(equipAirCond.c_status());
        ui->wind_1->setText(equipAirCond.c_wind());
        ui->wind_dir_1->setText(equipAirCond.c_wind_dir());
        ui->set_temp_1->setText(equipAirCond.c_set_temp());
        ui->temp_1->setText(equipAirCond.c_temp());
        ui->cost_1->setText(equipAirCond.c_cost());
    }

    if(systemStatusManager->refreshEquipmentStatus(equipBlower)) {
        qDebug() << "Blower Update";
        ui->power_2->setText(equipBlower.c_power());
        ui->status_2->setText(equipBlower.c_status());
        ui->wind_2->setText(equipBlower.c_wind());
        ui->wind_dir_2->setText(equipBlower.c_wind_dir());
        ui->set_temp_2->setText(equipBlower.c_set_temp());
        ui->temp_2->setText(equipBlower.c_temp());
        ui->cost_2->setText(equipBlower.c_cost());
    }
}

/**
    RS232 Serial Port handling Function
*/
void MainWindow::on_RS232Send_clicked()
{
    string input = ui->RS232Input->text().toStdString();
    ui->RS232Input->setText("");
    if(serialportImpl) {
        serialportImpl->send(input);
    }
}

void MainWindow::handleMessageReceiveAfter(QString result)
{
    if(result.length() == 0)
        return;

    QString message = ui->RS232Messages->toPlainText();
    message = "RS232 Receive: " + result + '\n' + message;
    cout << "Receive : "<< result.toStdString() << "(" << result.toStdString().length() << ")" << endl;
    ui->RS232Messages->setPlainText(message);
}

void MainWindow::handleMessageSendBefore(QString signal)
{
    QString message = ui->RS232Messages->toPlainText();
    message = "RS232 Send: " + signal + '\n' + message;
    ui->RS232Messages->setPlainText(message);
}
