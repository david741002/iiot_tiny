#ifndef EQUIPMENT
#define EQUIPMENT

#include <QString>
#include <QDebug>

using namespace std;

/**
    For Demo Use
    Variables all to declared as public
*/

class Equipment {
public:
    Equipment() {
        __id = set_temp = temp = cost = errorCode = remain = timestamp = '0';
        equipId = opCode = power = status = wind = wind_dir = -1;
    }
    Equipment(QString __id, int equipId, int opCode, int power, int status, int wind, int wind_dir, QString set_temp, QString temp, QString cost, QString errorCode, QString remain, QString timestamp) :
        __id(__id), equipId(equipId), opCode(opCode), power(power), status(status), wind(wind), wind_dir(wind_dir), set_temp(set_temp), temp(temp), cost(cost), errorCode(errorCode), remain(remain), timestamp(timestamp) { }

    QString __id;
    int equipId;
    int opCode;
    int power;
    int status;
    int wind;
    int wind_dir;
    QString set_temp;
    QString temp;
    QString cost;
    QString errorCode;
    QString remain;
    QString timestamp;

    void registerId(int __id) { equipId = __id; }
    QString id() { return QString::number(equipId); }
    QString c_equipId() { return QString::number(equipId); }
    QString c_power() { return power2c(power); }
    QString c_status() { return status2c(status); }
    QString c_wind() { return wind2c(wind); }
    QString c_wind_dir() { return wind_dir2c(wind_dir); }
    QString c_set_temp() { return QString::number(Hex2Dec(set_temp)) + "℃"; }
    QString c_temp() { return QString::number(Hex2Dec(temp)) + "℃"; }
    QString c_cost() { return QString::number(Hex2Dec(cost)) + "度"; }
    QString c_errorCode() { return errorCode; }
    QString c_remain() { return remain; }
    QString c_timestamp() { return timestamp; }

private:
    int Hex2Dec(QString hexCode);
    QString power2c(int power);
    QString status2c(int status);
    QString wind2c(int wind);
    QString wind_dir2c(int wind_dir);
};

#endif // EQUIPMENT

