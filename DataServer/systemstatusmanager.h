#ifndef SYSTEMSTATUS_H
#define SYSTEMSTATUS_H

#include <QObject>
#include "equipment.h"
#include <map>
#include <vector>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

class SystemStatusManager : public QObject
{
    Q_OBJECT
public:
    explicit SystemStatusManager(QObject *parent = 0);
    ~SystemStatusManager();

    bool createConnection(mongocxx::client& conn);
    bool refreshEquipmentStatus(Equipment& equipment);
    vector<Equipment> fecthSystemHistoryStatus();

signals:
    void fetchSystemStatusAfter();
public slots:
    void fetchSystemStatus();

private:
    mongocxx::database db;
    mongocxx::collection systemStatus;
    mongocxx::collection historySystemStatus;
    map<QString, Equipment> equipments;

    void parseJsonString(QString jsonStr, Equipment& equipment);
    QString valueWithKey(QJsonObject &obj, QString key);
    void checkEquipmentStatus(Equipment& equipment);
    void copy(Equipment& from, Equipment& to);
};

#endif // SYSTEMSTATUS_H
