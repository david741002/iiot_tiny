#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "serialportimpl.h"
#include "systemstatusmanager.h"
#include "mqttclient.h"
#include <QMainWindow>
#include <QTimer>
#include <map>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void updateSystemStatus();
    void on_RS232Send_clicked();
    void handleMessageReceiveAfter(QString);
    void handleMessageSendBefore(QString);
    void handleMqttMessageReceive(const QMQTT::Message &message);

    void on_MQTTSend_clicked();

    void on_load_clicked();

private:
    Ui::MainWindow *ui;
    // QTimer *systemUpdateTimer;
    SystemStatusManager *systemStatusManager;
    Equipment equipAirCond, equipBlower;
    SerialPortImpl *serialportImpl;
    mongocxx::instance inst;
    mongocxx::client conn;
    mongocxx::database db;
    MqttClient *mqttClient;

    void init_timer();
    void init_serialport();
    void init_statusmanager();
    void init_mqttclient();
    void init();
    bool createConnection();
};

#endif // MAINWINDOW_H
