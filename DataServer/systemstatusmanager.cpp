#include "systemstatusmanager.h"
#include <QDebug>

using bsoncxx::builder::stream::document;
using bsoncxx::builder::stream::open_array;
using bsoncxx::builder::stream::close_array;
using bsoncxx::builder::stream::open_document;
using bsoncxx::builder::stream::close_document;
using bsoncxx::builder::stream::finalize;
using bsoncxx::builder::stream::concatenate;

SystemStatusManager::SystemStatusManager(QObject *parent) : QObject(parent)
{

}

SystemStatusManager::~SystemStatusManager()
{

}

bool SystemStatusManager::createConnection(mongocxx::client& conn)
{
    db = conn["IAQ_Db"];
    if (!db) {
        qDebug() << "Database error occurred";
        return false;
    }

    if(!db.has_collection("system_status")) {
        db.create_collection("system_status");
    }

    if(!db.has_collection("history_status")) {
        db.create_collection("history_status");
    }

    systemStatus = db["system_status"];
    historySystemStatus = db["history_status"];
    return true;
}

void SystemStatusManager::fetchSystemStatus() {
    auto cursor = systemStatus.find({});
    QString message = "";
    for (auto&& doc : cursor) {
        QString jsonStr = QString::fromStdString(bsoncxx::to_json(doc));
        // qDebug() << jsonStr;
        Equipment equipment;
        parseJsonString(jsonStr, equipment);
        checkEquipmentStatus(equipment);
    }
    emit fetchSystemStatusAfter();
}

void SystemStatusManager::parseJsonString(QString jsonStr, Equipment& equipment) {
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonStr.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QString id = jsonObject["_id"].toObject()["$oid"].toString();
    int equipId = valueWithKey(jsonObject, "equipId").toInt();
    int opCode = valueWithKey(jsonObject, "opCode").toInt();
    int power = valueWithKey(jsonObject, "power").toInt();
    int status = valueWithKey(jsonObject, "status").toInt();
    int wind = valueWithKey(jsonObject, "wind").toInt();
    int wind_dir = valueWithKey(jsonObject, "wind_dir").toInt();
    QString set_temp = valueWithKey(jsonObject, "set_temp");
    QString temp = valueWithKey(jsonObject, "temp");
    QString cost = valueWithKey(jsonObject, "cost");
    QString errorCode = valueWithKey(jsonObject, "errorCode");
    QString remain = valueWithKey(jsonObject, "remain");
    QString timestamp = valueWithKey(jsonObject, "timestamp");

    equipment.__id = id;
    equipment.equipId = equipId;
    equipment.opCode = opCode;
    equipment.power = power;
    equipment.status = status;
    equipment.wind = wind;
    equipment.wind_dir = wind_dir;
    equipment.temp = temp;
    equipment.set_temp = set_temp;
    equipment.cost = cost;
    equipment.errorCode = errorCode;
    equipment.remain = remain;
    equipment.timestamp = timestamp;
}

void SystemStatusManager::checkEquipmentStatus(Equipment& equipment) {
    QString id = equipment.id();
    QString equipId = equipment.c_equipId();
    map<QString, Equipment>::iterator ei = equipments.find(id);
    if(ei == equipments.end()) {
        equipments.insert(pair<QString, Equipment>(id, equipment));
        QString message = "裝置(" + equipId + ")新增";
        qDebug() << message;
    } else {
        // update equipment status
        if(equipments[id].c_timestamp() != equipment.c_timestamp()) {
            copy(equipment, equipments[id]);
            QString message = "裝置(" + equipId + ")狀態變更";
            qDebug() << message;
        }
    }
}

vector<Equipment> SystemStatusManager::fecthSystemHistoryStatus() {
    auto cursor = historySystemStatus.find({});
    QString message = "";
    vector<Equipment> historyStatus;
    for (auto&& doc : cursor) {
        QString jsonStr = QString::fromStdString(bsoncxx::to_json(doc));
        qDebug() << jsonStr;
        Equipment equipment;
        parseJsonString(jsonStr, equipment);
        checkEquipmentStatus(equipment);
        historyStatus.push_back(equipment);
    }
    return historyStatus;
}

bool SystemStatusManager::refreshEquipmentStatus(Equipment &equipment) {
    QString id = equipment.id();
    if(equipment.c_timestamp() == equipments[id].c_timestamp())
        return false;
    copy(equipments[id], equipment);

    return true;
}

void SystemStatusManager::copy(Equipment& from, Equipment& to) {
    to.equipId = from.equipId;
    to.opCode = from.opCode;
    to.power = from.power;
    to.status = from.status;
    to.wind = from.wind;
    to.wind_dir = from.wind_dir;
    to.set_temp = from.set_temp;
    to.temp = from.temp;
    to.cost = from.cost;
    to.errorCode = from.errorCode;
    to.remain = from.remain;
    to.timestamp = from.timestamp;
}

QString SystemStatusManager::valueWithKey(QJsonObject &obj, QString key) {
    return obj[key].toString();
}

