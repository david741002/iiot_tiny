#-------------------------------------------------
#
# Project created by QtCreator 2017-02-03T21:15:58
#
#-------------------------------------------------

QT       += core gui sql widgets network

CONFIG += c++11
QMAKE_CXXFLAGS += -std=c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DataServer
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    equipment.cpp \
    systemstatusmanager.cpp \
    serialportimpl.cpp \
    mqttclient.cpp \
    qmqtt/qmqtt_client.cpp \
    qmqtt/qmqtt_client_p.cpp \
    qmqtt/qmqtt_frame.cpp \
    qmqtt/qmqtt_message.cpp \
    qmqtt/qmqtt_network.cpp \
    qmqtt/qmqtt_routedmessage.cpp \
    qmqtt/qmqtt_router.cpp \
    qmqtt/qmqtt_routesubscription.cpp \
    qmqtt/qmqtt_will.cpp

HEADERS  += mainwindow.h \
    equipment.h \
    systemstatusmanager.h \
    serialportimpl.h \
    mqttclient.h \
    qmqtt/qmqtt.h \
    qmqtt/qmqtt_client.h \
    qmqtt/qmqtt_client_p.h \
    qmqtt/qmqtt_frame.h \
    qmqtt/qmqtt_global.h \
    qmqtt/qmqtt_message.h \
    qmqtt/qmqtt_network.h \
    qmqtt/qmqtt_routedmessage.h \
    qmqtt/qmqtt_router.h \
    qmqtt/qmqtt_routesubscription.h \
    qmqtt/qmqtt_will.h

FORMS    += mainwindow.ui

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libbsoncxx
unix: PKGCONFIG += libmongocxx
