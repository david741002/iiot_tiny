#include "equipment.h"

QString Equipment::power2c(int power) {
    return power == 1 ? "開" : "關";
}

QString Equipment::status2c(int status) {
    switch(status) {
    case 0:
        return "冷氣";
        break;
    case 1:
        return "除濕";
        break;
    case 2:
        return "送風";
        break;
    case 4:
        return "暖氣";
        break;
    default:
        return "無";
        break;
    }
}

QString Equipment::wind2c(int wind) {
    switch(wind) {
    case 0:
        return "自動";
        break;
    case 1:
        return "低速";
        break;
    case 2:
        return "中速";
        break;
    case 4:
        return "高速";
        break;
    default:
        return "無";
        break;
    }
}

QString Equipment::wind_dir2c(int wind_dir) {
    switch(wind_dir) {
    case 0:
        return "自動";
        break;
    case 1:
        return "角度1";
        break;
    case 2:
        return "角度2";
        break;
    case 3:
        return "角度3";
        break;
    case 4:
        return "角度4";
        break;
    case 5:
        return "角度5";
        break;
    default:
        return "無";
        break;
    }
}

int Equipment::Hex2Dec(QString hexCode) {
    bool ok;
    int dec = hexCode.toInt(&ok, 16);
    if(ok)
        return dec;
    return -1;
}
