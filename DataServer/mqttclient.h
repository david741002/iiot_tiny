#ifndef MQTTCLIENT_H
#define MQTTCLIENT_H
#include <QString>
#include <QObject>
#include "qmqtt/qmqtt_client.h"

using QMQTT::Client;
using QMQTT::Will;
using QMQTT::Message;

class MqttClient : public Client
{

public:
    MqttClient(const QString & host, quint32 port) : Client(host, port) {

    }
    void setTopic(const QString & topic) {
        subTopic = topic;
    }
    void setQos(const QString & qos) {
        subQos = qos.toUInt();
    }

signals:

public slots:
    void subscribeTo();
    void showSubscribed();
    void showConnected();
    void showDisConnected();

private:
    QString subTopic;
    quint8 subQos;

};

#endif // MQTTCLIENT_H
