#include "mqttclient.h"
#include <iostream>

void MqttClient::subscribeTo() {
    subscribe(subTopic, subQos);
}

void MqttClient::showSubscribed() {
    std::cout << "mqtt subscribed" << std::endl;
}

void MqttClient::showConnected() {
    std::cout << "mqtt connected!" << std::endl;
}

void MqttClient::showDisConnected() {
    std::cout << "mqtt disconnected!" << std::endl;
}
