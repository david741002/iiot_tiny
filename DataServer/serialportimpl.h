#ifndef EQUIPMENTIMPL_H
#define EQUIPMENTIMPL_H

#include <QThread>
#include <string>
#include <QTimer>

using namespace std;

class SerialPortImpl : public QThread
{
    Q_OBJECT
public:
    explicit SerialPortImpl(QObject *parent = 0);
    ~SerialPortImpl();

    bool initialize();
    void receive(string &);
    void send(string data);
    virtual void run();

signals:
    void messageReceiveAfter(QString);
    void messageSendBefore(QString);
public slots:

private:
    int USB;
    bool isOpened;
    QTimer *timer;

private slots:
    void initializeReTimer();
};

#endif // EQUIPMENTIMPL_H
