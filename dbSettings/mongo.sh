db="IAQ_Db"
coll_device_address="device_address"
coll_equipment_map="equipment_map"
if [ $1 = "export" ]; then
mkdir export
mongoexport -d ${db} -c device_address -o export/${coll_device_address}.json
mongoexport -d ${db} -c equipment_map -o export/${coll_equipment_map}.json

elif [ $1 = "import" ]; then
mongoimport -d ${db} $2/${coll_device_address}.json
mongoimport -d ${db} $2/${coll_equipment_map}.json
fi
